﻿var projects = ['6153ma.js'];
module.exports = function(grunt) {

	console.log("");
	console.log("");
	console.log("");
	console.log("");
	console.log("              ▒████▓▓▒▒ ▒▒▒ ▒▒▒▒▒▒▓▓████▓▒                  ");
	console.log("           ▒███▓                      ▒███████▓▓▒           ");
	console.log("         ▒██▓    ▒▒███▓▓▒             ▓▓▒▒▒▒▒▓██████▓       ");
	console.log("       ▒██▓   ▓███▓▒        ▒▒▒▒▒▒▒▒▒▒      ▒▓▓███▓████▒▒   ");
	console.log("      ██▓  ▒▓██▓  ▒▓██████▓       ▒      ▒██▓▒   ▓███▒▓▒    ");
	console.log("     ██  ▓███▒  ▒██▒    ▒▒██▓           ██▒    ▒████▒█      ");
	console.log("    ██ ▒▓▒▓▓   ██           █▓         ██      ▒███  █▒     ");
	console.log("   ▓█         ██            ▓█        ██             █▒     ");
	console.log("   █▓         █▒  ████       █▒       ██            ███     ");
	console.log("  ▒█       ▒▓▒█▓ ▓████▓     ▒█        ▒█▒          ██ █▒    ");
	console.log("  ██     ▒▓▒▓▒██▒▒▓▓▓       ██         ▒████▓███████▓ █▒    ");
	console.log("  █▓     ▒   ▒ ▒██▓▒     ▒██▓              ██▓      ▒██▓    ");
	console.log("  █         ▓▒    ▒▓██████▓              ▒██   ▓█▓▓▒   ██   ");
	console.log(" ▒█                          ▓▒▓▒▒▒▒▒▓▓▓▓██  ▓█▓ ▒▒██▒  ██  ");
	console.log(" ▓█                          ▒▒▒▒▒▒▓▓▒  ██  ██▓ ▒ ▒ ██  ▒█  ");
	console.log(" ██                       ▒▓▒▒▒▒▒▒▒▒   ██  ▓█ █▓ █▒█▓█▓  █  ");
	console.log(" ██                         ▒▒▒▒▒ ▒▒  ▓█▓  ██ █▒▒█▒█▒▓█  █  ");
	console.log(" ██                        ▒          ▓█   █▒    ▒▒  ▒█ ▓█  ");
	console.log(" ▒█                        ▒▒▒▒▒▒▒▒▒▒  █▒  █▒        ▓█ █▓  ");
	console.log("  █▓ ▒▒                                ▓█  █▒        █ ▒█   ");
	console.log("  ▓█  ▒  ▒▒  ▒                          █  █▒       █▓ █▓   ");
	console.log("   █▒  ▒  ▒  ▒▒                         █  █▒      ▓█  █    SKYBRUD");
	console.log("   ██   ▒ ▒   ▒▒                        █  █▒      ██  █    FRONTEND v3");
	console.log("    █▓   ▒ ▒    ▒▒     ▒▒▒▒▒▒          ▒█ ▒█       █▒  █▓   ");
	console.log("    ▓█    ▒▒     ▒▒      ▒▒▒▓▓▓▒       ██ ██       ██  ▓█   ");
	console.log("     ██   ▒ ▒     ▒        ▒ ▒▒▓█▒    ▒█  █▓▒▓▓▓▒  ▓█   █▒  ");
	console.log("     ▒█▒   ▒▒▒    ▒          ▒▒▒ ▒▓   ██ ▒█    ▒▓▓  ██  █▒  ");
	console.log("      ▒█▒   ▒ ▒   ▒      ▒▒▒    ▒▒   ▒█  ██       █ ▒█  █▒  ");
	console.log("       ▓█   ▒ ▒    ▒▒    ▓▒▒▓▓▓▒  ▓▒ ██  ██▒▒▒▒▓▒▓▓███  █▒  ");
	console.log("        ██   ▒ ▒     ▒▒        ▓█▓   █▓  ██ ▓█ █ █  █▒  █▒  ");
	console.log("         ██    ▒▒      ▒▒        ▒█▓ █▓  ▓█▒▒█▒█ █▒██  ▒█   ");
	console.log("          ██▒    ▒   ▒   ▒▒        ▒▓██   ██    ▒▒██   ██   ");
	console.log("           ▓██        ▒▒   ▒         ▓█    ▓███▓▓██   ██    ");
	console.log("             ▓██▒      ▒▒▒▒▒          ██      ▒▒▒    ██     ");
	console.log("               ▓███▒       ▒▒▒▒▒▒▒▒    ▓██▒       ▒███      ");
	console.log("                 ▒▓███▓▒       ▒  ▒▒▒▒   ▒██▓██████▓        ");
	console.log("                     ▒████▓▒▒               ▓██▒            ");
	console.log("                         ▒▓████▓       ▓█████▒              ");
	console.log("                              ▒█████████▒                   ");
	console.log("                                  ▒▒▒                       ");
	console.log("");
	console.log("");
	console.log("");

	grunt.initConfig({
		hub: {
			dev: {
				src:projects,
				tasks:['dev']
			},
			watch: {
				src:projects,
				tasks:['watch']
			}
		}
	});

	grunt.loadNpmTasks('grunt-hub');
	grunt.registerTask('dev', ['hub:dev']);
	grunt.registerTask('default', ['hub:watch']);
	grunt.registerTask('watch', ['hub:watch']);

};
