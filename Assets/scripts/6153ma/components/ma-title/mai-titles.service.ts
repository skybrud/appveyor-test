(function() {
	"use strict";

	angular.module('maTitle').service('maTitles', maTitleService);

	maTitleService.$inject = [];

	function maTitleService() {
		var titles = [];

		this.getTitles = function() {
			return titles;
		};

		this.removeTitle = function(title) {
			var index = titles.indexOf(title);
			if(index !== -1) {
				titles.splice(index, 1);
			}
		};

		this.addTitle = function(title) {
			titles.push(title);
		};
	}
})();
