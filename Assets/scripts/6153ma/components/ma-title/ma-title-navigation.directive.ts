interface Window {
	shit: any;
}
(function() {
	"use strict";

	angular.module('maTitle').directive('maTitleNavigation', maTitleNavigationDirective);

	maTitleNavigationDirective.$inject = ['skyVisible', 'maTitles'];

	function maTitleNavigationDirective(skyVisible, maTitles) {
		return {
			restrict:'E',
			transclude:true,
			controller:controller,
			controllerAs:'MaTitleNavigation',
			templateUrl:'/ma-title/ma-title-navigation.template.html',
		};

		function controller() {
			var _this = this;

			this.titles = maTitles.getTitles();

			this.current = function() {
				for(var i = _this.titles.length - 1; i >= 0; i--) {
					if(_this.titles[i].current) {
						return i;
					}
				}
			};
		}
	}
})();
