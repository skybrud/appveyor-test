(function() {
	"use strict";

	angular.module('maTitle').directive('maTitle', maTitleDirective);

	maTitleDirective.$inject = ['maTitles', 'skyVisible', '$timeout', 'skySticky'];

	function maTitleDirective(maTitles, skyVisible, $timeout, skySticky) {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element, attrs, ctrl) {
			var titleObject = {
				title:attrs.maTitle,
				node:element[0],
				current:false,
				scrollTo:scrollTo
			};

			var elementDimensions;

			var tl = new TimelineLite();

			maTitles.addTitle(titleObject);

			skyVisible.bind(element[0], function(values, dimensions, scroll) {
				var top = dimensions.top - 100 - skySticky.getOffset();

				if(scroll.y > top && !titleObject.current) {
					$timeout(function(){
						titleObject.current = true;
					});
				} else if(scroll.y < top && titleObject.current) {
					$timeout(function(){
						titleObject.current = false;
					});
				}

				if(!elementDimensions) {
					elementDimensions = dimensions;
				}
			});

			element.on('$destroy', () => {
				maTitles.removeTitle(titleObject);
			});

			function scrollTo() {
				var progress = {y:0};
				var start = window.pageYOffset;

				// Make sure a user scroll breaks animation
				window.addEventListener('mousewheel', scrollBreak);

				tl = new TimelineLite();

				tl.eventCallback('onUpdate', function() {
					var target = ((elementDimensions.top - skySticky.getOffset() - 20) - start) * progress.y;

					window.scrollTo(0, start + target);

				});

				tl.to(progress, 1, {
					y:1,
					ease:Quart.easeInOut
				});
			}

			function scrollBreak(event) {
				if(event) {
					tl.kill();
					window.removeEventListener('mousewheel', scrollBreak);
				}
			}
		}
	}
})();
