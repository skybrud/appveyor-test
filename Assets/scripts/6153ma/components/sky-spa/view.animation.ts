(function() {
	'use strict';
	angular.module('skySpa').animation('.view', viewAnimation);

	viewAnimation.$inject = ['skyDirection','$animate', '$rootScope', 'maPageWipe'];
	function viewAnimation(skyDirection: sky.ISkyDirectionService, $animate: ng.animate.IAnimateService, $rootScope, maPageWipe) {

		var _wipe = angular.element(document.querySelector('.wipe'));

		var wipeStall = false;
		var wipeVisible = true;
		var success = true;

		var scope = $rootScope.$new();

		var events = {
			enter(element, done) {
				var direction = skyDirection.getDirection();

				if (direction + 'Enter' in animations) {
					animations[direction + 'Enter'](element, done);
				} else {
					done();
					$rootScope.$broadcast('viewEnter');
				}

			},
			leave(element, done) {
				var direction = skyDirection.getDirection();

				if (direction + 'Leave' in animations) {
					animations[direction + 'Leave'](element, done);
				} else {
					done();
					$rootScope.$broadcast('viewLeave');
				}
			}
		};

		var animations = {
			wipeEnter,
			wipeLeave,
			noAniEnter:() => {
				setTimeout(() => {
					maPageWipe.wipeOut();
				}, 100);
			}
		};

		function wipeEnter(element, done) {
			$rootScope.$broadcast('viewEnter');
			done();
		}

		function wipeLeave(element, done) {
			$rootScope.$broadcast('viewLeave');
			done();
		}

		return events;
	}

})();
