(function() {
	"use strict";

	angular.module('skySpa').directive('uiView', uiViewDirective);

	function uiViewDirective() {
		return {
			link:link
		};

		function link(scope, element) {
			scope.$on('$stateChangeSuccess', () => {
				element.addClass('ready');
			});
		}
	}
})();
