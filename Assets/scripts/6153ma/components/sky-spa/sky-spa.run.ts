interface Window {
	labels?:any;
	settings?:any;
}
(function() {
	'use strict';

	angular.module('skySpa').run(run);

	run.$inject = ['$rootScope', 'pagetreeCache', '$location', 'skyGaService'];

	function run($rootScope, pagetreeCache, $location, skyGaService) {

		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState) {
			skyGaService.send('pageview', $location.path());
		});

		$rootScope.settings = angular.copy(window.settings);
		$rootScope.labels = angular.copy(window.labels);

		$rootScope.settings.mainNavigation = [];
		angular.forEach(window.settings.mainNavigation, function(page) {
			pagetreeCache.add(page);
			$rootScope.settings.mainNavigation.push(page.id);
		});

		$rootScope.settings.secNavigation = [];
		angular.forEach(window.settings.secNavigation, function(page) {
			pagetreeCache.add(page);
			$rootScope.settings.secNavigation.push(page.id);
		});

		$rootScope.goToSearch = function() {
			$location.path('/search/');
		};

	}

})();
