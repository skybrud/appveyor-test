(function() {
	"use strict";

	angular.module('skySpa').directive('skyViewType', skyViewTemplateDirective);

	function skyViewTemplateDirective() {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element) {
			var templateName = scope.stateCtrl && scope.stateCtrl.data ? scope.stateCtrl.data.templatename : '';
			var documentType = templateName.slice(0, templateName.indexOf('.'));
			var template = documentType.replace('Made', '').toLowerCase();

			var documentTypes = {
				MadeForside:'frontpage',

				MadeTemaer:'articles',
				MadeUnderforside:'articles',
				MadeArrangementer:'articles',
				MadeNyheder:'articles',
				MadeVoresPulse:'articles',

				MadePersoner:'article',
				MadePerson:'article',
				MadeArrangement:'article event',
				MadeTemaUnderside:'article',
				MadeUnderside:'article',
				MadeCase:'article',
				MadeNyhed:'article',
				MadeTema:'article',
				MadeFaqs:'article'
			};

			element.addClass(documentTypes[documentType] || 'article');
		}
	}

})();
