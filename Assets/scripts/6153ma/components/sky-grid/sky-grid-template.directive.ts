(function() {
	"use strict";

	angular.module('skyGrid').directive('skyGridTemplate', skyGridTemplateDirective);

	skyGridTemplateDirective.$inject = ['$compile', '$templateCache'];

	function skyGridTemplateDirective($compile, $templateCache) {
		return {
			restrict:'E',
			scope:{
				alias:'=',
				control:'='
			},
			compile:compile
		};

		function compile() {
			return {
				pre:pre
			};
		}

		function pre(scope, element, attrs) {
			var once = scope.$watch('alias', (alias) => {
				alias = alias || attrs.alias;
				element.html($templateCache.get(`/sky-grid/controls/${alias}.template.html`));
				$compile(element.contents())(scope);
				element.addClass(alias);
				once();
			});
		}
	}
})();
