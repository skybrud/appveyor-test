(function() {
	"use strict";

	angular.module('maFaqs').directive('maFaqs', maFaqsDirective);

	function maFaqsDirective() {
		return {
			restrict:'E',
			scope: {
				items:'='
			},
			templateUrl:'/sky-grid/components/ma-faqs/ma-faqs.template.html',
			bindToController:true,
			controllerAs:'MaFaqs',
			controller:controller
		};

		function controller() {
		}
	}

})();
