(function() {
	"use strict";

	angular.module('maContactPerson').directive('maContactPerson', maContactPersonDirective);

	function maContactPersonDirective() {
		return {
			restrict:'E',
			scope: {
				details:'='
			},
			bindToController:true,
			controllerAs:'MaContactPerson',
			controller:controller,
			templateUrl:'/sky-grid/components/ma-contact-person/ma-contact-person.template.html',
		}

		function controller() {
			this.details = this.details || [];

			var detailProperties = [
				'company',
				'address',
				'addressMore',
				'city',
				'zip',
				'phone',
				'email'
			];

			var detailPropertiesLocal = {
				'company':'Virksomhed',
				'address':'Adresse',
				'city':'By',
				'addressMore':'',
				'zip':'',
				'phone':'Mobil',
				'email':'Mail'
			};

			this.showDetails = false;

			this.properties = [];

			detailProperties.forEach((item) => {
				var value = this.details[item];
				var key = detailPropertiesLocal[item];

				if(value) {
					this.showDetails = true;

					if(item === 'phone') {
						value = `<a href="tel:${value}">${value}</a>`;
					} else if(item === 'email') {
						value = `<a href="mailto:${value}">${value}</a>`;
					}
				}

				this.properties.push({key, value});
			});
		}
	}
})();
