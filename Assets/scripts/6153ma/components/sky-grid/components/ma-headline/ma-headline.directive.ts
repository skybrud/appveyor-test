(function() {
	"use strict";

	angular.module('maHeadline').directive('maHeadline', maHeadlineDirective);

	function maHeadlineDirective() {
		return {
			restrict:'E',
			scope:{
				headline:'='
			},
			templateUrl:'/sky-grid/components/ma-headline/ma-headline.template.html',
		};
	}
})();
