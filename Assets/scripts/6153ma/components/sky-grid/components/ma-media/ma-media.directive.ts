(function() {
	"use strict";

	angular.module('maMedia').directive('maMedia', maMediaDirective);

	function maMediaDirective() {
		return {
			restrict:'E',
			scope:{
				src:'=',
				alt:'='
			},
			templateUrl:'/sky-grid/components/ma-media/ma-media.template.html'
		};
	}
})();

