(function() {
	"use strict";

	angular.module('maForm').directive('maForm', maFormDirective);

	function maFormDirective() {
		return {
			restrict:'E',
			scope:{
				guid:'=',
			},
			templateUrl:'/sky-grid/components/ma-form/ma-form.template.html'
		};
	}
})();

