(function() {
	"use strict";

	angular.module('maRelatedLinks').directive('maRelatedLinks', maRelatedLinksDirective);

	function maRelatedLinksDirective() {
		return {
			restrict:'E',
			scope: {
				links:'=',
				type:'='
			},
			bindToController:true,
			controllerAs:'MaRelatedLinks',
			controller:controller,
			templateUrl:'/sky-grid/components/ma-related-links/ma-related-links.template.html',
			link:link
		};

		function controller() {
			var _this = this;

			var alias = {
				tools:'værktøj',
				cases:'case'
			};

			_this.items = angular.merge(new Array(5), _this.links || []);
			_this.pages = Math.ceil(_this.links.length / 3);
			_this.page = 0;
			_this.typeAlias = alias[_this.type];
		}

		function link(scope, element, attrs, ctrl) {
			var _element = element[0];
			var _list = _element.querySelector('ul');

			ctrl.next = () => {
				ctrl.page++;
				animate();
			};

			ctrl.previous = () => {
				ctrl.page--;
				animate();
			};

			function animate() {
				TweenLite.set(_list, {
					xPercent:-(ctrl.page * 100)
				});
			}
		}
	}
})();
