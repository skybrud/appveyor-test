(function() {
	'use strict';

	angular.module('core').service('skyWindowSizeListener', skyWindowSizeListener);

	skyWindowSizeListener.$inject = ['$window','$timeout'];

	function skyWindowSizeListener($window,$timeout) {
		var _this = this;
		var windowProps = {
			width:0,
            height:0
		};
		var scopes = [];

		/**
		 * add method takes scope to $apply() when resize occurs
		 * returns object with window size properties
		 */
		_this.add = function(scopeInstance) {
			scopes.push(scopeInstance);
			return windowProps;
		};

		/**
		 * measureWindow method remeasures width and height windowProps
		 * and forEaches over scopes array to $apply() on each scope instance
		 */
		_this.measureWindow = function() {
			windowProps.width = $window.innerWidth;
			windowProps.height = $window.innerHeight;

			angular.forEach(scopes, function(instance) {
				instance.$apply();
			});
		};
		_this.measureWindow();

		var debounceTimer = 0;
		var debounceMeasure = function() {
			$timeout.cancel(debounceTimer);
			debounceTimer = $timeout(_this.measureWindow, 300);
		};

		angular.element($window).on('resize', debounceMeasure);
	}

})();
