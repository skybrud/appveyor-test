(function () {
	'use strict';

	describe('Service: skyQueryString', function() {

		var skyQueryString,
			$window,
			skyUtils,
			queryString = '?param1=para1&param2=para2',
			serializeSpy,
			unserializeSpy;

		var returnValue = {param1:'para1',param2:'para2'};

		beforeEach(module('core'));

		beforeEach(function() {
			module(function($provide) {
				$provide.value('$window', {location:{search:queryString}});

				serializeSpy = jasmine.createSpy('serialize').and.returnValue(returnValue);
				unserializeSpy = jasmine.createSpy('unserialize').and.returnValue(returnValue);

				$provide.value('skyUtils', {
					serialize:serializeSpy,
					unserialize:unserializeSpy
				});
			});

			inject(function(_$window_, _skyUtils_,  _skyQueryString_) {
				skyUtils = _skyUtils_;
				skyQueryString = _skyQueryString_;
				$window = _$window_;
			});
		});

		it('should call skyUtils.unserialize width $window.location.search', function() {
			skyQueryString.get();

			expect(unserializeSpy).toHaveBeenCalledWith(queryString);
		});

		it('should return object of params', function() {
			var res = skyQueryString.get();
			expect(typeof res).toBe('object');

			expect(res).toEqual({param1:'para1',param2:'para2'});
		});

		it('should return specific attribute when provided', function() {
			var res = skyQueryString.get('param1');

			expect(typeof res).toBe('string');
			expect(res).toBe('para1');
		});

	});

})();
