(function() {
	'use strict';

	describe('Service: skyFit',function() {

		var skyFit;

		beforeEach(module('core'));

		beforeEach(inject(function(_skyFit_) {
			skyFit=_skyFit_;
		}));

		it('should have a contain-method', function() {
			expect(skyFit.contain).toBeDefined();
		});

		it('should work when source is smaller than fit', function() {
			var source = {
				width:10,
				height:10
			};
			var fit = {
				width:20,
				height:20
			};

			var res = skyFit.contain(source,fit);

			expect(res.width).toBe('10px');
			expect(res.height).toBe('10px');
			expect(res.maxWidth).toBe('10px');
			expect(res.maxHeight).toBe('10px');
		});

		it('should work when source is wider than fit', function() {
			var source = {
				width:30,
				height:20
			};
			var fit = {
				width:10,
				height:10
			};

			var res = skyFit.contain(source,fit);

			expect(res.width).toBe('10px');
			expect(res.height).toBe('6.667px');
			expect(res.maxWidth).toBe('30px');
			expect(res.maxHeight).toBe('20px');
		});

		it('should work when source is taller than fit', function() {
			var source = {
				width:20,
				height:30
			};
			var fit = {
				width:10,
				height:10
			};

			var res = skyFit.contain(source,fit);

			expect(res.width).toBe('6.667px');
			expect(res.height).toBe('10px');
			expect(res.maxWidth).toBe('20px');
			expect(res.maxHeight).toBe('30px');
		});

		it('should work when source is exactly same as fit', function() {
			var source = {
				width:10,
				height:10
			};
			var fit = {
				width:10,
				height:10
			};

			var res = skyFit.contain(source,fit);

			expect(res.width).toBe('10px');
			expect(res.height).toBe('10px');
			expect(res.maxWidth).toBe('10px');
			expect(res.maxHeight).toBe('10px');
		});
	});
})();
