(function () {
	'use strict';

	/**
	 * skyQueryString
	 * Get method returns query parameters as an object or string if param is provided.
	 * This service is only intended for use without routing.
	 */

	angular.module('core').service('skyQueryString',['$window','skyUtils', function($window, skyUtils) {
		var _this = this;
		var queryParams = skyUtils.unserialize($window.location.search);
		_this.get = function(param) {
			var undef;
			if (param) {
				return queryParams.hasOwnProperty(param) ? queryParams[param] : undefined;
			}
			return angular.copy(queryParams);
		};

		_this.getAll = _this.get;

	}]);

})();
