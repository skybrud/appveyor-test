(function() {
	'use strict';
	describe('Service: skyWindowSizeListener', function() {
		var skyWindowSizeListener,
			$timeout,
			$scope,
			$window;

		beforeEach(module('core'));

		beforeEach(inject(function(_skyWindowSizeListener_, _$timeout_, _$rootScope_, _$window_) {
			skyWindowSizeListener = _skyWindowSizeListener_;
			$timeout = _$timeout_;
			$scope = _$rootScope_.$new();
			$window = _$window_;
		}));

		beforeEach(function() {
			$window.innerWidth=10;
			$window.innerHeight=20;
		});

		it('should have an add-method', function() {
			expect(skyWindowSizeListener.add).toBeDefined();
		});

		it('should return window-dimensions in the windowProps object on add()', function() {
			var props = skyWindowSizeListener.add($scope);

			expect(typeof props).toBe('object');
			expect(props.width).toBe(10);
			expect(props.height).toBe(20);
		});

		it('should debounce the resize-action', function() {
			var spy = spyOn(skyWindowSizeListener,'measureWindow');

			angular.element($window).triggerHandler('resize');

			expect(spy).not.toHaveBeenCalled();

			$timeout.flush();

			expect(spy).toHaveBeenCalled();
		});

		it('should update the windowProps object on resize', function() {
			$window.innerWidth=100;
			$window.innerHeight=200;

			angular.element($window).triggerHandler('resize');

			$timeout.flush();

			var props = skyWindowSizeListener.add($scope);

			expect(typeof props).toBe('object');
			expect(props.width).toBe(100);
			expect(props.height).toBe(200);
		});

		it('should apply all added scopes on resize', function() {
			var spy = jasmine.createSpy('spy');

			$scope.$watch(spy,function() {});

			var props = skyWindowSizeListener.add($scope);
			$window.innerWidth=100;
			$window.innerHeight=200;

			expect(spy).not.toHaveBeenCalled();

			angular.element($window).triggerHandler('resize');
			$timeout.flush();

			expect(spy).toHaveBeenCalled();
		});
	});
})();
