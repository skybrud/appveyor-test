(function () {
	'use strict';

	angular.module('core',[
		'layout',
		'ngSanitize'
	]);

})();
