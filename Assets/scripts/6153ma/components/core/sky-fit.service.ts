(function() {
	'use strict';

	angular.module('core').service('skyFit',skyFit);

	skyFit.$inject = [];

	function skyFit() {
		var _this = this;

		_this.contain = function(element, constraints) {
			var width = 0,
				height = 0;

			if (constraints.width > element.width && constraints.height > element.height) {
				/**
				 * If source is smaller than fit
				 */
				width = element.width;
				height = element.height;

			} else if (element.width / element.height > constraints.width / constraints.height) {
				/**
				 * If source wider than fit
				 * (larger source aspect than fit aspect)
				 */
				width = (constraints.width);
				height = (element.height / element.width * (constraints.width));

			} else {
				/**
				 * If source taller than fit
				 * (smaller source aspect than fit aspect)
				 */
				width = (element.width / element.height * (constraints.height));
				height = (constraints.height).toFixed(3);

			}

			return {
				width: Math.round(width*1000)/1000+'px',
				height: Math.round(height*1000)/1000+'px',
				maxWidth: Math.round(element.width*1000)/1000 + 'px',
				maxHeight: Math.round(element.height*1000)/1000  + 'px'
			};
		};
	}
})();