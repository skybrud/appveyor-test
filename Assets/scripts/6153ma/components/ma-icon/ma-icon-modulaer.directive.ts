
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconModulaer', maIconModulaerDirective);

	function maIconModulaerDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-modulaer.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var arrowDown = svg[0].querySelector('#XMLID_34_');
			var arrowUp = svg[0].querySelector('#XMLID_39_');
			var boxDotted = svg[0].querySelector('#XMLID_69_');
			var boxSolid = svg[0].querySelector('#XMLID_153_');
			var boxRight = svg[0].querySelector('#XMLID_32_');
			var boxLeft = svg[0].querySelector('#XMLID_35_');
			var boxLeftCopy = svg[0].querySelector('#XMLID_35_copy');

			TweenLite.set(boxSolid, {
				opacity:0
			});

			timeline.to([boxDotted, boxSolid], 1, {
				y:-17.5,
				ease:Back.easeInOut
			},0);

			timeline.to([arrowDown, arrowUp], 0.9, {
				y:-17.5,
				ease:Quart.easeOut
			},0.3);

			timeline.to(arrowDown, 0.2, {
				opacity:0,
				transformOrigin:'center center',
				scale:0.9
			},0.1);
			timeline.to(arrowUp, 0.4, {
				opacity:0,
				transformOrigin:'center center',
				scale:0.9
			},0.5);

			timeline.to(arrowUp, 0.2, {
			},0.2);

			timeline.to(boxSolid, 0.4, {
				opacity:1
			},0.5);

			timeline.to(boxDotted, 0.4, {
				opacity:0
			},0.5);


			timeline.to([boxLeftCopy, boxLeft, boxRight, boxSolid], 0.6, {
				x:89.5,
				ease:Quart.easeInOut
			}, 0.8);

			

			timeline.to([arrowDown, arrowUp], 0.9, {
				y:0,
				ease:Quart.easeOut
			},1.3);

			timeline.to(arrowDown, 0.2, {
				opacity:1,
				scale:1
			},1.3);
			timeline.to(arrowUp, 0.3, {
				opacity:1,
				scale:1
			},1.3 + 0.4);


			timeline.to(boxDotted, 0.8, {
				y:0,
				ease:Quart.easeInOut
			}, 1.1);

			timeline.to(boxLeft, 0.8, {
				y:17.5,
				ease:Quart.easeInOut
			}, 1.1);

			timeline.to(boxLeft, 0.4, {
				opacity:0
			}, 1.1 + 0.2);

			timeline.to(boxDotted, 0.4, {
				opacity:1
			}, 1.1 + 0.2);


			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		return directive;
	}
})();
