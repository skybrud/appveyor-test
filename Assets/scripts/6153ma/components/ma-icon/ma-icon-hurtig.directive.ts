
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconHurtig', maIconHurtigDirective);

	function maIconHurtigDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-hurtig.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var boxRight = svg[0].querySelector('#XMLID_116_');
			var boxRightCopy = svg[0].querySelector('#XMLID_116_copy');
			var boxLeftDotted = svg[0].querySelector('#XMLID_136_');
			var boxLeftSolid = svg[0].querySelector('#XMLID_157_');
			var arrow = svg[0].querySelector('#XMLID_109_');
			var arrowCopy = svg[0].querySelector('#XMLID_109_copy');

			TweenLite.set(boxLeftSolid, {
				opacity:0
			});

			TweenLite.set(boxRightCopy, {
				opacity:0,
				x:78.45,
			});

			TweenLite.set(arrowCopy, {
				x:-78.45 * 2,
			});

			timeline.to(boxRight, 0.6, {
				x:-78.45,
				ease:Quart.easeInOut
			},0);

			timeline.to(boxLeftDotted, 0.4, {
				x:78.45,
				ease:Cubic.easeInOut
			},0);
			timeline.to(boxLeftDotted, 0.2, {
				opacity:0,
			},0.2);

			timeline.to(arrow, 0.6, {
				x:78.45 * 2,
				ease:Cubic.easeInOut
			},0);

			timeline.to(arrow, 0.2, {
			},0.4);

			timeline.to(boxRight, 0.5, {
				scale:1.2,
				opacity:0,
				transformOrigin:'center center',
				ease:Quart.easeOut
			},0.5);


			timeline.to(boxLeftDotted, 0.1, {
				x:-78.45,
			},0.5);
			


			timeline.to(arrowCopy, 0.4, {
				x:0,
				opacity:1,
				ease:Cubic.easeOut
			},0.9);

			timeline.to(boxLeftDotted, 0.4, {
				x:0,
				opacity:1,
				ease:Cubic.easeOut
			},0.9);

			timeline.to(boxRightCopy, 0.4, {
				x:0,
				opacity:1,
				ease:Quart.easeOut
			},0.9);



			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		return directive;
	}
})();
