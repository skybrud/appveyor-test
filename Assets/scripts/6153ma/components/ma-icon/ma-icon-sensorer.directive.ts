
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconSensorer', maIconSensorerDirective);

	function maIconSensorerDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-sensorer.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var outerGroup = svg[0].querySelector('#XMLID_42_');
			var outerCorners = outerGroup.querySelectorAll('polyline');

			var innerGroup = svg[0].querySelector('#XMLID_56_');
			var innerCorners = innerGroup.querySelectorAll('polyline');
			var innerLines = innerGroup.querySelectorAll('line');


			timeline.to(outerCorners[0], 0.2, {
				x:3,
				y:3,
				ease:Quart.easeOut
			}, 0.5)
			.to(outerCorners[1], 0.2, {
				x:-3,
				y:3,
				ease:Quart.easeOut
			}, '-=0.1')
			.to(outerCorners[2], 0.2, {
				x:-3,
				y:-3,
				ease:Quart.easeOut
			}, '-=0.1')
			.to(outerCorners[3], 0.2, {
				x:3,
				y:-3,
				ease:Quart.easeOut
			}, '-=0.1');


			for (var i = innerLines.length - 1; i >= 0; i--) {
				var loopStart = 0.3;
				var loopSpace = 0.4;
				var loopTimes = 2;
				for (var j = 0; j < loopTimes; j++) {
					timeline.to(innerLines[i], 0, {
						opacity:0,
					}, 0.3 + loopSpace * j + i * 0.05);
					timeline.to(innerLines[i], 0, {
						opacity:1,
					}, 0.45 + loopSpace * j + i * 0.05);
				}
			}

			timeline.to(innerCorners[0], 0.3, {
				x:-22,
				y:-22,
				ease:SteppedEase.config(3)
			}, 0)
			.to(innerCorners[1], 0.3, {
				x:22,
				y:-22,
				ease:SteppedEase.config(3)
			}, 0)
			.to(innerCorners[2], 0.3, {
				x:22,
				y:22,
				ease:SteppedEase.config(3)
			}, 0)
			.to(innerCorners[3], 0.3, {
				x:-22,
				y:22,
				ease:SteppedEase.config(3)
			}, 0)
			.to(innerCorners, 0.3, {
				x:0,
				y:0,
				ease:SteppedEase.config(3)
			});


			timeline.to(outerCorners[0], 0.2, {
				x:0,
				y:0,
				ease:Quart.easeOut
			}, '-=0.1')
			.to(outerCorners[1], 0.2, {
				x:0,
				y:0,
				ease:Quart.easeOut
			}, '-=0.1')
			.to(outerCorners[2], 0.2, {
				x:0,
				y:0,
				ease:Quart.easeOut
			}, '-=0.1')
			.to(outerCorners[3], 0.2, {
				x:0,
				y:0,
				ease:Quart.easeOut
			}, '-=0.1');



			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		return directive;
	}
})();
