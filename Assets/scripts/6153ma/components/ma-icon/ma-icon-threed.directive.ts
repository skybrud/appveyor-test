
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconThreed', maIconThreedDirective);

	function maIconThreedDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-threed.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var maskRect = svg[0].querySelector('#XMLID_1372_');
			var maskRectDummy = {
				x:0
			};
			var capRight = svg[0].querySelector('#capright');
			var capLeft = svg[0].querySelector('#capleft');
			var topGroup = svg[0].querySelector('#top_group');
			var topTip = svg[0].querySelector('#top_tip');
			var topBase = svg[0].querySelector('#top_base');

			timeline.to(maskRectDummy, 1, {
				x:110 * 0.5,
				ease:SteppedEase.config(11),
				onUpdate() {
					TweenLite.set(maskRect, {
						attr: {
							transform:'translate(' + this.target.x + ')',
						},
						immediateRender:true
					});
				}
			},0);

			timeline.to([capLeft,capRight], 1, {
				x:110 * 0.5,
				ease:SteppedEase.config(11),
			},0);

			timeline.to(topGroup, 1, {
				x:110 * 0.5,
				ease:Linear.easeNone
			},0);

			timeline.to(topGroup, 0.4, {
				x:110 * -0.5,
				ease:Quad.easeInOut
			},1);

			timeline.set(maskRectDummy, {
				x:110 * 0.45,
				onUpdate() {
					TweenLite.set(maskRect, {
						attr: {
							transform:'translate(' + this.target.x + ')',
						},
						immediateRender:true
					});
				}
			},1 + 0.4);

			timeline.set([capLeft,capRight], {
				x:110 * 0.45,
			},1 + 0.4);


			timeline.to(maskRectDummy, 2, {
				x:110 * 1.45,
				ease:SteppedEase.config(22),
				onUpdate() {
					TweenLite.set(maskRect, {
						attr: {
							transform:'translate(' + this.target.x + ')',
						},
						immediateRender:true
					});
				}
			},1 + 0.4);

			timeline.to([capLeft,capRight], 2, {
				x:110 * 1.45,
				ease:SteppedEase.config(22),
			},1 + 0.4);

			timeline.to(topGroup, 2, {
				x:110 * 0.5,
				ease:Linear.easeNone
			},1 + 0.4);

			timeline.to(topGroup, 0.4, {
				x:110 * -0.5,
				ease:Quad.easeInOut
			},1 + 0.4 + 2);


			timeline.set(maskRectDummy, {
				x:110 * -0.5,
				onUpdate() {
					TweenLite.set(maskRect, {
						attr: {
							transform:'translate(' + this.target.x + ')',
						},
						immediateRender:true
					});
				}
			},1 + 0.4 + 2 + 0.4);

			timeline.set([capLeft,capRight], {
				x:110 * -0.5,
			},1 + 0.4 + 2 + 0.4);

			timeline.to(maskRectDummy, 1, {
				x:0,
				ease:SteppedEase.config(11),
				onUpdate() {
					TweenLite.set(maskRect, {
						attr: {
							transform:'translate(' + this.target.x + ')',
						},
						immediateRender:true
					});
				}
			},1 + 0.4 + 2 + 0.4);

			timeline.to([capLeft,capRight], 1, {
				x:0,
				ease:SteppedEase.config(11),
			},1 + 0.4 + 2 + 0.4);




			timeline.to(topGroup, 1, {
				x:0,
				ease:Linear.easeNone
			},1 + 0.4 + 2 + 0.4);


			for (var i = 0; i < 0.9 / 0.4; i++) {

				timeline.to(topTip, 0.2, {
					y:35,
					ease:Quart.easeInOut
				}, 0 + (i * 0.4));

				timeline.to(topTip, 0.2, {
					y:0,
					ease:Quart.easeInOut
				}, 0 + (i * 0.4) + 0.2);


				timeline.to(topBase, 0.2, {
					x:(i % 2 > 0) ? -3 : 3,
					y:5,
					ease:Quart.easeInOut
				}, 0 + (i * 0.4));

				timeline.to(topBase, 0.2, {
					x:0,
					y:0,
					ease:Quart.easeInOut
				}, 0 + (i * 0.4) + 0.2);
			}

			for (var j = 0; j < 2 / 0.4; j++) {

				timeline.to(topTip, 0.2, {
					y:35,
					ease:Quart.easeInOut
				}, 1 + 0.4 + (j * 0.4));

				timeline.to(topTip, 0.2, {
					y:0,
					ease:Quart.easeInOut
				}, 1 + 0.4 + (j * 0.4) + 0.2);

				timeline.to(topBase, 0.2, {
					x:(j % 2 > 0) ? -3 : 3,
					y:5,
					ease:Quart.easeInOut
				}, 1 + 0.4 + (j * 0.4));

				timeline.to(topBase, 0.2, {
					x:0,
					y:0,
					ease:Quart.easeInOut
				}, 1 + 0.4 + (j * 0.4) + 0.2);
			}

			for (var k = 0; k < 0.8 / 0.4; k++) {

				timeline.to(topTip, 0.2, {
					y:35,
					ease:Quart.easeInOut
				}, 1 + 0.4 + 2 + 0.4 + (k * 0.4));

				timeline.to(topTip, 0.2, {
					y:0,
					ease:Quart.easeInOut
				}, 1 + 0.4 + 2 + 0.4 + (k * 0.4) + 0.2);


				timeline.to(topBase, 0.2, {
					x:(k % 2 > 0) ? -3 : 3,
					y:5,
					ease:Quart.easeInOut
				}, 1 + 0.4 + 2 + 0.4 + (k * 0.4));

				timeline.to(topBase, 0.2, {
					x:0,
					y:0,
					ease:Quart.easeInOut
				}, 1 + 0.4 + 2 + 0.4 + (k * 0.4) + 0.2);
			}

			timeline.timeScale(3);

			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		return directive;
	}
})();
