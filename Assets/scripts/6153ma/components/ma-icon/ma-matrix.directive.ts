(function() {
	"use strict";

	angular.module('maIcon').directive('maMatrix', maMatrixDirective);

	function maMatrixDirective() {
		var directive = {
			restrict:'E',
			templateUrl:'/ma-icon/ma-matrix.template.html',
			controller:controller,
			controllerAs:'maMatrixCtrl'
		};

		controller.$inject = ['$http', 'skyPath'];

		function controller($http, skyPath) {
			var api = skyPath.get() + '/umbraco/api/NavigationApi/GetPageTree/?nodeId=1061&levels=2';

			this.themes = [];

			$http.get(api).success((result) => {
				this.themes = result.data.children;
			});
		}

		return directive;
	}
})();
