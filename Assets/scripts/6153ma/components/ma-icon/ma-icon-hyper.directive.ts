
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconHyper', maIconHyperDirective);

	function maIconHyperDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-hyper.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var leftArm = svg[0].querySelector('#XMLID_95_');
			var leftJoint1 = svg[0].querySelector('#XMLID_169_');
			var leftJoint2 = svg[0].querySelector('#XMLID_170_');
			var leftJoint3 = svg[0].querySelector('#XMLID_71_');

			var rightArm = svg[0].querySelector('#XMLID_31_');
			var rightJoint1 = svg[0].querySelector('#XMLID_99_');
			var rightJoint2 = svg[0].querySelector('#XMLID_101_');
			var rightJoint3 = svg[0].querySelector('#XMLID_104_');



			TweenLite.set(leftArm, {
				transformOrigin:'5px 200px',
			});


			TweenLite.set(rightArm, {
				transformOrigin:'25px 190px',
			});


			TweenLite.set(leftJoint1, {
				transformOrigin:'51% 135%',
			});


			TweenLite.set(rightJoint1, {
				transformOrigin:'51% 140%',
			});


			TweenLite.set(leftJoint2, {
				transformOrigin:'-33% 123%',
			});


			TweenLite.set(rightJoint2, {
				transformOrigin:'128% 130%',
			});


			TweenLite.set(leftJoint3, {
				transformOrigin:'-55% 160%',
			});

			TweenLite.set(rightJoint3, {
				transformOrigin:'155% -54%',
			});


			timeline.to(leftArm, 1, {
				rotation:-10,
				ease:Cubic.easeInOut
			},0.5);


			timeline.to(rightArm, 1, {
				rotation:5,
				ease:Cubic.easeInOut
			},0);
			timeline.to(rightArm, 1, {
				rotation:-9,
				y:20,
				ease:Sine.easeInOut
			},1);


			timeline.to(leftJoint1, 1, {
				rotation:79,
				ease:Cubic.easeInOut
			},0.5);


			timeline.to(rightJoint1, 1, {
				rotation:-10,
				ease:Cubic.easeInOut
			},0);
			timeline.to(rightJoint1, 1, {
				rotation:40,
				ease:Sine.easeInOut
			},1);


			timeline.to(leftJoint2, 1, {
				rotation:-160,
				ease:Cubic.easeInOut
			},0.5);
			


			timeline.to(rightJoint2, 1, {
				rotation:-20,
				ease:Cubic.easeInOut
			},0);
			timeline.to(rightJoint2, 1, {
				rotation:-50,
				ease:Sine.easeInOut
			},1);


			timeline.to(leftJoint3, 1, {
				rotation:360,
				ease:Cubic.easeInOut
			},0.5);

			timeline.to(rightJoint3, 1, {
				rotation:420,
				ease:Cubic.easeInOut
			},0);
			timeline.to(rightJoint3, 1, {
				rotation:500,
				ease:Sine.easeInOut
			},1);




			timeline.to(leftArm, 1.5, {
				rotation:0,
				ease:Cubic.easeInOut
			}, 1.5);


			timeline.to(rightArm, 1, {
				rotation:0,
				y:0,
				ease:Cubic.easeInOut
			}, 2);


			timeline.to(leftJoint1, 1.5, {
				rotation:0,
				ease:Cubic.easeInOut
			}, 1.5);


			timeline.to(rightJoint1, 1, {
				rotation:0,
				ease:Cubic.easeInOut
			}, 2);


			timeline.to(leftJoint2, 1.5, {
				rotation:0,
				ease:Cubic.easeInOut
			}, 1.5);


			timeline.to(rightJoint2, 1, {
				rotation:0,
				ease:Cubic.easeInOut
			}, 2);


			timeline.to(leftJoint3, 1.5, {
				rotation:360 * 2,
				ease:Cubic.easeInOut
			}, 1.5);

			timeline.to(rightJoint3, 1, {
				rotation:0,
				ease:Cubic.easeInOut
			}, 2);




			if (ctrl) {
				ctrl.timeline = timeline;
			}
		}

		return directive;
	}
})();
