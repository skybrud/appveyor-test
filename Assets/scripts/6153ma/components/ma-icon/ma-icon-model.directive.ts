
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconModel', maIconModelDirective);

	function maIconModelDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-model.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var circle1 = svg[0].querySelector('#XMLID_6_');
			var circle2 = svg[0].querySelector('#XMLID_9_');
			var circle3 = svg[0].querySelector('#XMLID_12_');
			var circle4 = svg[0].querySelector('#XMLID_178_');

			timeline.to(circle1, 0.8, {
				x:87.773,
				ease:Cubic.easeInOut
			},0);
			timeline.to(circle2, 0.9, {
				x:87.773,
				ease:Cubic.easeInOut
			},0);
			timeline.to(circle3, 0.9, {
				x:87.773,
				ease:Cubic.easeInOut
			},0);
			timeline.to(circle4, 1, {
				x:87.773,
				ease:Cubic.easeInOut
			},0);


			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		return directive;
	}
})();
