
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconLivslang', maIconLivslangDirective);

	function maIconLivslangDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-livslang.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var centerCircle = svg[0].querySelector('#XMLID_21_');

			var groupBox = svg[0].querySelector('#XMLID_76_');
			var lines = groupBox.querySelectorAll('line');

			var groupWaves = svg[0].querySelector('#XMLID_183_');
			var waves = groupWaves.querySelectorAll('circle');


			timeline.to(waves[0], 1, {
				attr: {
					r:43
				},
				ease:Sine.easeInOut
			}, 0);

			timeline.to(waves[1], 1, {
				attr: {
					r:60
				},
				ease:Sine.easeInOut
			}, 0);
			timeline.to(waves[0], 1, {
				attr: {
					r:46.8
				},
				ease:Sine.easeInOut
			}, 1);

			timeline.to(waves[1], 1, {
				attr: {
					r:67.8
				},
				ease:Sine.easeInOut
			}, 1);

			//	timeline.from(waves[1], 1, {
			//		attr: {
			//			r:40
			//		},
			//		ease:Cubic.easeInOut
			//	}, 0);


			//	timeline.set(centerCircle, {
			//		strokeDasharray:88,
			//		strokeDashoffset:88,
			//	});

			timeline.fromTo(centerCircle, 0.75, {
				attr: {
					r:14
				},
				transformOrigin:'50% 50%',
				rotation:-90 + 45 / 2,
				strokeDasharray:88,
				strokeDashoffset:0,
			},{
				attr: {
					r:17
				},
				rotation:90 + 45,
				strokeDasharray:107,
				strokeDashoffset:-107,// * (0.25 + 0.125) ,
				ease:Cubic.easeIn
			}, 0.25);
			timeline.to(centerCircle, 0.75, {
				attr: {
					r:14
				},
				rotation:270 + 45,
				transformOrigin:'50% 50%',
				strokeDasharray:88,
				strokeDashoffset:-88 * 2,
				ease:Cubic.easeOut
			},1.1);
			

			
			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		return directive;
	}
})();
