
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconDigital', maIconDigitalDirective);

	function maIconDigitalDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-digital.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var products = svg[0].querySelector('#XMLID_132_');
			var scanline = svg[0].querySelector('#XMLID_1852_');
			var lamp = svg[0].querySelector('#XMLID_1872_');

			TweenLite.set(lamp, {
				opacity:0
			});

			timeline.to(products, 1, {
				x:60.941,
				ease:Cubic.easeInOut
			});
			
			timeline.to(scanline, 1.2, {
				y:77,
				ease:Cubic.easeInOut
			}, 0.8);

			timeline.fromTo(scanline, 1.2, {
				strokeDashoffset:15,
			},{
				strokeDashoffset:-300,
				ease:SteppedEase.config(15)
			}, 0.8);


			for (var i = 0; i < 4; i++) {
				timeline.to(lamp, 0.1, {
					opacity:1,
					ease:SteppedEase.config(1)
				}, 1 + i * 0.2);

				timeline.to(lamp, 0.1, {
					opacity:0,
					ease:SteppedEase.config(1)
				}, 1 + i * 0.2 + 0.1);

			}

			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		return directive;
	}
})();
