(function() {
	"use strict";

	angular.module('maIcon', []).run(run);

	run.$inject = ['$rootScope', '$location']

	function run($rootScope, $location) {
		$rootScope.location = $location.path();
		$rootScope.$on('$stateChangeSuccess', () => {
			$rootScope.location = $location.path();
		});
	}

})();

