(function() {
	"use strict";

	angular.module('maIcon').directive('maIcon', maIconDirective);

	maIconDirective.$inject = ['$compile', '$timeout'];

	function maIconDirective($compile, $timeout) {
		return {
			restirct:'E',
			link:link
		};

		function link(scope, element, attrs) {
			var iconMap = {
				'Hurtig Produktudvikling':'icon-hurtig',
				'Modulær produktions­platform':'icon-modulaer',
				'3D print og nye produktionsprocesser':'icon-threed',
				'Modelbaseret udvikling af forsyningskæder':'icon-model',
				'Digital udvikling af forsyningskæder':'icon-digital',
				'Livslang produkttilpasning':'icon-livslang',
				'Fremtidens produktions­paradigme':'icon-fremtid',
				'Hyperfleksibel automation':'icon-hyper',
				'Sensorer og kvalitetskontrol':'icon-sensorer'
			};

			var icon = iconMap[attrs.icon];

			if(icon) {
				element[0].innerHTML  = `<ma-${icon} class="icon"></ma-${icon}>`;

				scope.$on('viewLeave', () => {
					$compile(element.contents())(scope);
				});
			}
		}
	}

})();
