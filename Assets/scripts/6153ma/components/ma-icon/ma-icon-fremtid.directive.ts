
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconFremtid', maIconFremtidDirective);

	function maIconFremtidDirective() {
		var directive = {
			restrict:'E',
			require:'?^maIconHover',
			link:link,
			templateUrl:'/icon-fremtid.svg'
		};

		function link(scope, element, attrs, ctrl) {
			var timeline:any = new TimelineMax({paused:true});
			var svg = element.find('svg');

			var factory = svg[0].querySelector('#XMLID_120_');
			var outer = svg[0].querySelector('#XMLID_114_');
			var inner = svg[0].querySelector('#XMLID_122_');
			var outerMost = svg[0].querySelector('#XMLID_156_');
			var innerMost = svg[0].querySelector('#XMLID_158_');

			var outerPoints = polylineGet(outer);
			var innerPoints = polylineGet(inner);
			var outerMostPoints = polylineGet(outerMost);
			var innerMostPoints = polylineGet(innerMost);

			TweenLite.set(outerMost, {
				opacity:0
			});

			timeline.to(factory, 0.4, {
				x:3,
				y:-2,
				ease:Cubic.easeIn,
			},0.1);
			timeline.to(factory, 0.4, {
				x:0,
				y:0,
				ease:Cubic.easeOut,
			},0.5);

			timeline.to(innerMostPoints[0], 0.8, {
				x:innerPoints[0].x,
				y:innerPoints[0].y,
				ease:Cubic.easeInOut,
			},0);

			timeline.to(innerMostPoints[1], 0.8, {
				x:innerPoints[1].x,
				y:innerPoints[1].y,
				ease:Cubic.easeInOut,
			},0);

			timeline.to(innerMostPoints[2], 0.8, {
				x:innerPoints[2].x,
				y:innerPoints[2].y,
				ease:Cubic.easeInOut,
				onUpdate: function() {
					polylineSet(innerMost, innerMostPoints);
				}
			},0);

			timeline.to(innerPoints[0], 0.9, {
				x:outerPoints[0].x,
				y:outerPoints[0].y,
				ease:Cubic.easeInOut,
			},0);

			timeline.to(innerPoints[1], 0.9, {
				x:outerPoints[1].x,
				y:outerPoints[1].y,
				ease:Cubic.easeInOut,
			},0);

			timeline.to(innerPoints[2], 0.9, {
				x:outerPoints[2].x,
				y:outerPoints[2].y,
				ease:Cubic.easeInOut,
				onUpdate: function() {
					polylineSet(inner, innerPoints);
				}
			},0);


			timeline.to(outerPoints[0], 1, {
				x:outerMostPoints[0].x,
				y:outerMostPoints[0].y,
				ease:Cubic.easeInOut,
			},0);

			timeline.to(outerPoints[1], 1, {
				x:outerMostPoints[1].x,
				y:outerMostPoints[1].y,
				ease:Cubic.easeInOut,
			},0);

			timeline.to(outerPoints[2], 1, {
				x:outerMostPoints[2].x,
				y:outerMostPoints[2].y,
				ease:Cubic.easeInOut,
				onUpdate: function() {
					polylineSet(outer, outerPoints);
				}
			},0);

			timeline.to(outer, 1, {
				opacity:0,
				ease:Cubic.easeInOut,
			},0);

			if (ctrl) {
				ctrl.timeline = timeline;
			}

		}

		function polylineGet(polyline) {
			var points = [];

			var str = angular.element(polyline).attr('points');
			var pointStrings = str.split(' ');

			pointStrings.forEach(function(ele) {
				var re = /,/g;
				var splitted = ele.split(',');
				points.push({
					x:parseFloat(splitted[0]),
					y:parseFloat(splitted[1])
				});
			});

			return points;
		}

		function polylineSet(polyline, points) {
			var str = '';
			var polylineEle = angular.element(polyline);

			angular.forEach(points, function(ele) {
				str += (Math.round(ele.x * 10) / 10) + ',' + (Math.round(ele.y * 10) / 10) + ' ';
			});

			if (polylineEle.attr('points') !== str) {
				angular.element(polyline).attr('points', str);
			}


			return str;
		}

		return directive;
	}
})();
