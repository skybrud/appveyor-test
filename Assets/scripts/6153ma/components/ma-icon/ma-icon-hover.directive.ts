
(function() {
	"use strict";

	angular.module('maIcon').directive('maIconHover', maIconHoverDirective);

	function maIconHoverDirective() {
		var directive = {
			restrict:'A',
			controller:MaIconHoverController,
		};

		MaIconHoverController.$inject = ['$element'];

		function MaIconHoverController($element) {
			var _this = this;
			var timelineInited = false;

			_this.hovering = false;

			$element.on('mouseenter', function() {
				_this.hovering = true;

				if (_this.timeline) {

					if (!timelineInited) {
						initTimeline();
					}

					if (!_this.timeline.isActive()) {
						_this.timeline.restart();
					}
				}
			});

			$element.on('mouseleave', function() {
				_this.hovering = false;
			});

			$element.on('click', function() {
				_this.hovering = false;
				if (!_this.timeline.isActive()) {
					_this.timeline.restart();
				}
			});



			function initTimeline() {
				timelineInited = true;

				_this.timeline.addCallback(function() {
					if (_this.hovering) {
						_this.timeline.restart();
					}
				});
			}
		}

		return directive;
	}
})();
