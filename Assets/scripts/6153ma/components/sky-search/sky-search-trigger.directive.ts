(function() {
	"use strict";

	angular.module('skySearch').directive('skySearchTrigger', skySearchTriggerDirective);

	skySearchTriggerDirective.$inject = ['$rootScope', '$timeout', 'skyList'];

	function skySearchTriggerDirective($rootScope, $timeout, skyList) {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element) {
			var debounce;

			skyList.getInstance('search').then((instance) => {

				element.on('click', () => {
					var scrollY = window.pageYOffset;
					var position = {y:scrollY};
					$timeout.cancel(debounce);

					if($rootScope.searchOpen) {
						debounce = $timeout(() => {
							instance.query.keywords = '';
						}, 600);
					}

					if(!scrollY) {
						$timeout(() => {
							$rootScope.searchOpen = !$rootScope.searchOpen;
						});
					} else {
						TweenLite.to(position, 0.5, {
							y:0,
							ease:Cubic.easeInOut,
							onUpdate:() => { window.scrollTo(0, position.y); },
							onComplete:() => {
								$timeout(() => {
									$rootScope.searchOpen = !$rootScope.searchOpen;
								}, 100);
							}
						});
					}
				});
			});
		}
	}
})();
