(function() {
	'use strict';

	angular.module('skySearch').directive('skySearch',skySearchDirective);

	skySearchDirective.$inject = ['skyList', 'skyQueryString'];

	function skySearchDirective(skyList:sky.ISkyListFactory, skyQueryString) {
		var directive = {
			restrict:'E',
			templateUrl:'/sky-search/sky-search.template.html',
			scope:{},
			controller:controller,
			controllerAs:'skySearchCtrl'
		};

		controller.$inject = ['$scope', '$rootScope'];
		function controller($scope, $rootScope) {
			var _this = this;

			var api = '/umbraco/api/SiteSearchApi/Search/';

			_this.query = {
				keywords:''
			};

			var lastQuery = _this.query.keywords;

			var keywords = skyQueryString.get('keywords');

			if(keywords) {
				_this.query.keywords = keywords;
			}

			_this.loading = false;

			skyList.createInstance('search',{limit:8, api:api}).then(function(searchList) {
				_this.data = searchList.results;

				_this.showMore = function() {
					_this.loading = true;
					searchList.getNext();
				};

				$scope.$watch(()=>_this.query.keywords, function() {
					if(_this.query.keywords.length<2) {
						return searchList.empty();
					}

					if(lastQuery != _this.query.keywords) {
						_this.loading = true;
						searchList.getResults(_this.query);
					}

					lastQuery = _this.query.keywords;
				});

				$scope.$watch(() => {
					return _this.data.items.length;
				}, () => {
					_this.loading = false;
				});

			});
		}
		return directive;
	}

})();
