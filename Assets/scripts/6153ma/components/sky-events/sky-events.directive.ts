(function() {
	'use strict';

	angular.module('skyEvents').directive('skyEvents',skyEventsDirective);

	skyEventsDirective.$inject = ['skyList','skyQueryString', 'skyPath'];

	function skyEventsDirective(skyList:sky.ISkyListFactory, skyQueryString, skyPath) {
		var directive = {
			restrict:'E',
			templateUrl:'/sky-events/sky-events.template.html',
			scope:{},
			controller:controller,
			controllerAs:'skyEventsCtrl'
		};

		controller.$inject = ['$scope', '$attrs'];
		function controller($scope, $attrs) {
			var _this = this;

			_this.query = {
				keywords:''
			};

			var threads = {
				events:'eventapi',
				news:'newsapi'
			};

			var instance = $attrs.instance || 'events';

			var lastQuery = _this.query.keywords;

			var keywords = skyQueryString.get('keywords');
			if(keywords) {
				_this.query.keywords = keywords;
			}

			_this.loading = false;

			skyList.createInstance(instance, { api:`/umbraco/api/${threads[instance]}/search/`, limit:10 }).then(function(eventsList) {
				eventsList.query = angular.extend(_this.query, eventsList.query);
				_this.data = eventsList.results;

				_this.showMore = function() {
					_this.loading = true;
					eventsList.getNext();
				};

				$scope.$watch(()=>_this.query.keywords, function() {
					if(_this.query.keywords.length<2) {
						return eventsList.empty();
					}

					if(lastQuery != _this.query.keywords) {
						_this.loading = true;
						eventsList.getResults(_this.query);
					}
					lastQuery = _this.query.keywords;
				});

				$scope.$watch(() => {
					return _this.data.items.length;
				}, () => {
					_this.loading = false;
				});

				$scope.$on('$destroy', () => {
					skyList.killInstance('events');
				});
			});

		}

		return directive;

	}

})();
