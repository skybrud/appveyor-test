(function() {
	"use strict";

	angular.module('maPulsen').directive('maPulsenSection', maPulsenSectionDirecitve);

	maPulsenSectionDirecitve.$inject = ['$compile', '$templateCache'];
	function maPulsenSectionDirecitve($compile, $templateCache) {
		return {
			restrict:'E',
			compile:compile
		};

		function compile() {
			return {
				pre:(scope, element) => {
					var layout = scope.$index % 3;
					var templateUrl = `/ma-pulsen/sections/layout-${layout}.html`;
					var template = $templateCache.get(templateUrl);

					element[0].innerHTML = template;
					$compile(element.contents())(scope);
				}
			};
		}
	}
})();
