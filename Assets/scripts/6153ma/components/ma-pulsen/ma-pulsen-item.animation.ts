(function() {
	"use strict";

	angular.module('layout').animation('.pulsen-item', pulsenItemAnimation);

	function pulsenItemAnimation() {
		return {
			enter: function(element) {
				var delay = Math.floor(Math.random() * 100) + 50;
				setTimeout(element.removeClass.bind(element, 'pulsen-item'), delay);
			},
		};
	}
})();
