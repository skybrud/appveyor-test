(function() {
	"use strict";

	angular.module('maPulsen').directive('maPulsenItem', maPulsenItemDirecitve);

	maPulsenItemDirecitve.$inject = ['$compile', '$templateCache'];
	function maPulsenItemDirecitve($compile, $templateCache) {
		return {
			restrict:'E',
			compile:compile
		};

		function compile() {
			return {
				pre:(scope, element) => {
					var type = (scope.item.socialType||'').toLowerCase();
					var templateUrl = `/ma-pulsen/types/${type}.html`;
					var template = $templateCache.get(templateUrl);

					element.addClass(type);

					if(!scope.item.link) {
						element.addClass('empty');
					}

					element[0].innerHTML = template;
					$compile(element.contents())(scope);
				}
			};
		}
	}
})();
