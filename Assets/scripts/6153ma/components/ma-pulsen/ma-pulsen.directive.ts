(function() {
	"use strict";

	angular.module('maPulsen').directive('maPulsen', maPulsenDirecitve);

	maPulsenDirecitve.$inject = ['skyList', 'skyQueryString', 'skyVisible', 'skyMediaQuery', '$timeout'];

	function maPulsenDirecitve(skyList, skyQueryString, skyVisible, skyMediaQuery, $timeout) {
		var directive = {
			restrict:'E',
			scope:{},
			templateUrl:(element, attrs) => {
				var template = attrs.template ? '-' + attrs.template : '';
				return `/ma-pulsen/ma-pulsen${template}.template.html`;
			},
			controllerAs:'maPulsenCtrl',
			controller:controller,
			link:link
		};

		controller.$inject = ['$scope', '$rootScope', '$attrs'];
		function controller($scope, $rootScope, $attrs) {
			var _this = this;

			var api = '/umbraco/api/SoMeApi/GetSoMe/';

			var items = {};

			var filters = [];

			var medias = [
				'news',
				'events',
				'instagram',
				'twitter',
				'linkedin'
			];

			var layouts = [
				[
					'news',
					'news',
					'instagram',
					'twitter',
				],
				[
					'instagram',
					'twitter',
					'linkedin',
					'events',
					'events',
					'linkedin',
					'twitter'
				],
				[
					'news',
					'twitter',
					'events'
				]
			];

			this.loading = true;

			this.currentSection = 0;

			this.medias = [
				{icon:'icon-twitter', value:'twitter'},
				{icon:'icon-instagram', value:'instagram'},
				{icon:'icon-linkedin', value:'linkedin'},
				{name:'Nyheder', value:'news'},
				{name:'Aktiviteter', value:'events'},
			];

			this.sections = [];

			this.limit = (skyMediaQuery.media === 'phone' && angular.isDefined($attrs.template))  ? 1 : this.sections.length;

			this.filters = [];
			this.enabledFilters = [];

			this.toggleFilter = function(media) {
				if(_this.filters.indexOf(media) === -1) {
					_this.filters.push(media);
				} else {
					_this.filters.splice(_this.filters.indexOf(media), 1);
				}

				if(_this.filters.length === filters.length || media == 'clear') {
					_this.filters = angular.copy(filters);
					_this.enabledFilters = angular.copy(filters);
				} else {
					_this.enabledFilters = filters.filter((i) => {
						return _this.filters.indexOf(i) == -1;
					});
				}
			};

			this.filterActive = function(media) {
				if(media == 'clear') {
					return _this.filters.length === filters.length;
				} else {
					return _this.filters.indexOf(media) == -1;
				}
			};

			skyList.createInstance('pulsen',{limit:1,api:api}).then(function(pulsenList) {
				pulsenList.getResults();
				_this.offset = pulsenList.offset;

				$scope.$watchCollection(() => {return pulsenList.results;}, (results) => {
					_this.offset = pulsenList.offset;
					if(results.items[pulsenList.offset]) {
						// Cache items
						angular.forEach(results.items[pulsenList.offset], (array, media) => {
							items[media] = items[media] ? items[media].concat(array) : array;
						});

						_this.sections = buildSections(items);
						_this.pagination = results.pagination;
						_this.loading = false;
					}
				});

				_this.medias.forEach((filter) => {
					filters.push(filter.value);
				});

				_this.filters = angular.copy(filters);
				_this.enabledFilters = angular.copy(filters);

				_this.showMore = () => {
					_this.loading = true;
					pulsenList.getNext();
				};
			});

			function buildSections(result) {
				var sections = [];
				var totalItems = 0;
				var current = 0;
				var offsets = {};

				// Get totalItems
				angular.forEach(result, (item) => {totalItems += item.length});

				// Make sure all types a pressent
				medias.forEach((media) => {
					if(!result[media]) {
						result[media] = [];
					}
				});


				while(current < totalItems) {
					var layout = sections.length % 3;
					var types = layouts[layout];
					var section = [];

					types.forEach((type) => {
						let offset = offsets[type] || 0;
						let item = result[type][offset];

						if(!item) {
							item = result[type][0] || {};
							offset = 0;
						} else {
							current++;
						}

						if(!item.socialType) {
							item.socialType = type;
						}

						section.push(item);
						offsets[type] = (offset||0) + 1;
					});

					sections.push(section);
				}

				return sections;
			}
		}

		function link(scope, element, attrs, ctrl) {
			var sectionsWrapper = element[0].querySelector('.horizontal-sections');

			ctrl.next = () => {
				if(ctrl.currentSection < ctrl.sections.length) {
					var width = sectionsWrapper.children[ctrl.currentSection].clientWidth;

					ctrl.currentSection++;

					TweenLite.set(sectionsWrapper, {
						x:'-=' + width
					});
				}
			}

			ctrl.previous = () => {
				if(ctrl.currentSection > 0)  {
					ctrl.currentSection--;
					var width = sectionsWrapper.children[ctrl.currentSection].clientWidth;

					TweenLite.set(sectionsWrapper, {
						x:'+='  + width
					});
				}
			}

			skyVisible.bind(element, {recalculate:calculate});

			function calculate(index) {
				$timeout(() => {
					ctrl.limit = (skyMediaQuery.media === 'phone' && angular.isDefined(attrs.template))  ? 1 : ctrl.sections.length;
				});

				setTimeout(() => {
					var width = 0;

					if(ctrl.limit > 1 || !sectionsWrapper) {
						return;
					}

					for(var i = 0;i < ctrl.currentSection; i++) {
						width += sectionsWrapper.children[i].clientWidth;
					}

					TweenLite.set(sectionsWrapper, {
						x:-width
					});
				}, 10);
			}
		}

		return directive;
	}
})();
