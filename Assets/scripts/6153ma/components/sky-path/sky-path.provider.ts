(function() {
	'use strict';

	angular.module('skyPath').provider('skyPath', skyPathProvider);

	function skyPathProvider():any {
		// Determins if on local domain or not
		var isLocal = (/(made|testserver|rpjengaard)/i).test(window.location.host);

		// The base path
		var path = isLocal ? '' : 'http://6153ma.testserver.nu';
		//var path = isLocal ? '' : 'http://6153ma.rpjengaard';

		/*
		 * Change the local state
		 */
		this.setLocal = function(local) {
			isLocal = local;
		};

		/*
		 * Change the path
		 */
		this.setPath = function(newPath) {
			path = newPath;
		};

		this.$get = function() {
			return {
				get: function() {
					return path;
				}
			};
		};
	}

})();
