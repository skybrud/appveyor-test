(function() {
	'use strict';

	angular.module('skyGa').provider('skyGa', skyGaProvider);
	function skyGaProvider() {
		var trackingiId = '', //UA-xxxxxxx-x - using empty (not setting at config-timg) string to enable debug-/dev-mode...
			configObject; //domain.com

		var properties = {
			debug:true,
			trackingId:'',
			configObject:''
		};

		this.setTrackingId = (value) => {
			properties.trackingId = value;
			properties.debug = !value;
		};

		this.setConfigObject = function(value) {
			properties.configObject = value;
		};

		this.$get = function() {
			return properties;
		};

		return this;
	}

})();
