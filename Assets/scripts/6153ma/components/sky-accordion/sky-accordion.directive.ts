/* global angular */
(function () {
	'use strict';

	/**
	 * Directive: skyAccordion
	 * Directive for making an accordion.
	 *
	 * Each skyAccordion must contain a skyAccordionToggle and a skyAccordionContent.
	 *
	 * This directive has scope methods for open, close and toggle. These are all
	 * exposed to the controller as well. The controller also has an internal method
	 * for registering the content.
	 *
	 * The directive starts out by calling close on itself (with the noAni-flag).
	 *
	**/

	angular.module('skyAccordion').directive('skyAccordion',skyAccordion);

	skyAccordion.$inject = ['skyAccordionTopScrollOffset','$window', 'skyMediaQuery'];

	function skyAccordion(skyAccordionTopScrollOffset, $window, skyMediaQuery) {

		var directive = {
			restrict:'A',
			scope:true,
			controllerAs:'skyAccordion',
			controller:skyAccordionCtrl,
			link:link
		};

		skyAccordionCtrl.$inject = ['$scope','$element', 'skyVisible'];

		function skyAccordionCtrl($scope, $element, skyVisible) {
			var _this = this;

			var line = new TimelineMax();

			_this.isOpen = !$element.hasClass('group');

			/*
			 * Open the collapser by animating (css transition or animation) the height of the container to the height
			 * of the content. When the transition is complete reset the container styles
			 */
			_this.open = function(noAni) {
				if(_this.isOpen && !noAni) {
					return;
				}

				$element.addClass('open');
				TweenLite.to(_this.contentContainer, 0.4, {
					height:_this.contentContainer.children[0].clientHeight,
					onComplete:() => {
						TweenLite.set(_this.contentContainer, {height:'auto'});
						skyVisible.recalculate();
						skyVisible.checkViews(false, false);
					},
				});
				_this.isOpen=true;
			};

			/*
			 * Close the collapser by animating the container height to 0 with overflow hidden. (start out by setting the
			 * height of the container to the measured height of the content, since we can't animate from auto).
			 *
			 * When the noAni-flag is on, skip the measuring part (transition from auto => no animation).
			 *
			 * Start by removing the transition-end listener, so it doesn't do unintentional stuff,
			 * when closing before the open-transition is complete.
			 */
			_this.close = function(noAni) {
				if(!_this.isOpen) {
					return;
				}

				$element.removeClass('open');
				TweenLite.to(_this.contentContainer, 0.4, {
					height:0,
					onComplete:() => {
						skyVisible.recalculate();
						skyVisible.checkViews(false, false);
					}
				});

				_this.isOpen=false;
			};

			/*
			 * Toggle the animation by calling close if it is opened, or vice versa.
			 */
			_this.toggle = function() {
				if (_this.isOpen) {
					_this.close();
				} else {
					_this.open();
				}
			};

			/*
			 * Register the accordion content
			 */
			_this.addContent = function(ele) {
				_this.contentContainer=ele[0];
				_this.content = ele[0].childNodes[0];
			};

			/*
			 * Expose relevant methods to the $scope
			 */
			$scope.open = _this.open;
			$scope.close = _this.close;
			$scope.toggle = _this.toggle;
		}

		function link(scope,element,attrs, ctrl) {
			var init = function() {
				scope.toggle();
				checkHash();
			};

			var checkHash = function() {
				if ($window.location.hash && attrs.skyAccordion === $window.location.hash.substr(1)) {
					var parent = findParentByTag(element[0], 'div');
					var parentScope = <any>angular.element(parent).scope();

					/**
					 * Scroll to and open
					 */
					element[0].focus();
					scope.open(true);

					var scrollTo = element[0].getBoundingClientRect().top + $window.scrollY;
					setTimeout(function() {
						if(parentScope.open) {
							parentScope.open(true);
						}
						$window.scroll(0, scrollTo - skyAccordionTopScrollOffset);
					},200);
				}
			};

			init();

			angular.element($window).on('hashchange', checkHash);

			scope.$on('$destroy', function() {
				angular.element($window).off('hashchange', checkHash);
			});

			/**
			 * Findes parent tag
			 */
			function findParentByTag(node, tag) {
				var parent = node.parentElement;

				if(parent.tagName.toLowerCase() === tag.toLowerCase()) {
					return parent;
				}

				return findParentByTag(parent, tag);
			}
		}

		return directive;

	}

})();
