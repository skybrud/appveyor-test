/* global angular */
(function () {
	'use strict';

	angular.module('skyAccordion',['core']);
	angular.module('skyAccordion').constant('skyAccordionTopScrollOffset', 75);

})();
