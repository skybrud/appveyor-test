(function() {
	"use strict";

	angular.module('layout').directive('maSectionClick', maSectionClickDirective);

	function maSectionClickDirective() {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element) {
			element.on('click', clickAnchor);

			function clickAnchor(e) {
				var anchor = element[0].querySelector('a');
				element.off('click', clickAnchor);
				anchor.click();
			}
		}
	}
})();
