(function() {
	"use stickt";

	angular.module('layout').directive('maStickyFooter', maStickyFooterDirective);

	maStickyFooterDirective.$inject = ['skyVisible', 'skyMediaQuery'];

	function maStickyFooterDirective(skyVisible, skyMediaQuery) {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element, attrs) {
			var _element = element[0];
			var _sections = _element.querySelectorAll('section');
			var windowHeight = window.innerHeight;
			var documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);

			skyVisible.bind(element, {recalculate:calculate, shouldUpdate:shouldUpdate, cache:false}, (values, dimensions, scroll) => {
				if(scroll.y + windowHeight > documentHeight - dimensions.height) {
					TweenLite.set(_sections[1], {y:0});
				} else {
					TweenLite.set(_sections[1], {y:1000});
				}

				if(documentHeight - 200 < scroll.y + windowHeight) {
					element.addClass('appear');
				} else {
					element.removeClass('appear');
				}
			});

			function shouldUpdate() {
				return skyMediaQuery.media !== 'phone';
			}

			function calculate() {
				if(skyMediaQuery.media === 'phone') {
					TweenLite.set([_element, _sections[1]], {clearProps:'all'});
				} else {
					_sections = _element.querySelectorAll('section');
					windowHeight = window.innerHeight;
					TweenLite.set(_element, {paddingBottom:_sections[1].clientHeight});
					documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
				}
			}
		}
	}
})();
