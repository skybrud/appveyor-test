(function() {
	"use strict";

	angular.module('layout').directive('maHeader', maHeaderDirective);

	maHeaderDirective.$inject = ['skyVisible'];

	function maHeaderDirective(skyVisible) {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element) {
			var _element = element[0];

			skyVisible.bind(_element, {recalculate:calculate}, (values, dimensions) => {
				if(values.progress < 0.1) {
					element.addClass('appear');
				} else {
					element.removeClass('appear');
				}
			});

			function calculate() {
				skyVisible.checkViews(_element, false);
			}
		}
	}
})();
