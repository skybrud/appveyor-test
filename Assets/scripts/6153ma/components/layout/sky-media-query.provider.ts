(function() {
	"use strict";

	angular.module('layout').provider('skyMediaQuery', skyMediaQueryProvider);

	skyMediaQueryProvider.$inject = [];

	function skyMediaQueryProvider():any {

		var _this= this;

		// Default breakpoints
		_this.breakpoints = {
			phone: '(max-width:800px)',
			tablet: '(min-width:800px) AND (max-width:1180px)',
			desktop: '(min-width:1180px)'
		};

		// Window element event
		_this.event = true;

		// Bind skyMediaQuery to rootscope
		_this.rootScope = true;

		// Trigger a broadcast
		_this.broadcast = true;

		// Trigger a digest cycle on match
		_this.digest = true;

		_this.$get = get;

		get.$inject = ['$rootScope', '$timeout'];

		function get($rootScope, $timeout) {
			var first = false;

			// Returned object
			var skyMediaQuery = angular.copy(_this);

			// The current media query. mobile||tablet etc.
			skyMediaQuery.media = '';

			/*
			 * Mathes either a media query or a
			 * specific media
			 */
			skyMediaQuery.match = function(query):any {
				if(query === skyMediaQuery.media || query === skyMediaQuery.breakpoints[this.media]) {
					return true;
				} else if(skyMediaQuery.breakpoints[query]) {
					return matchQuery(skyMediaQuery.breakpoints[query]);
				}

				return matchQuery(query);
			};

			/*
			 * Compares against the current media
			 */
			skyMediaQuery.is = function(media) {
				return skyMediaQuery.media === media;
			};

			/*
			 * Matches a givin media query
			 */
			var matchQuery = function(query):any {
				return window.matchMedia(query).matches;
			};

			/*
			 * Updates the current media.
			 */
			var updateMedia = function(mediaQuery, media) {
				if(mediaQuery.matches) {
					skyMediaQuery.media = media;

					if(first) {
						broadcastMedia();
					}
				}
			};

			/*
			 * [All broadcasts etc are placed in here]
			 */
			var broadcastMedia = function() {
				var media = skyMediaQuery.media;

				// Window event [true]
				if(skyMediaQuery.event) {
					angular.element(window).triggerHandler('skyMediaQuery:match', media);
				}

				// rootScope event [false]
				if(skyMediaQuery.broadcast) {
					$rootScope.$broadcast('skyMediaQuery:match', media);
				}

				// Trigger digest from $timeout to prevent
				// multiple digest at the same time
				if(skyMediaQuery.digest){
					$timeout(function() {},0);
				}
			};

			/*
			 * Set matchMedia listeners
			 */
			var init = function() {

				for(var media in skyMediaQuery.breakpoints) {
					// Query
					var query = skyMediaQuery.breakpoints[media];

					// Match media object
					var matchMedia = window.matchMedia(query);

					// Add eventlistener
					matchMedia.addListener(updateMedia.bind(undefined, matchMedia, media));

					// Update media query
					if(matchMedia.matches) {
						updateMedia(matchMedia, media);
					}
				}

				// Bind to rootScope
				if(skyMediaQuery.rootScope){
					$rootScope.skyMediaQuery = skyMediaQuery;
				}

				// Notify
				broadcastMedia();

				// Make sure listeners notify
				first = true;

				return skyMediaQuery;
			};

			angular.element(window).on('load', broadcastMedia);

			// Exposed methods
			return init();
		}
	}
})();


