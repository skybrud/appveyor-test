(function() {
	"use strict";

	angular.module('layout').directive('maLine', maLineDirective);

	function maLineDirective() {
		return {
			restrict:'E',
			transclude:true,
			templateUrl:'/layout/ma-line.template.html',
		};
	}
})();
