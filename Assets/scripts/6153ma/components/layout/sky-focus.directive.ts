(function() {
	"use strict";

	angular.module('layout').directive('skyFocus', skyFocusDirective);


	function skyFocusDirective() {
		var debounce;

		var directive = {
			compile:compile
		};

		function compile() {
			return {
				// Make sure it focusable
				pre: function(scope, element) {
					element[0].tabindex = -1;
				},
				post: function(scope,element,attrs) {
					scope.$watch(attrs.skyFocus, function(focus) {
						if(focus || focus === undefined) {
							setFocus(element[0]);
						}
					});

					function setFocus(el) {
						clearTimeout(debounce);

						setTimeout(function() {
							el.focus();
						}, 10);

						debounce = setTimeout(function() {
							if(el !== document.activeElement) {
								setFocus(el);
							}
						}, 200);
					}
				}
			};
		}

		return directive;
	}
})();
