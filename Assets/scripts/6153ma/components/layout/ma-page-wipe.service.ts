(function() {
	"use strict";

	angular.module('layout').service('maPageWipe', maPageWipeDirective);

	maPageWipeDirective.$inject = ['$animate', '$q', '$rootScope', '$timeout'];

	function maPageWipeDirective($animate, $q, $rootScope, $timeout) {
		var _wipe = angular.element(document.querySelector('.wipe'));

		var stateSuccess = true;
		var wipeStall = false;
		var wipeVisible = true;

		this.wipeIn = wipeIn;
		this.wipeOut = wipeOut;

		var enterDeferred = $q.defer();;
		var leaveDeferred = $q.defer();;

		function wipeIn(delay){

			if(enterDeferred.promise.$$state.status) {
				enterDeferred = $q.defer();
			}

			if(wipeVisible || wipeStall) {
				return enterDeferred.promise;
			}

			wipeStall = true;
			$timeout(() => {
				$animate.addClass(_wipe, 'enter').then(wipe);
			}, delay);

			function wipe() {
				_wipe.removeClass('leave');
				window.scrollTo(0,0);
				wipeStall = false;
				wipeVisible = true;
				wipeOut();
				$rootScope.$broadcast('wipeIn');
				enterDeferred.resolve();
				enterDeferred = $q.defer();
			}

			return enterDeferred.promise;
		}

		function wipeOut() {

			if(leaveDeferred.promise.$$state.status) {
				leaveDeferred = $q.defer();
			}

			if(!wipeVisible || !stateSuccess) {
				return leaveDeferred.promise;
			}

			document.body.classList.remove('init');

			$timeout(() => {
				$animate.addClass(_wipe, 'leave').then(wipe);
				_wipe.removeClass('enter');
				stateSuccess = false;
			}, 500);

			function wipe() {
				wipeVisible = false;
				$rootScope.$broadcast('wipeOut');
				leaveDeferred.resolve();
				leaveDeferred = $q.defer();
			};

			return leaveDeferred.promise;
		}

		$rootScope.$on('$stateChangeSuccess', () => {
			stateSuccess = true;
			this.wipeOut();
		});
	}
})();
