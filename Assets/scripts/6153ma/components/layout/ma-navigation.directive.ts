(function() {
	"use strict";

	angular.module('layout').directive('maNavigation', maNavigationDirective);

	maNavigationDirective.$inject = ['skyVisible', 'skyMediaQuery'];

	function maNavigationDirective(skyVisible, skyMediaQuery) {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element) {
			var _element = element[0];
			var _search;
			var _searchLine;

			calculate();

			skyVisible.bind(element, {recalculate:calculate,cache:false, shouldUpdate:shouldUpdate}, (values, dimensions, scroll) => {
				var progress = Math.min(Math.max(scroll.y / dimensions.height, 0), 1);

				if(!_searchLine) {
					calculate();
				}

				TweenLite.set(_searchLine, {
					strokeDashoffset:-15 * progress,
				});

				TweenLite.set(_search, {
					yPercent:-50 * progress
				});

				if(progress > 0.5) {
					element.addClass('hide');
				} else {
					element.removeClass('hide');
				}
			});

			function calculate() {
				_element = element[0];
				_search = _element.querySelector('.search');
				_searchLine = _search.querySelector('.svg-line line');
			}

			function shouldUpdate() {
				return skyMediaQuery.media !== 'phone';
			}
		}
	}

})();
