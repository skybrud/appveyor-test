(function() {
	"use strict";

	angular.module('layout').run(layoutRun);

	layoutRun.$inject = ['skyVisible', 'skyMediaQuery', '$rootScope'];

	function layoutRun(skyVisible, skyMediaQuery, $rootScope) {
		window.addEventListener('load', () => {
			skyVisible.recalculate();
			skyVisible.checkViews(false, false);
		});

		$rootScope.$on('$stateChangeSuccess', recalculate);
		$rootScope.$on('viewEnter', recalculate);

		function recalculate() {
			setTimeout(function() {
				skyVisible.recalculate();
				skyVisible.checkViews(false, false);
			}, 100);
		}
	}
})();
