(function() {
	"use strict";

	angular.module('layout').directive('maTextContain', maTextContainDirective);

	maTextContainDirective.$inject = ['skyVisible'];

	function maTextContainDirective(skyVisible) {
		return {
			restrict:'E',
			transclude:true,
			template:'<ng-transclude></ng-transclude>',
			link:link
		};

		function link(scope, element, attrs) {
			var _element = element[0];
			var _content = _element.querySelector('ng-transclude');

			var boundingBox;
			var contentBoundingBox;

			var stall = false;

			skyVisible.bind(element, {recalculate:calculate});

			function calculate() {
				if(stall) {
					return;
				}

				TweenLite.set(_content, {clearProps:'all'});
				var height = _element.clientHeight;
				var newHeight = 0;
				var scale = 0;

				boundingBox = _element.getBoundingClientRect();
				contentBoundingBox = _content.getBoundingClientRect();

				scale = boundingBox.width / contentBoundingBox.width;

				TweenLite.set(_content, {scale:scale.toFixed(2)});

				newHeight = _content.getBoundingClientRect().height;

				TweenLite.set(_element, {height:newHeight});

				window.scrollBy(0, newHeight - height);

				stall = true;

				skyVisible.recalculate(true);
				setTimeout(() => {
					stall = false;
				}, 40);
			}

			function resize(font=0) {
			}
		}
	}
})();
