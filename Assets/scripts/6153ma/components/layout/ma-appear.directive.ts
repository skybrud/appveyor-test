(function() {
	"use strict";

	angular.module('layout').directive('maAppear', maAppearDirective);

	maAppearDirective.$inject = ['skyVisible'];

	function maAppearDirective(skyVisible) {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element, attr) {
			var _element = element[0];
			var target = Number(attr.maAppear || 0.05);

			skyVisible.bind(_element, {foldOffset:false, recalculate:calculate}, (values, dimensions, scroll) => {
				var bottom = dimensions.top + dimensions.height;

				if(values.progress > target) {
					element.addClass('appear');
				} else {
					//element.removeClass('appear');
				}
			});

			function calculate() {
				skyVisible.checkViews(_element, false);
			}
		}
	}
})();
