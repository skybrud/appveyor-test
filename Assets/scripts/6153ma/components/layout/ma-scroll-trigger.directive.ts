(function() {
	"use strict";

	angular.module('layout').directive('maScrollTrigger', maScrollTriggerDirective);

	function maScrollTriggerDirective() {
		return {
			restrict:'A',
			link:link
		};

		function link(scope, element) {
			element.on('click', () => {
				var bounding = element[0].getBoundingClientRect();
				var position = {y:window.pageYOffset};

				var target = bounding.bottom;

				TweenLite.to(position, 0.5, {
					y:target,
					onUpdate:() => {
						window.scrollTo(0, position.y);
					},
					ease:Cubic.easeInOut
				});
			});
		}
	}
})();
