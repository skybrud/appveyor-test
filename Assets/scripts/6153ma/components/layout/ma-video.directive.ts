(function() {
	"use strict";

	angular.module('layout').directive('maVideo', maVideoDirective);

	maVideoDirective.$inject = ['skyMediaQuery'];

	function maVideoDirective(skyMediaQuery) {
		return {
			restrict:'E',
			link:link
		};

		function link(scope, element, attrs) {
			var video = document.createElement('video');

			angular.forEach(attrs.$attr, (attr) => {
				video[attr] = attrs[attr] || true;
			});

			if(skyMediaQuery.media !== 'phone') {
				element.replaceWith(video);
			}
		}
	}
})();
