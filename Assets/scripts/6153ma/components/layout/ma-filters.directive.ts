(function() {
	"use strict";

	angular.module('layout').directive('maFilters', maFiltersDirective);

	maFiltersDirective.$inject = ['skyList'];

	function maFiltersDirective(skyList) {
		var directive =  {
			restrict:'E',
			scope: {
				filters:'=',
				instance:'@',
				enableHasBeen:'='
			},
			templateUrl:'/layout/ma-filters.template.html',
			bindToController:true,
			controllerAs:'MaFilters',
			controller:controller
		};

		controller.$inject = ['$scope'];

		function controller($scope) {
			this.selectedOptions = [];

			this.options;

			this.filter = '';

			this.query = {
			};

			this.hasBeen = "Vis afholdte";

			skyList.getInstance(this.instance).then((instance) => {
				instance.query = angular.extend(instance.query, this.query);

				$scope.$watch(() => {return this.query;}, (query) => {
					instance.getResults(query);
				}, true);

			});

			this.clearOptions = () => {
				this.selectedOptions = [];
				this.options = {};
				delete this.query[this.filter];
				this.filter = '';
			}

			this.setOptions = (filter) => {
				this.clearOptions();

				this.filter = filter;
				this.filters.forEach((item) => {
					if(item.alias === filter) {
						this.options = item;
					}
				});
			}

			this.toggleOption = (id) => {
				if(this.selectedOptions.indexOf(id) !== -1) {
					this.selectedOptions.splice(this.selectedOptions.indexOf(id), 1);
				} else {
					this.selectedOptions.push(id);
				}

				if(this.selectedOptions.length) {
					this.query[this.filter] = this.selectedOptions.join(',');
				} else {
					delete this.query[this.filter];
				}
			}

			this.toggleHasBeen = () => {
				this.query.hasBeen = !this.query.hasBeen;
				this.hasBeen = this.query.hasBeen ? 'Vis kommende' : 'Vis afholdte'
				console.log(this, !!this.query.hasBeen);
			}
		}

		return directive;
	}
})();
