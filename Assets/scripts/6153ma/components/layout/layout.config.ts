(function() {
	"use strict";

	angular.module('layout').config(config);

	config.$inject = ['skyVisibleViewsProvider', 'skyVisibleProvider'];

	function config(skyVisibleViewsProvider) {
		skyVisibleViewsProvider.views['stickyBottom'] = function(element, dimensions, scrollPosition, windowHeight) {
			var bottom = dimensions.height + dimensions.top;
			var parentBottom = windowHeight + scrollPosition.y;
			return bottom <= parentBottom;
		};

		skyVisibleViewsProvider.views['inView'] = function(element, dimensions, scrollPosition, windowHeight) {
			var range = (dimensions.height + dimensions.top) - windowHeight;

			return Math.min(Math.max(scrollPosition.y / range, 0), 1);
		};
	}
})();

