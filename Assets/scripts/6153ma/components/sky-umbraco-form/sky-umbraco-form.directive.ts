(function() {
	'use strict';

	angular.module('skyUmbracoForm').directive('skyUmbracoForm', skyUmbracoFormDirective);

	skyUmbracoFormDirective.$inject = ['skyPath'];
	function skyUmbracoFormDirective(skyPath) {
		var directive = {
			restrict: 'E',
			scope:{
				formGuid: '='
			},
			bindToController: true,
			controller: skyUmbracoFormController,
			controllerAs: 'skyUmbracoFormCtrl',
			templateUrl: '/sky-umbraco-form/sky-umbraco-form.template.html'
		};

		skyUmbracoFormController.$inject = ['$http', 'skyPath', '$element', '$timeout', 'skySticky'];
		function skyUmbracoFormController($http, skyPath, $element, $timeout, skySticky) {
			var _this = this;
			var wrapper = $element[0].querySelector('sky-compile');

			$element.addClass('loading');

			var path = skyPath.get();

			$http({
				method: 'GET',
				url: path + '/formrender/',
				withCredentials: true,
				params: {
					guid: _this.formGuid
				}
			}).then((result) => handleMarkup(result.data));

			function handleMarkup(markup) {
				var tl = new TimelineLite();
				var target = wrapper.getBoundingClientRect().top + window.pageYOffset - 20;
				var scrollY = window.pageYOffset;
				var scrollProgress = {y:0};

				if(_this.markup) {
					tl.set(wrapper, {
						display:'block',
						overflow:'hidden',
					});

					tl.to(wrapper, 1, {
						height:0,
						opacity:0,
						ease:Cubic.easeInOut,
						onComplete:handle
					});

					tl.to(scrollProgress, 0.5, {
						y:1,
						ease:Cubic.easeInOut,
						onUpdate: () => {
							var progress = ((target - skySticky.getOffset()) - scrollY) * scrollProgress.y;
							window.scrollTo(0, progress + scrollY)
						}
					}, 0.1);
				} else {
					handle();
				}

				function handle() {
					$timeout(() => {
						$element.removeClass('loading');
						_this.markup = markup;
					});

					setTimeout(()=>{
						var scripttags = $element.find('script');

						angular.forEach(scripttags, function(scripttag) {
							if (scripttag.src) {
								var s = document.createElement('script');
								s.src = scripttag.src;
								document.body.appendChild(s);
							} else {
								eval(scripttag.innerHTML);
							}
						});

						tl.set(wrapper, {height:'auto', opacity:1, display:'block'});
						tl.from(wrapper, 0.5, {
							height:0,
							opacity:-1,
							ease:Cubic.easeInOut
						});

						var form = $element.find('form');

						// specialcase for dealing with multistep-forms
						var buttons = angular.element($element[0].querySelector('input[type=submit]'));
						var direction = '';
						angular.forEach(buttons, function(button) {
							if(button.name === 'next' || button.name === 'submit') {
								direction = button.name;
							}
							angular.element(button).on('click', function() {
								direction = button.name;
							});
						});

						form.on('submit', function(event) {
							event.preventDefault();

							var formData:any = new FormData();

							angular.forEach(form[0].querySelectorAll('input, textarea, select'), function(field) {
								if (field.type == 'submit' && field.name != direction) {
									// specialcase for dealing with multistep-forms
									return;
								}

								//handle specialcase: fileupload
								if(field.type === 'file') {
									if (formData.fake) {
										alert('Your browser does not support file-uploads. Remaining fields are submitted!');
										return;
									}
									Array.prototype.forEach.call(field.files, function(file) {
										formData.append(field.name, file);
									});
									return;
								}

								//handle specialcase: checkbox+radio
								if (field.type === 'checkbox' || field.type === 'radio')  {
									if (field.checked) {
										formData.append(field.name, field.value);
									}
									return;
								}

								// all other fields
								formData.append(field.name, field.value);
							});

							$element.addClass('loading');

							$http({
								method: 'POST',
								data: formData,
								withCredentials: true,
								url: path + form.attr('action'),
								headers: {
									'Content-Type': undefined
								}
							}).then((result) => handleMarkup(result.data));

							return false;
						});

					}, 0);

				}

			}

		}

		return directive;
	}
})();
