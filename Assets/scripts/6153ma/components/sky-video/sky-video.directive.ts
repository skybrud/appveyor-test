/* global angular */
(function () {
	'use strict';

	/**
	 * Directive: skyVideo
	 *
	 * Takes most kinds of url's for youtube or vimeo videos
	 *
	 * Shows poster with custom play-button for fast load,
	 * replaces with real video once clicked.
	 *
	 * Usage: <div sky-video="https://www.youtube.com/watch?v=mxzgwJ8tSE0"></div>
	 *
	 **/

	angular.module('skyVideo').directive('skyVideo',skyVideo);

	skyVideo.$inject = ['skyVideoHelper', 'skyPath'];

	function skyVideo(skyVideoHelper, skyPath) {
		var directive = {
			restrict:'A',
			template:'<div style="padding-top:44.44%;"><button no-uniform title="Play video" sky-svg="play"></button></div>', /* 56.25% because 16:9 */
			link:link
		};

		function link(scope,element,attributes) {
			var videoElement = <any>angular.element('<iframe />');
            var autoplay = attributes.autoplay !== undefined;
            var loop = attributes.loop !== undefined;
            var attrPoster = attributes.skyPoster;

            var options = {
                loop:loop ? 1 : 0
            };

			if(attrPoster) {
				element.css({backgroundImage:'url('+skyPath.get()+attrPoster+')'});
			}

			skyVideoHelper.getObjFromString(attributes.skyVideo, options).then(function(video) {
				if(!attrPoster) {
					element.css({backgroundImage:'url('+(video.poster)+')'});
				}

				element.one('click', function(e) {
					e.preventDefault();
                    play();
					return false;
				});

                (autoplay ? play : angular.noop)();

                function play() {
                    element.addClass('playing');
                    videoElement.attr('src',video.embed);
                    videoElement.css({cssText:'position:absolute;top:0;bottom:0;left:0;right:0;width:100%;height:100%'});
                    videoElement.attr('allowfullscreen','allowfullscreen');
                    element.append(videoElement);
                }
			}, function(err) {
				throw(err);
			});
		}

		return directive;
	}

})();
