interface CustomEasePlugin {
    create(name: string, points: any[]): any;
    byName(name: string): any;
}
declare var CustomEase: CustomEasePlugin;