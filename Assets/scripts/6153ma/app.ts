(function () {
	'use strict';

	angular.module('app',[
		'ngAnimate',
		'core',
		'skySvg',
		'skySpa',
		'skyGrid',
		'skyLightbox',
		'skyCrop',
		'skyVideo',
		'skySticky',
		'skyAccordion',
		'skyVisible',
		'skyList',
		'skySearch',
		'skyEvents',
		'skyform',
		'skyAgo',
		'skyUmbracoForm',
		'skyGa',
		'skyMailchimp',
		'maTitle',
		'maPulsen',
		'maIcon',
		'maForm',
		'ui.bootstrap'
	]);


})();






