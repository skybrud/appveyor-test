(function() {
	"use strict";

	angular.module('app').config(appConfig);

	appConfig.$inject = ['uibDatepickerPopupConfig','uibDatepickerConfig', 'skyGaProvider'];

	function appConfig(uibDatepickerPopupConfig,datepickerConfig, skyGaProvider) {
		skyGaProvider.setTrackingId('UA-66293557-1');
		skyGaProvider.setConfigObject('http://made.dk/');

		datepickerConfig.startingDay = 1;

		if (window.labels) {
			uibDatepickerPopupConfig.currentText = window.labels.datepickerCurrentText;
			uibDatepickerPopupConfig.clearText = window.labels.datepickerClearText;
			uibDatepickerPopupConfig.closeText = window.labels.datepickerCloseText;
		} else {
			uibDatepickerPopupConfig.currentText = 'I dag';
			uibDatepickerPopupConfig.clearText = 'Ryd';
			uibDatepickerPopupConfig.closeText = 'Luk';
		}
	};

})();

