var projectNumb='6153ma';
module.exports = function(grunt) {

	// Just In Time Grunt: auto-loads necessary plugins - replacing rows of loadNpmTasks.
	require('jit-grunt')(grunt,{
		// Custom plugin mappings go here.
		ngtemplates:'grunt-angular-templates',
		svgcombine:'grunt-svg-combine',
	});

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
			toMarkup: {
				files: [{
					expand: true,
					cwd: 'assets/scss/'+projectNumb+'/',
					src: ['**/*.scss', '!_*.scss'],
					dest: '.tmp/styles/markup',
					ext: '.css'
				}],
				options: {
					outputStyle: 'nested',
					sourceMap: true
				}
			},
			toDev: {
				files: [{
					expand: true,
					cwd: 'assets/scss/'+projectNumb+'/',
					src: ['**/*.scss', '!_*.scss'],
					dest: '.tmp/styles/dev',
					ext: '.css'
				}],
				options: {
					outputStyle: 'compressed'
				}
			}
		},

		autoprefixer: {
			options: {
				browsers: ['last 2 version', 'ie 9']
			},
			toMarkup: {
				files:[{
					expand: true,
					cwd: '.tmp/styles/markup/',
					src:'*.css',
					dest: 'markup/css/'+projectNumb+'/'
				}],
				options: {
					map:true
				}
			},

			toDev: {
				files:[{
					expand: true,
					cwd: '.tmp/styles/dev/',
					src:'*.css',
					dest: 'Dev/web/css/'+projectNumb+'/',
				}],
				options: {
					map:false
				}
			}
		},

		tsconfig: {
			make: {
				rootDir:'Assets/scripts/'+projectNumb+'/',
				options: {
					additionalOptions: {
						compilerOptions:{
							"sourceMap": true
						},
					},
					filesGlob: [
						'definitions/**/*.d.ts',
						'app.ts',
						'components/**/*.module.ts',
						'components/**/*.provider.ts',
						'components/**/*.config.ts',
						'components/**/*.ts',
						'!components/**/*.spec.ts',
					]
				}
			}
		},

		ngtemplates: {
			options: {
				prefix: '/',
				htmlmin : {
				    collapseBooleanAttributes: true,
				    collapseWhitespace: true,
				    removeAttributeQuotes: true,
				    removeComments: true,
				    removeEmptyAttributes: true,
				    removeRedundantAttributes: true,
				    removeScriptTypeAttributes: true,
				    removeStyleLinkTypeAttributes: true
				}
			},
		    layout: {
		    	cwd: 'assets/scripts/'+projectNumb+'/components/',
		        src: '**/*.html',
		        dest: '.tmp/ngtemplates/'+projectNumb+'/templates.js'
		    },
		    skySvg: {
		    	cwd: '.tmp/svgs/'+projectNumb+'/',
		        src: '**/*.svg',
		        dest: '.tmp/ngtemplates/'+projectNumb+'/svgs.js'
		    }
		},

		ts: {
			toTmp: {
				options:{
					sourceMap:true
				},
				out:'.tmp/ts/'+projectNumb+'/scripts.js',
				files: [{
					src: [
						'assets/scripts/'+projectNumb+'/definitions/**/*.d.ts',
						'assets/scripts/'+projectNumb+'/app.ts',
						'assets/scripts/'+projectNumb+'/**/*.module.ts',
						'assets/scripts/'+projectNumb+'/**/*.provider.ts',
						'assets/scripts/'+projectNumb+'/**/*.config.ts',
						'assets/scripts/'+projectNumb+'/**/*.ts',
						'!assets/scripts/'+projectNumb+'/**/*.spec.ts'
					]
				}]

			}
		},

		concat_sourcemap: {
			toMarkup: {
				files: [{
					src: [
						'assets/scripts/'+projectNumb+'/plugins/**/*.js',
						'.tmp/ts/'+projectNumb+'/scripts.js',
						'.tmp/ngtemplates/'+projectNumb+'/templates.js',
						'.tmp/ngtemplates/'+projectNumb+'/svgs.js',
						'assets/scripts/'+projectNumb+'/app.bootstrap.js'
					],
					dest: 'markup/scripts/'+projectNumb+'/scripts.js'
				}]
			}
		},

		uglify: {
			toDev: {
				options: {
					preserveComments:false
				},
				files: [{
					src: [
						'assets/scripts/'+projectNumb+'/plugins/**/*.js',
						'.tmp/ts/'+projectNumb+'/scripts.js',
						'.tmp/ngtemplates/'+projectNumb+'/templates.js',
						'.tmp/ngtemplates/'+projectNumb+'/svgs.js',
						'assets/scripts/'+projectNumb+'/app.bootstrap.js'
					],
					dest: 'Dev/web/scripts/'+projectNumb+'/scripts.js'
				}]
			}
		},

		copy: {
			toMarkup: {
				files: [
					{
						// copies videos
						expand: true,
						cwd: 'assets/videos/'+projectNumb+'/',
						src: ['**/*.*'],
						dest: 'markup/videos/'+projectNumb+'/'
					},
					{
						// htaccess + php-stufftopy:
						expand: true,
						cwd: 'Assets/html/'+projectNumb+'/',
						src: ['.htaccess','*.php'],
						dest: 'markup/'
					},
					{
						// copies webfonts
						expand: true,
						cwd: 'assets/fonts/'+projectNumb+'/',
						src: ['**/*.*'],
						dest: 'markup/css/'+projectNumb+'/fonts/'
					},
					{
						// copies other images (that imagemin doesn't touch)
						expand: true,
						cwd: 'assets/images/'+projectNumb+'/',
						src: ['**/*.*','!**/*.{jpg,jpeg,png,gif}'],
						dest: 'markup/img/'+projectNumb+'/'
					}
				]
			},
			toDev: {
				files: [
					{
						// copies videos
						expand: true,
						cwd: 'assets/videos/'+projectNumb+'/',
						src: ['**/*.*'],
						dest: 'Dev/web/videos/'+projectNumb+'/'
					},
					{
						expand: true,
						cwd: 'assets/fonts/'+projectNumb+'/',
						src: ['**/*.*'],
						dest: 'Dev/web/css/'+projectNumb+'/fonts/'
					},
					{
						expand: true,
						cwd: 'assets/images/'+projectNumb+'/',
						src: ['**/*.*','!**/*.{jpg,jpeg,png,gif}'],
						dest: 'Dev/web/img/'+projectNumb+'/'
					}
				]
			},
		},

		imagemin: {
			toMarkup: {
				options: {
					optimizationLevel: 4
				},
				files: [
					{
						expand: true,
						cwd: 'assets/images/'+projectNumb+'/',
						src: ['**/*.{png,jpeg,jpg,gif}'],
						dest: 'markup/img/'+projectNumb+'/'
					}
				]
			},
			toDev: {
				options: { optimizationLevel: 4 },
				files: [
					{
						expand: true,
						cwd: 'assets/images/'+projectNumb+'/',
						src: ['**/*.{png,jpeg,jpg,gif}'],
						dest: 'Dev/web/img/'+projectNumb+'/'
					}
				]
			}
		},

		bake: {
			toMarkup: {
				options:{
					parsePattern: /\{\{\{\s?([\.\-\w]*)\s?\}\}\}/g
				},
				files: [
					{
						expand: true,
						cwd: 'assets/html/'+projectNumb+'/',
						src: ['*.html'],
						dest: 'markup/'
					}
				]
			}
		},

		svgcombine: {
			options:{
				filter: function(svg,fileName,filePath){ // global filter
					// Add classname
					svg.attr('class', 'svg-' + fileName);
				},
				svgo: {
					plugins: [
						{'mergePaths':false},
						{'convertShapeToPath':false},
						{'cleanupIDs':false},
						{'removeHiddenElems':false}
					]
				}
			},
			toMarkup: {
				files:[
					{
						expand: true,
						cwd: 'assets/svgs/'+projectNumb+'/',
						src: ['*.svg'],
						dest: '.tmp/svgs/'+projectNumb+'/'
					}
				]
			},
		},

		watch: {
			options: {
				'atBegin':true
			},
			sass: {
				files: ['assets/scss/'+projectNumb+'/**/*.scss'],
				tasks: ['sass:toMarkup','autoprefixer:toMarkup']
			},
			copy: {
				files: ['assets/fonts/'+projectNumb+'/**/*.*','assets/images/'+projectNumb+'/**/*.*','!assets/images/'+projectNumb+'/**/*.{jpg,jpeg,png,gif}'],
				tasks: ['newer:copy:toMarkup']
			},
			svg: {
				files: ['assets/svgs/'+projectNumb+'/**/*.svg'],
				tasks: ['svgcombine','ngtemplates','ts','concat_sourcemap:toMarkup']
			},
			javascript: {
				files: ['assets/scripts/'+projectNumb+'/**/*.ts','assets/scripts/'+projectNumb+'/components/**/*.html'],
				tasks: ['tsconfig','ngtemplates','ts','concat_sourcemap:toMarkup']
			},
			imagemin: {
				files: ['assets/images/'+projectNumb+'/*.{png,jpeg,jpg,svg,ico,gif}'],
				tasks: ['newer:imagemin:toMarkup']
			},
			html: {
				files: ['assets/html/'+projectNumb+'/**/*.html'],
				tasks: ['bake:toMarkup']
			}
		}
	});


	grunt.registerTask('default', ['watch']);
	grunt.registerTask('dev',['sass:toDev','autoprefixer:toDev','tsconfig','svgcombine', 'ngtemplates','ts','uglify:toDev','copy:toDev','imagemin:toDev']);

};
