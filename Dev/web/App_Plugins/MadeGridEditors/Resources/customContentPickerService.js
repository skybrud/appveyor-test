﻿angular.module("umbraco.resources").factory("customContentPickerService", function ($http) {
    return {
        //archiveEntry: function (entryId) {
        //    return $http.get('/umbraco/backoffice/Feedback/Backend/Archive?entryId=' + entryId);
        //},
        //getEntry: function (entryId) {
        //    return $http.get('/umbraco/backoffice/Feedback/Backend/GetEntry?entryId=' + entryId);
        //},
        getContentByDocumentAlias: function (documentTypeAlias, startNodeId, limit, sort, order) {
            return $http({
                method: 'GET',
                url: '/umbraco/backoffice/CustomContent/Picker/GetContentByDocumentAlias/',
                params: {
                    documentTypeAlias: documentTypeAlias,
                    startNodeId: startNodeId,
                    limit: limit,
                    sort: sort,
                    order: order
                }
            });
        }
    };
});