﻿angular.module('umbraco').controller('MadeGridEditors.CasesAndTools.Controller', function ($scope, focus, dialogService) {

    if (!$scope.control.value || typeof ($scope.control.value) != 'object') $scope.control.value = {};
    if (!$scope.control.value.title) $scope.control.value.title = '';
    if (!$scope.control.value.description) $scope.control.value.description = '';
    if (!$scope.control.value.items) $scope.control.value.items = [];

    $scope.addItem = function () {
        $scope.control.value.items.push({
            title: null,
            description: null,
            link: null
        });
        focus('focusMe');
    };

    $scope.removeItem = function (index) {
        $scope.control.value.items.splice(index, 1);
    };

    function swap(array, i, j) {
        var a = array[i];
        array[i] = array[j];
        array[j] = a;
    }

    $scope.moveItemLeft = function (index) {
        swap($scope.control.value.items, index, index - 1);
    };

    $scope.moveItemRight = function (index) {
        swap($scope.control.value.items, index, index + 1);
    };

    $scope.sortableOptions = {
        axis: 'y',
        cursor: 'move',
        handle: '.handle',
        helper: false,
        tolerance: 'pointer'
    };



    function parseUmbracoLink(e) {
        return {
            id: e.id || 0,
            name: e.name || '',
            url: e.url,
            target: e.target || '_self',
            mode: (e.id ? (e.isMedia ? 'media' : 'content') : 'url')
        };
    }

    $scope.addLink = function (item) {
        dialogService.closeAll();
        dialogService.linkPicker({
            callback: function (e) {
                if (!e.id && !e.url && !confirm('The selected link appears to be empty. Do you want to continue anyways?')) return;
                item.link = parseUmbracoLink(e);
                dialogService.closeAll();
            }
        });
    };

    $scope.editLink = function (item) {
        dialogService.closeAll();

        if (item.link == null) return;

        if (item.link.mode == 'media') {
            dialogService.mediaPicker({
                callback: function (e) {
                    if (!e.id && !e.url && !confirm('The selected link appears to be empty. Do you want to continue anyways?')) return;
                    item.link = parseUmbracoLink(e);
                    dialogService.closeAll();
                }
            });
        } else {
            dialogService.linkPicker({
                currentTarget: {
                    id: item.link.id,
                    name: item.link.name,
                    url: item.link.url,
                    target: item.link.target
                },
                callback: function (e) {
                    if (!e.id && !e.url && !confirm('The selected link appears to be empty. Do you want to continue anyways?')) return;
                    item.link = parseUmbracoLink(e);
                    dialogService.closeAll();
                }
            });
        }
    };
});