﻿angular.module('umbraco').controller('MadeGridEditors.Image.Controller', function ($scope, $element, $routeParams, dialogService, userService, mediaHelper, entityResource) {

    $scope.special = $routeParams.id == 2120;

    function init() {

        if (!$scope.control.value) return;

        if ($scope.control.value.image) {
            entityResource.getById($scope.control.value.image, "Media").then(function (media) {
                if (media.parentId >= -1) {
                    if (!media.thumbnail) media.thumbnail = mediaHelper.resolveFileFromEntity(media, true);
                    setImage(media);
                }
            });
        }

    }

    function update() {

        if (!$scope.control.value) return;

        if (!$scope.control.value.image && !$scope.control.value.title && !$scope.control.value.subtitle && !$scope.control.value.text) {
            $scope.control.value = null;
            return;
        }

        if (!$scope.control.value.text) $scope.control.value.text = '';
        if (!$scope.control.value.title) $scope.control.value.title = '';
        if (!$scope.control.value.subtitle) $scope.control.value.subtitle = '';
        if (!$scope.control.value.image) $scope.control.value.image = 0;

    }

    /// Sets the image
    function setImage(image) {
        if (!$scope.control.value) $scope.control.value = { image: 0, title: '', subtitle: '', text: '' };
        if (image) {
            image.thumborig = image.thumbnail;
            image.thumbnail = image.thumbnail.split('?')[0] + '?width=950&height=500&mode=max';
        }
        $scope.image = image;
        $scope.control.value.image = image ? image.id : 0;
        update();
    }

    $scope.image = null;

    var startNodeId = null;
    userService.getCurrentUser().then(function (userData) {
        startNodeId = userData.startMediaId;
    });

    $scope.addImage = function () {
        dialogService.mediaPicker({
            startNodeId: startNodeId,
            multiPicker: false,
            callback: function (data) {
                setImage(data);
            }
        });
    };

    $scope.removeImage = function () {
        setImage(null);
    };

    init();

});