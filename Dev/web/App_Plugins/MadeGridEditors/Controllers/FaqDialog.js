﻿angular.module('umbraco').controller('MadeGridEditors.FaqDialog.Controller', function ($scope, $http, $timeout) {

    var wait = null;

    // Add the selected items in a new array
    $scope.selected = [];
    var ids = {};
    angular.forEach($scope.dialogData, function (item) {
        ids[item.id] = true;
        $scope.selected.push(item);
    });

    $scope.query = '';

    function search() {
        $http.get('/umbraco/backoffice/api/Faq/Search?query=' + $scope.query).success(function (r) {
            $scope.items = r.data;
        });
    }

    $scope.items = [];

    $scope.addItem = function (item) {
        ids[item.id] = true;
        $scope.selected.push(item);
    };

    $scope.removeItem = function (index, item) {
        $scope.selected.splice(index, 1);
        ids[item.id] = false;
    };

    $scope.isSelected = function (item) {
        return ids[item.id];
    };

    $scope.sortableOptions = {
        axis: 'y',
        cursor: 'move',
        handle: '.handle'
    };

    $scope.search = search;

    $scope.$watch('query', function () {
        if (wait) $timeout.cancel(wait);
        wait = $timeout(function () {
            search();
        }, 300);

    }, true);



});