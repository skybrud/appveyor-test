﻿angular.module('umbraco').controller('MadeGridEditors.Gallery.Controller', function ($scope, $http, dialogService, $timeout, mediaHelper, entityResource, userService) {

    function init() {
        if (!$scope.control.value) {
            $scope.control.value = {};
            $scope.control.value.goDeep = false;
        }

        if (!$scope.control.value.mediafolderId) return;

        update();
    }

    function update() {
        $scope.mediafolder = $scope.control.value.mediafolderId ? $scope.control.value.mediafolderId : null;

        $http.get('/umbraco/backoffice/api/Media/GetByFolderId?mediaFolderId=' + $scope.mediafolder + '&goDeep=' + $scope.control.value.goDeep).success(function (r) {
            $scope.items = r.data;
        });

        $http.get('/umbraco/backoffice/api/Media/GetMediaFolderName?mediaFolderId=' + $scope.mediafolder).success(function (r) {
            $scope.mediafoldername = r.data;
        });
    }

    var startNodeId = null;
    userService.getCurrentUser().then(function (userData) {
        startNodeId = userData.startMediaId;
    });

    $scope.addItems = function () {
        dialogService.mediaPicker({
            startNodeId: startNodeId,
            multiPicker: false,
            callback: function (data) {

                if (!data.isFolder) {
                    alert('Du kan kun vælge en folder');

                    return false;
                }

                $scope.control.value.mediafolderId = data.id;
                update();
            }
        });
    };

    init();
});