﻿angular.module('umbraco').controller('MadeGridEditors.MailChimp.Controller', function ($scope) {

    if (!$scope.control.value || typeof ($scope.control.value) != 'object') $scope.control.value = {};

    function init() {
        if (!$scope.control.value) return;
    }

    function update() {
        
        if (!$scope.control.value) return;

        if (!$scope.control.value.mailChimpListId && !$scope.control.value.mailChimpSuccessMsg) {
            $scope.control.value = null;
            return;
        }

        if (!$scope.control.value.mailChimpListId) $scope.control.value.mailChimpListId = '';
        if (!$scope.control.value.mailChimpSuccessMsg) $scope.control.value.mailChimpSuccessMsg = '';
    }

    init();

});