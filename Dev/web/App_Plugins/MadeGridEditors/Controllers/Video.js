﻿angular.module('umbraco').controller('MadeGridEditors.Video.Controller', function ($scope) {
    if ("config" in $scope.control) {
        $scope.control.config.showControls = false;
    }
    else {
        $scope.control.config = { showControls: true };
    }

    function getEmbedUrl(str) {
        var match;
        if (str.indexOf('vimeo') != -1) {
            match = str.match(/video\/|https?:\/\/vimeo\.com\/{1,2}([0-9]*)/);
            if (match && match[1].length > 1) {
                return { type: 'vimeo', url: '//player.vimeo.com/video/' + match[1] + '?title=0&amp;byline=0&amp;portrait=0&amp;&amp;autoplay=0' };
            }
        } else {
            match = str.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/);
            if (match && match[7].length == 11) {
                return { type: 'youtube', url: '//www.youtube.com/embed/' + match[7] + '?rel=0&controls=1&showinfo=0&autoplay=false' };
            }
        }
        return null;
    }

    $scope.update = function () {
        $scope.embed = $scope.control.value ? getEmbedUrl($scope.control.value.url) : null;
    };

    $scope.showConfig = function () {
        if ($scope.control.config.showControls == true) {
            $scope.control.config.showControls = false;
        }
        else {
            $scope.control.config.showControls = true;
        }

    };

    $scope.update();

});