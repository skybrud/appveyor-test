﻿angular.module('umbraco').controller('MadeGridEditors.ContactPerson.Controller', function ($scope, $http, dialogService, $timeout) {

    $scope.items = [];
    
    function init() {
        if ($scope.control.value) {
            $http.get('/umbraco/backoffice/CustomContent/Picker/GetItemsByIds?ids=' + $scope.control.value.contentIds).success(function(body) {
                $scope.items = body.data;
            });
        } else {
            $scope.control.value = {};
        }
    }

    function update() {
        var temp = [];
        for (var i = 0; i < $scope.items.length; i++) {
            temp.push($scope.items[i].id);
        }
        $scope.control.value.contentIds = temp.join(',');
    }

    $scope.addItems = function () {

        dialogService.open({
            template: '/App_Plugins/MadeGridEditors/Views/Dialogs/ContactPersonDialog.html',
            dialogData: $scope.items,
            callback: function (data) {
                $scope.items = data;
                update();
            }
        });
    };

    $scope.removeItem = function (index) {
        var temp = [];
        for (var i = 0; i < $scope.items.length; i++) {
            if (index != i) temp.push($scope.items[i]);
        }
        $scope.items = temp;
        update();
    };

    $scope.sortableOptions = {
        axis: 'y',
        cursor: 'move',
        handle: '.handle',
        update: function () {
            $timeout(function () {
                update();
            }, 10);
        }
    };

    init();
});