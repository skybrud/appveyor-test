angular.module("umbraco")
    .controller("Skybrud.CharLimitEditorController",
    function ($scope) {
         $scope.limitChars = function(){
			var limit = parseInt($scope.model.config.limit);

			if ($scope.model.value.length > limit && $scope.model.config.enforce == 1)
			{
				$scope.info = 'You cannot write more then ' + limit  + ' characters!';
				$scope.model.value = $scope.model.value.substr(0, limit);
			}
			else
			{
				$scope.info = 'You have ' + (limit - $scope.model.value.length) + ' characters left.';
			}
         };
    });