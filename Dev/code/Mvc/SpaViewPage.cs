﻿using made.Models.Website.Common;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace made.Mvc{
    public abstract class SpaViewPage<T> : UmbracoViewPage<T>{
        private Settings _settings;

        public Settings Settings{
            get{
                return _settings ??
                       (_settings =
                           Settings.GetFromContent(
                               UmbracoContext.PublishedContentRequest.PublishedContent.AncestorOrSelf(
                                   "SpaDomaeneKontainer"), new Settings()));
            }
        }

        //public SpaContext Spa
        //{
        //    get { return SpaContext.Current; }
        //}
    }
}