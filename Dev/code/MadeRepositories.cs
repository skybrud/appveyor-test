﻿using made.Models.Categories;
using made.Models.Events;
using made.Models.Faq;
using made.Models.News;
using made.Models.Persons;
using made.Models.SoMe;

namespace made
{
    public class MadeRepositories
    {
        #region Properties

        public MadeEventRepository Events { get; private set; }
        public MadePersonRepository Persons { get; private set; }
        public MadeFaqRepository Faq { get; private set; }
        public MadeNewsRepository News { get; private set; }
        public MadeCategoryRepository Categories { get; private set; }
        public MadeSoMeRepository SoMe { get; private set; }
        //public MadeSiteSearchRepository SiteSearch { get; private set; }

        #endregion

        #region Constructors

        internal MadeRepositories()
        {
            Events = new MadeEventRepository();
            Persons = new MadePersonRepository();
            Faq = new MadeFaqRepository();
            News = new MadeNewsRepository();
            Categories = new MadeCategoryRepository();
            SoMe = new MadeSoMeRepository();

            //Pulse = new MadePulseRepository();
            //SiteSearch = new MadeSiteSearchRepository();
        }

        #endregion
    }
}