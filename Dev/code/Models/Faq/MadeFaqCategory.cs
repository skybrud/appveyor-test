﻿using System.Linq;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Faq
{
    public class MadeFaqCategory
    {
        #region Properties

        [JsonIgnore]
        public IPublishedContent Content { get; private set; }

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("urlName")]
        public string UrlName { get; private set; }

        [JsonProperty("hideInBruttoList")]
        public bool HideInBruttoList { get; private set; }

        [JsonProperty("items")]
        public MadeFaqItem[] Items { get; private set; }

        #endregion

        #region Static methods

        public static MadeFaqCategory GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            if (content.GetPropertyValue<bool>("hideInBruttolist")) return null;

            return new MadeFaqCategory
            {
                Content = content,
                Id = content.Id,
                Name = content.Name,
                UrlName = content.UrlName,
                HideInBruttoList = content.GetPropertyValue<bool>("hideInBruttolist"),
                Items = content.Children.Any() ? content.Children.Where(x => x.DocumentTypeAlias == "MadeFaqSpoergsmaal" && !x.Hidden() && !x.GetPropertyValue<bool>("hideInBruttolist")).Select(MadeFaqItem.GetFromContent).ToArray() : null
            };
        }

        #endregion
    }
}