﻿using System.Collections.Generic;
using System.Linq;
using Skybrud.Umbraco.Module;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Faq
{
    public class MadeFaqRepository
    {
        protected const int RootId = 1156;


        internal static MadeFaqItem[] Search(string query, int[] categories)
        {
            // Make the basic search
            int count;

            IEnumerable<IPublishedContent> result = SkybrudSearch.SearchDocuments(
                query,
                out count,
                fields: new[] {"nodeName_lci", "question_lci"},
                documentTypes: new[] {"madefaqspoergsmaal"},
                orderBy: "nodeName"
                );
            // Filter by categories
            if (categories != null && categories.Length > 0)
            {
                result = result.Where(x => categories.Contains(x.Parent.Id));
            }


            return result.Select(MadeFaqItem.GetFromContent).Where(x => x != null).ToArray();
        }

        internal static object GetItemsByIds(int[] ids)
        {
            return (
                from id in ids
                let item = UmbracoContext.Current.ContentCache.GetById(id)
                where item != null
                select MadeFaqItem.GetFromContent(item)
                ).ToArray();
        }

        #region Public methods

        public MadeFaqCategory[] GetFaqCategories()
        {
            MadeFaqCategory[] faq =
                UmbracoContext.Current.ContentCache.GetById(RootId)
                    .Children.Select(MadeFaqCategory.GetFromContent)
                    .ToArray();

            return faq;
        }

        #endregion
    }
}