﻿using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Faq
{
    public class MadeFaqItem
    {
        #region Properties

        [JsonIgnore]
        public IPublishedContent Content { get; private set; }

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("question")]
        public string Question { get; private set; }

        [JsonProperty("answer")]
        public string Answer { get; private set; }

        [JsonProperty("urlName")]
        public string UrlName { get; private set; }

        [JsonIgnore]
        public bool HideInBruttoList { get; private set; }

        #endregion

        #region Static methods

        public static MadeFaqItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            if (content.GetPropertyValue<bool>("hideInBruttolist")) return null;

            return new MadeFaqItem
            {
                Content = content,
                Id = content.Id,
                Name = content.Name,
                Question = content.HasValue("question") ? content.GetPropertyValue<string>("question") : content.Name,
                Answer = content.GetPropertyValue<string>("answer"),
                UrlName = content.UrlName,
                HideInBruttoList = content.HasValue("hideInBruttolist") && content.GetPropertyValue<bool>("hideInBruttolist")
            };
        }

        #endregion
    }
}