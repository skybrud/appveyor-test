﻿using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;


namespace made.Models.Media
{
    public class MediaRepository
    {
        public static MediaItem[] GetByMediaFolderId(int mediaFolderId, bool goDeep = false)
        {
            IPublishedContent mediaFolder = UmbracoContext.Current.MediaCache.GetById(mediaFolderId);

            if (mediaFolder == null)
                return null;

            return goDeep ? mediaFolder.Descendants().ToList().Where(x => x.ContentType.Alias == "Image").Select(MediaItem.GetFromContent).ToArray() : mediaFolder.Children.ToList().Where(x => x.ContentType.Alias == "Image").Select(MediaItem.GetFromContent).ToArray();
        }

        internal static string GetMediaFolderName(int mediaFolderId)
        {
            IPublishedContent mediaFolder = UmbracoContext.Current.MediaCache.GetById(mediaFolderId);

            if (mediaFolder == null)
                return null;

            return mediaFolder.Name;
        }

    }
}
