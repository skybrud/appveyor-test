﻿using System;
using System.IO;
using System.Net.Http;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.LinkPicker.Extensions.Json;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Interfaces;
using Umbraco.Forms.Core;
using Umbraco.Forms.Mvc.BusinessLogic;
using Umbraco.Forms.Mvc.Models;
using Umbraco.Forms.Web.Controllers;

namespace made.Models.Grid
{
    public class GridControlMacroValue : IGridControlValue
    {
        #region Properties

        [JsonProperty("jobject")]
        public JObject JObject { get; private set; }

        [JsonProperty("formGuid")]
        public String FormGuid { get; private set; }

        public string FormMarkup { get; private set; }

        public FormViewModel FormModel { get; private set; }

        [JsonIgnore]
        public GridControl Control { get; private set; }

        #endregion

        #region Static methods

        public static GridControlMacroValue Parse(GridControl control, JToken token)
        {
            return Parse(control, token as JObject);
        }

        public static GridControlMacroValue Parse(GridControl control, JObject obj)
        {
            if (obj == null) return null;

            string formGuidString = obj.GetObject("macroParamsDictionary").GetString("FormGuid");

            Guid formGuid = new Guid(formGuidString);


            //RouteData routeData = new RouteData();
            //routeData.Values["controller"] = "UmbracoForms";
            //routeData.Values["action"] = "Render";
            
            //IController ctrl = new UmbracoFormsController();

            //RequestContext rc = new RequestContext(new HttpContextWrapper(HttpContext.Current), routeData);

            //ctrl.Execute(rc);

            //string markup = ViewRenderer.RenderView("~/Views/Partials/Forms/Form.cshtml", new FormViewModel(), new ControllerContext());



            #region Bjerners aproach

            HttpContextWrapper context = new HttpContextWrapper(HttpContext.Current);

            RouteData route = new RouteData();
            route.Values["controller"] = "UmbracoForms";
            route.Values["action"] = "Render";
            route.Values["route"] = new {formId = formGuid, mode = "form"};

            ControllerContext ctx = new ControllerContext(new RequestContext(context, route), new UmbracoFormsController());

            ViewEngineResult result = ViewEngines.Engines.FindPartialView(ctx, "~/Views/Partials/Forms/Form.cshtml");
            if(result == null || result.View == null) throw new Exception("Partial View not found");

            StringBuilder sb = new StringBuilder();
            using (StringWriter writer = new StringWriter(sb))
            {

                FormViewModel fvm = new FormViewModel();

                //fvm.RenderMode = "form";

                ViewContext vctx = new ViewContext(ctx, result.View, new ViewDataDictionary { Model = fvm }, new TempDataDictionary(), writer);
                //ViewContext vctx = new ViewContext(ctx, result.View, new ViewDataDictionary { Model = fvm }, new TempDataDictionary(), writer);
                
                //HtmlHelper helper = new HtmlHelper(new ViewContext(ctx, result.View, null, null, writer), null);

                //helper.RenderAction("Render", "UmbracoForms", new { formId = formGuid, mode = "form" });

                result.View.Render(vctx, writer);
            }

            string markup = sb.ToString();

            #endregion



            //Umbraco.Forms.Web.Controllers.UmbracoFormsController

            //Html.RenderAction("Render", "UmbracoForms", new {formId = g, mode = "form"});
            //Html.RenderAction("actionName", "controllerName", new object { routeValues })

            
            return new GridControlMacroValue
            {
                JObject = obj,
                FormGuid = formGuidString,
                FormMarkup = markup
            };
        }

        #endregion
    }
}