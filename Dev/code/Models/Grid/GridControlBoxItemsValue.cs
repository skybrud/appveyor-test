﻿using System.Linq;
using made.Models.Boxes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;

namespace made.Models.Grid
{
    public class GridControlBoxItemsValue : IGridControlValue
    {
        #region Properties

        [JsonIgnore]
        public JObject JObject { get; private set; }

        [JsonProperty("headline")]
        public string Headline { get; private set; }

        [JsonProperty("description")]
        public string Description { get; private set; }

        [JsonProperty("items")]
        public CaseAndToolBox[] Items { get; private set; }

        public GridControl Control { get; set; }

        #endregion


        #region Static methods

        public static GridControlBoxItemsValue Parse(JToken token)
        {
            return Parse(token as JObject);
        }

        public static GridControlBoxItemsValue Parse(JObject obj)
        {
            if (obj == null) return null;

            return new GridControlBoxItemsValue
            {
                JObject = obj,
                Headline = obj.GetString("title"),
                Description = obj.GetString("description"),
                Items = (
                    from JObject o in obj.GetArray("items")
                    select CaseAndToolBox.Parse(o)
                    ).ToArray()
            };
        }

        #endregion
    }
}