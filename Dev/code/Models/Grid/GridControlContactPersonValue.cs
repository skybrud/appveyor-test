﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using made.Models.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.LinkPicker;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;

namespace made.Models.Grid
{
    public class GridControlContactPersonValue : IGridControlValue
    {
        #region Properties

        [JsonIgnore]
        public JObject JObject { get; private set; }

        [JsonIgnore]
        public GridControl Control { get; private set; }

        [JsonProperty("items")]
        public MadePersonItem[] ContactPersons { get; private set; }

        #endregion


        #region Static methods

        public static GridControlContactPersonValue Parse(JToken token)
        {
            return Parse(token as JObject);
        }

        public static GridControlContactPersonValue Parse(JObject obj)
        {
            if (obj == null) return null;

            int[] ids = obj.GetString("contentIds").Split(',').Select(int.Parse).ToArray();

            return new GridControlContactPersonValue
            {
                JObject = obj,
                ContactPersons = (
                    from int id in ids
                    select MadePersonItem.GetFromId(id)
                ).ToArray()

            };
        }

        #endregion
    }
}
