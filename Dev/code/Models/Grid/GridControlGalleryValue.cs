﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using made.Models.Gallery;
using made.Models.Media;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Grid
{
    public class GridControlGalleryValue : IGridControlValue
    {
        #region Properties

        [JsonIgnore]
        public JObject JObject { get; private set; }

        [JsonIgnore]
        public int MediaFolderId { get; private set; }

        [JsonIgnore]
        public string MediaFolderName { get; private set; }

        [JsonIgnore]
        public bool GoDeep { get; private set; }

        [JsonProperty("headline")]
        public string Headline { get; private set; }

        [JsonProperty("items")]
        public GalleryItem[] Items { get; private set; }

        [JsonIgnore]
        public GridControl Control { get; private set; }

        #endregion


        #region Static methods

        public static GridControlGalleryValue Parse(GridControl control, JToken token)
        {
            return Parse(control, token as JObject);
        }

        public static GridControlGalleryValue Parse(GridControl control, JObject obj)
        {

            if (obj == null) return null;

            int mediaFolderId = 0;

            int.TryParse(obj.GetString("mediafolderId"), out mediaFolderId);

            if (mediaFolderId == 0) return null;

            IPublishedContent mediaFolder = UmbracoContext.Current == null ? null : UmbracoContext.Current.MediaCache.GetById(mediaFolderId);

            if (mediaFolder == null) return null;

            bool goDeep = obj.GetBoolean("goDeep");


            // Parse the JSON value
            var value = new GridControlGalleryValue
            {
                JObject = obj,
                MediaFolderId = mediaFolder.Id,
                MediaFolderName = mediaFolder.Name,
                GoDeep = goDeep,
                Headline = obj.GetString("title"),
                Items = goDeep
                    ? mediaFolder.Descendants().Where(x => x.ContentType.Alias == "Image").ToList().Select(GalleryItem.GetFromContent).ToArray()
                    : mediaFolder.Children.Where(x => x.ContentType.Alias == "Image").ToList().Select(GalleryItem.GetFromContent).ToArray()
            };

            return value;
        }

        #endregion
    }
}
