﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;
using Skybrud.Umbraco.Module.Models;

namespace made.Models.Grid
{
    public class GridControlVideoValue : IGridControlValue
    {
        #region Properties

        [JsonIgnore]
        public JObject JObject { get; private set; }

        [JsonIgnore]
        public GridControl Control { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }

        [JsonProperty("description")]
        public string Description { get; private set; }

        [JsonProperty("poster")]
        public string Poster { get; private set; }

        [JsonIgnore]
        public Video Video { get; private set; }

        [JsonIgnore]
        public bool HasVideo
        {
            get { return Video != null; }
        }

        [JsonIgnore]
        public bool HasDescription
        {
            get { return !String.IsNullOrWhiteSpace(Description); }
        }

        #endregion


        #region Static methods

        public static GridControlVideoValue Parse(GridControl control, JToken token)
        {
            return Parse(control, token as JObject);
        }

        public static GridControlVideoValue Parse(GridControl control, JObject obj)
        {

            if (obj == null) return null;

            // Parse the JSON value
            GridControlVideoValue value = new GridControlVideoValue
            {
                JObject = obj,
                Url = obj.GetString("url"),
                Description = obj.GetString("description"),
                Poster = null       //ikke implementeret i editoren
            };

            // Parse the URL
            value.Video = Video.GetFromString(value.Url);

            return value;

        }

        #endregion
    }
}
