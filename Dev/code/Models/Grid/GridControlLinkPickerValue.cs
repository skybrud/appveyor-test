﻿using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.LinkPicker;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;

namespace made.Models.Grid {

    public class GridControlLinkPickerValue : IGridControlValue {

        #region Properties
        
        /// <summary>
        /// Gets a reference to the parent control.
        /// </summary>
        [JsonIgnore]
        public GridControl Control { get; private set; }

        /// <summary>
        /// Gets a reference to the instance of <code>JArray</code> the object was created from.
        /// </summary>
        [JsonIgnore]
        public JArray JArray { get; private set; }

        [JsonProperty("headline")]
        public string Headline { get; private set; }

        /// <summary>
        /// Gets an array of all link items.
        /// </summary>
        [JsonProperty("items")]
        public LinkPickerItem[] Items { get; internal set; }

        /// <summary>
        /// Gets the total amount of link items.
        /// </summary>
        [JsonProperty("count")]
        public int Count {
            get { return Items.Length; }
        }

        #endregion



        #region Static methods


        public static GridControlLinkPickerValue Parse(JToken token)
        {
            return Parse(token as JObject);
        }


        /// <summary>
        /// Gets a media value from the specified <code>JArray</code>.
        /// </summary>
        /// <param name="control">The parent control.</param>
        /// <param name="obj"></param>
        public static GridControlLinkPickerValue Parse(JObject obj) {
            if (obj == null) return null;

            string title = obj.GetString("title");

            return new GridControlLinkPickerValue {
                Headline = !string.IsNullOrEmpty(title) ? title : "Relateret indhold",  //set default title if no title exists
                Items = (
                    from JObject p in obj.GetArray("items")
                    select LinkPickerItem.Parse(p)
                ).ToArray()
            };
        }

        #endregion

    }

}