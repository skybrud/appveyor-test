﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;

namespace made.Models.Grid
{
    public class GridControlBoxFactValue : IGridControlValue
    {
        #region Properties

        [JsonIgnore]
        public JObject JObject { get; private set; }

        [JsonIgnore]
        public GridControl Control { get; private set; }

        [JsonProperty("headline")]
        public string Headline { get; private set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; private set; }

        [JsonProperty("property")]
        public string Property { get; private set; }

        [JsonProperty("description")]
        public string Description { get; private set; }

        #endregion


        #region Static methods

        public static GridControlBoxFactValue Parse(JToken token)
        {
            return Parse(token as JObject);
        }

        public static GridControlBoxFactValue Parse(JObject obj)
        {
            if (obj == null) return null;

            return new GridControlBoxFactValue
            {
                Headline = obj.GetString("headline"),
                Subtitle = obj.GetString("subtitle"),
                Property = obj.GetString("property"),
                Description = obj.GetString("description"),
            };
        }

        #endregion
    }
}
