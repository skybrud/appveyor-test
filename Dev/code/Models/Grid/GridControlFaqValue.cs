﻿using System.Collections.Generic;
using System.Linq;
using made.Models.Faq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Grid
{
    public class GridControlFaqValue : IGridControlValue
    {
        #region Properties

        [JsonIgnore]
        public JObject JObject { get; private set; }

        [JsonProperty("headline")]
        public string Headline { get; private set; }

        [JsonProperty("description")]
        public string Description { get; private set; }

        [JsonProperty("items")]
        public MadeFaqItem[] Items { get; private set; }

        [JsonIgnore]
        public GridControl Control { get; private set; }

        #endregion


        #region Static methods

        public static GridControlFaqValue Parse(JToken token)
        {
            return Parse(token as JObject);
        }

        public static GridControlFaqValue Parse(JObject obj)
        {
            if (obj == null) return null;

            int[] ids =
                obj.GetString("faqIds").Split(',').Where(y => !string.IsNullOrEmpty(y)).Select(int.Parse).ToArray();

            var faqs = new List<IPublishedContent>();

            for (int i = 0; i <= ids.Length - 1; i++)
            {
                int id = ids[i];

                if (id > 0)
                {
                    IPublishedContent c = UmbracoContext.Current.ContentCache.GetById(id);

                    if (c != null)
                    {
                        faqs.Add(c);
                    }
                }
            }

            return new GridControlFaqValue
            {
                JObject = obj,
                Headline = obj.GetString("title"),
                Description = obj.GetString("description"),
                Items = faqs.Select(MadeFaqItem.GetFromContent).ToArray()
            };
        }

        #endregion
    }
}