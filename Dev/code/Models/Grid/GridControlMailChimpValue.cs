﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.LinkPicker.Extensions.Json;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Interfaces;

namespace made.Models.Grid
{
    public class GridControlMailChimpValue : IGridControlValue
    {
        #region Properties

        [JsonProperty("jobject")]
        public JObject JObject { get; private set; }

        [JsonProperty("mailchimpListId")]
        public string MailChimpListId { get; private set; }

        [JsonProperty("mailchimpSuccessMsg")]
        public string MailChimpSuccessMsg { get; private set; }

        [JsonIgnore]
        public GridControl Control { get; private set; }

        #endregion

        #region Static methods

        public static GridControlMailChimpValue Parse(GridControl control, JToken token)
        {
            return Parse(control, token as JObject);
        }

        public static GridControlMailChimpValue Parse(GridControl control, JObject obj)
        {
            if (obj == null) return null;

            return new GridControlMailChimpValue
            {
                JObject = obj,
                MailChimpListId = obj.GetString("mailChimpListId"),
                MailChimpSuccessMsg = obj.GetString("mailChimpSuccessMsg")
            };
        }

        #endregion
    }
}