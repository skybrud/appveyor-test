﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace made.Models.MadeMembers
{
    public class MadeMedlemCategory
    {
        [JsonProperty("categoryName")]
        public string Categoryname { get; private set; }

        [JsonProperty("members")]
        public List<MadeMedlemItem> Members { get; private set; }

        protected MadeMedlemCategory(IPublishedContent content)
        {
            Categoryname = content.Name;

            Members = content.Children.Any() 
                ?  content.Children.Select(MadeMedlemItem.GetFromContent).ToList()
                : null;
        }

        public static MadeMedlemCategory GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeMedlemCategory(content);
        }
    }
}
