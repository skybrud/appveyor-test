﻿using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.MadeMembers
{
    public class MadeMedlemItem
    {
        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }


        public static MadeMedlemItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            return new MadeMedlemItem
            {
                Name = content.HasValue("medlem") ? content.GetPropertyValue<string>("medlem") : content.Name,
                Url = content.GetPropertyValue<string>("webpageUrl")
            };
        }
    }
}