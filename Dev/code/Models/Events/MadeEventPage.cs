﻿using System;
using System.Collections.Generic;
using Archetype.Models;
using made.Models.Website;
using made.Models.Website.Common.ArchetypeEventProgram;
using Newtonsoft.Json;
using Skybrud.Umbraco.GridData;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Events
{
    public class MadeEventPage : Master
    {
        #region Properties

       [JsonProperty("formSignup")]
        public string FormSignup { get; private set; }

        [JsonProperty("infos")]
        public MadeEventInfo[] Infos { get; private set; }

        [JsonProperty("program")]
        public IEnumerable<ProgramItem> Program { get; private set; }

        [JsonProperty("grid")]
        public GridDataModel Grid { get; private set; }

        [JsonProperty("isMemberEvent")]
        public bool IsMemberEvent { get; private set; }

        #endregion

        protected MadeEventPage(IPublishedContent content)
            : base(content)
        {
            Infos = MadeEventInfo.GetMadeEventInfos(content);
            Program = Website.Common.ArchetypeEventProgram.Program.GetFromContent(content.GetPropertyValue<ArchetypeModel>("program"));
            FormSignup = content.HasValue("formSignup") ? content.GetPropertyValue<Guid>("formSignup").ToString() : null;
            Grid = content.GetPropertyValue<GridDataModel>("grid");
            IsMemberEvent = content.GetPropertyValue<bool>("isMemberEvent");
        }

        public static MadeEventPage GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeEventPage(content);
        }

    }
}