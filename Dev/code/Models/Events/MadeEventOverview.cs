﻿using made.Models.Categories;
using made.Models.Website;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace made.Models.Events
{
    public class MadeEventOverview : Master
    {
        #region Properties

        [JsonProperty("filters")]
        public MadeCategory[] Filters { get; private set; }

        #endregion

        protected MadeEventOverview(IPublishedContent content)
            : base(content)
        {

            Filters = MadeContext.Current.Repositories.Events.GetEventCategories();

        }

        public static MadeEventOverview GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeEventOverview(content);
        }
    }
}