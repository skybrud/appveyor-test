﻿using System.Collections.Generic;
using System.Linq;
using Lucene.Net.Search.Function;
using made.Models.Categories;
using made.Models.SearchResults;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Events
{
    public class MadeEventRepository
    {
        #region Public methods

        public MadeSearchResult SearchEvents(
            string theme = "",
            string region = "",
            string type = "",
            bool hasBeen = false,
            int limit = 10,
            int offset = 0)
        {
            string[] arrThemes = theme.Split(',');
            string[] arrRegions = region.Split(',');
            string[] arrTypes = type.Split(',');


            return Search.SearchEventDocuments(arrThemes, arrRegions, arrTypes, hasBeen, limit, offset);
        }

        /// <summary>
        /// Get categories for filtering
        /// </summary>
        /// <returns></returns>
        public MadeCategory[] GetEventCategories()
        {
            List<MadeCategory> output = new List<MadeCategory>();

            //Themes
            IPublishedContent themes = UmbracoContext.Current.ContentCache.GetSingleByXPath("//MadeTemaer");

            if (themes != null)
            {
                MadeCategory category = new MadeCategory
                {
                    Name = "Tema",
                    Alias = "theme",
                    Items = themes.Children.Where(x => !x.Hidden()).Select(MadeCategoryItem.GetFromContent).ToArray()
                };

                output.Add(category);
            }

            //Region
            MadeCategory regionCat = MadeContext.Current.Repositories.Categories.GetCategoryById(1130, "Region");

            if (regionCat != null)
            {
                output.Add(regionCat);
            }
            

            //Types
            MadeCategory typeCat = MadeContext.Current.Repositories.Categories.GetCategoryById(1131, "Type");

            if (typeCat != null)
            {
                output.Add(typeCat);
            }


            return output.ToArray();
        }

        #endregion
    }
}