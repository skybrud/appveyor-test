﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.Strings;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Events
{
    public class MadeEventInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }


        public static MadeEventInfo GetFromContent(string name, string value)
        {
            return new MadeEventInfo
            {
                Name = name,
                Value = value
            };
        }

        public static MadeEventInfo[] GetMadeEventInfos(IPublishedContent content)
        {
            if (content == null) return null;

            List<MadeEventInfo> list = new List<MadeEventInfo>();

            list.Add(GetFromContent("Dato", GetMarkupForDate(content)));
            list.Add(GetFromContent("Sted", content.GetPropertyValue<string>("location")));
            list.Add(GetFromContent("Sprog", content.GetPropertyValue<string>("language")));
            list.Add(GetFromContent("Pris", content.GetPropertyValue<string>("price")));
            list.Add(GetFromContent("Info", content.GetPropertyValue<string>("info")));
            list.Add(GetFromContent("Mere info", content.GetPropertyValue<string>("moreInfo")));

            if (content.HasValue("eventCoordinatorLink"))
            {
                list.Add(GetFromContent("Arrangør",
                    string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", content.GetPropertyValue<string>("eventCoordinatorLink"),
                        content.GetPropertyValue<string>("eventCoordinator"))));
            }
            else
            {
                list.Add(GetFromContent("Arrangør",
                    string.Format("{0}", content.GetPropertyValue<string>("eventCoordinator"))));
            }

            return list.ToArray();
        }


        private static string GetMarkupForDate(IPublishedContent content)
        {
            CultureInfo ci = new CultureInfo("da-DK");

            DateTime from = content.GetPropertyValue<DateTime>("startdato");
            DateTime to = content.GetPropertyValue<DateTime>("slutdato");
            string info = content.GetPropertyValue<string>("dateExtraInfo");



            string output = "";

            if (from.Date == to.Date){
                output = string.Format("<strong>{0} kl. {1} - {2}.</strong><br />{3}",
                    from.ToString("dddd \\den d. MMM. yyyy", ci).FirstCharToUpper(),
                    from.ToString("HH:mm"),
                    to.ToString("HH:mm"),
                    info);
            } else
            {
                output = string.Format("<strong>{0} - {1}</strong><br />{2}",
                    from.ToString("dddd \\den d. MMM. yyyy, kl. HH:mm", ci).FirstCharToUpper(),
                    to.ToString("dddd \\den d. MMM. yyyy, kl. HH:mm", ci).FirstCharToUpper(),
                    info);
            }

            return output;
        }
    }
}
