﻿using System;
using System.Linq;
using made.Extensions;
using made.Interfaces;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Models.Events
{
    public class MadeEventItem : ISearchResultItem
    {
        [JsonProperty("teaser")]
        public string Teaser { get; private set; }

        [JsonProperty("displayDate")]
        public DateTime DisplayDate { get; private set; }

        [JsonProperty("startdate")]
        public DateTime Startdate { get; private set; }

        [JsonProperty("enddate")]
        public DateTime Enddate { get; private set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; private set; }

        [JsonProperty("activityTypes")]
        public string[] ActivityTypes { get; private set; }

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }

        [JsonProperty("isMemberEvent")]
        public bool IsMemberEvent { get; private set; }


        public static MadeEventItem GetFromId(int id)
        {
            return GetFromContent(UmbracoContext.Current.ContentCache.GetById(id));
        }

        public static MadeEventItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            IPublishedContent m = content.HasValue("overviewImage") ? content.TypedMedia("overviewImage") : null;

            return new MadeEventItem
            {
                Id = content.Id,
                Name = content.HasValue("title") ? content.GetPropertyValue<string>("title") : content.Name,
                Teaser = content.GetPropertyValue<string>("teaser").Nl2P().ToString(),
                Url = content.Url,
                Startdate = content.GetPropertyValue<DateTime>("startdato"),
                Enddate = content.GetPropertyValue<DateTime>("slutdato"),
                DisplayDate = content.GetPropertyValue<DateTime>("startdato"),
                ImageUrl =
                    m != null
                        ? m.GetCropUrl(Convert.ToInt32(m.GetPropertyValue<string>("umbracoWidth")),
                            Convert.ToInt32(m.GetPropertyValue<string>("umbracoheight")),
                            imageCropMode: ImageCropMode.Crop, preferFocalPoint: true)
                        : null,
                ActivityTypes = content.TypedCsvContent("eventtype").Select(x => x.GetTitle()).ToArray(),
                IsMemberEvent = content.GetPropertyValue<bool>("isMemberEvent")
            };
        }
    }
}