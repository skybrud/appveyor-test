﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.LinkPicker;
using Skybrud.Umbraco.GridData.Extensions.Json;

namespace made.Models.Boxes
{
    public class CaseAndToolBox
    {
        [JsonProperty("headline")]
        public string Headline { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("link")]
        public LinkPickerItem Link { get; set; }

        public static CaseAndToolBox Parse(JObject obj)
        {
            if (obj == null) return null;

            return new CaseAndToolBox
            {
                Headline = obj.GetString("title"),
                Description = obj.GetString("description"),
                Link = LinkPickerItem.Parse(obj.GetObject("link"))
            };
        }
    }
}