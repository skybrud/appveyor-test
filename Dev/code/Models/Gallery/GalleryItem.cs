﻿using System;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Models.Gallery
{
    public class GalleryItem
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("imageText")]
        public string ImageText { get; set; }

        public static GalleryItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            return new GalleryItem
            {
                Url = content.GetCropUrl(Convert.ToInt32(content.GetPropertyValue<string>("umbracoWidth")),
                    Convert.ToInt32(content.GetPropertyValue<string>("umbracoheight")),
                    imageCropMode: ImageCropMode.Crop, preferFocalPoint: true),
                ImageText = content.GetPropertyValue<string>("description")

            };
        }
    }
}