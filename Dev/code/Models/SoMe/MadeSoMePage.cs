﻿using made.Models.Website;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace made.Models.SoMe
{
    public class MadeSoMePage : Master
    {
        #region Properties

        #endregion


        protected MadeSoMePage(IPublishedContent content)
            : base(content)
        {
        }

        public static MadeSoMePage GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeSoMePage(content);
        }
    }
}