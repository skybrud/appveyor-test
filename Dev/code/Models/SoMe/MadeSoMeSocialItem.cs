﻿using System;
using Newtonsoft.Json;

namespace made.Models.SoMe
{
    public class MadeSoMeSocialItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("link")]
        public string Link { get; set; }
        [JsonProperty("picture")]
        public string Picture { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("teaser")]
        public string Teaser { get; set; }
        [JsonProperty("likeCount")]
        public int LikeCount { get; set; }
        [JsonProperty("socialType")]
        public string SocialType { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("tags")]
        public string[] Tags { get; set; }
        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }
    }
}
