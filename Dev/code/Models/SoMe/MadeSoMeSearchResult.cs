﻿using Newtonsoft.Json;

namespace made.Models.SoMe
{
    public class MadeSoMeSearchResult
    {
        [JsonProperty("total")]
        public int Total { get; internal set; }

        [JsonProperty("offset")]
        public int Offset { get; internal set; }

        [JsonProperty("limit")]
        public int Limit { get; internal set; }

        [JsonProperty("items")]
        public object[] Items { get; internal set; }
    }
}
