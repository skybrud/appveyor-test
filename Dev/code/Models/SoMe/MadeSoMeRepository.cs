﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using made.Models.Events;
using made.Models.News;
using Skybrud.Social.Instagram;
using Skybrud.Social.Instagram.OAuth;
using Skybrud.Social.Instagram.Objects;
using Skybrud.Social.LinkedIn;
using Skybrud.Social.LinkedIn.Objects.Updates;
using Skybrud.Social.Twitter;
using Skybrud.Social.Twitter.OAuth;
using Skybrud.Social.Twitter.Objects;
using Umbraco.Core;

namespace made.Models.SoMe
{
    public class MadeSoMeRepository
    {
        #region Public Methods

        public MadeSoMeSearchResult GetSoMe(bool includeInstagram = false, bool includeTwitter = false,
            bool includeLinkedIn = false, bool includeNews = false, bool includeEvents = false, int limit = 1, int offset = 0)
        {
            var resInstagram = new List<MadeSoMeSocialItem>();
            var resTwitter = new List<MadeSoMeSocialItem>();
            var resLinkedIn = new List<MadeSoMeSocialItem>();
            var resNews = new List<MadeSoMeSocialItem>();
            var resEvents = new List<MadeSoMeSocialItem>();


            #region Instagram

            if (includeInstagram)
            {
                InstagramService inService = InstagramService.CreateFromOAuthClient(new InstagramOAuthClient
                {
                    ClientSecret = "8b0559b319fe44f784d23f977039e37b",
                    ClientId = "2d852507396e421ea853fb1a4a09f481",
                    AccessToken = "1709924144.2d85250.0349ba2a64c448d7ad3c9a945ca6ed6d"
                });

                var instaItems = new List<InstagramMedia>();

                instaItems.AddRange(inService.Users.GetRecentMedia().Body.Data);

                resInstagram.AddRange(from media in instaItems
                    select new MadeSoMeSocialItem
                    {
                        Id = media.Id,
                        Link = media.Link,
                        Picture = media.Standard,
                        Title = media.CaptionText,
                        LikeCount = media.LikeCount,
                        SocialType = "instagram",
                        Tags = media.Tags,
                        CreatedDate = media.Created
                    });
            }

            #endregion

            #region Twitter

            if (includeTwitter)
            {
                TwitterService twService = TwitterService.CreateFromOAuthClient(new TwitterOAuthClient
                {
                    ConsumerKey = "Ud0vI0RaYq9k8gutsgTadaJ6T",
                    ConsumerSecret = "R6XCDt2WxB1xebHvF8iIOsP40No5xlHz9JXFAkOYMDTFOI3Y3E",
                    Token = "3059061814-S3PgTpFEvmfThokN5tuczFPbfRIs618WWRpAscn",
                    TokenSecret = "GifEgq9zR4yKLGFxpRwTHwVQT9KfSDsf7VdXaiQUgf7UU"
                });

                TwitterStatusMessage[] twitterData = twService.Statuses.GetUserTimeline("").Body;

                resTwitter.AddRange(
                    from media in twitterData
                    select new MadeSoMeSocialItem
                    {
                        Id = media.Id.ToString(CultureInfo.InvariantCulture),
                        Link = string.Format("https://twitter.com/statuses/{0}", media.Id),
                        Picture = "",
                        Title = media.Text,
                        LikeCount = 0,
                        SocialType = "twitter",
                        CreatedDate = media.CreatedAt
                    });
            }

            #endregion

            #region LinkedIn

            if (includeLinkedIn)
            {
                LinkedInService liService =
                    LinkedInService.CreateFromAccessToken("AQXNrQMxGL7iMKyGIZcOGGbBPgz7XrgOVgTmLQOJNOr-T49X4RefjG8G-rfExXmHMa92swlupOJbR7Zw17xR673zXMMoAP4htgpK00AaDpAWgqSh29tE1WMcHmQWabJ5JfTY0WlYHgi4u4BEPyIAVHjQ-rpVCeREL5KofB678shPuFIj7Rc");

                //CompanyId: 9459579
                LinkedInUpdate[] liData = liService.Companies.GetUpdates(9459579).Body.Values;

                resLinkedIn.AddRange(
                    from media in liData
                    select new MadeSoMeSocialItem
                    {
                        Id = media.UpdateContent.CompanyStatusUpdate.Share.Id ?? "-1",
                        Link = media.UpdateContent.CompanyStatusUpdate.Share.Content != null ? media.UpdateContent.CompanyStatusUpdate.Share.Content.EyebrowUrl : null,
                        Picture = media.UpdateContent.CompanyStatusUpdate.Share.Content != null ? media.UpdateContent.CompanyStatusUpdate.Share.Content.SubmittedImageUrl : null,
                        Title = media.UpdateContent.CompanyStatusUpdate.Share.Content != null ? media.UpdateContent.CompanyStatusUpdate.Share.Comment : null,
                        LikeCount = 0,
                        SocialType = "linkedIn",
                        CreatedDate = media.Timestamp
                    });


            }

            #endregion

            #region News

            if (includeNews)
            {
                var newsData = MadeContext.Current.Repositories.News.SearchNews(limit: 20, offset: 0).Items.Select(x => x as MadeNewsItem);

                resNews.AddRange(
                    from media in newsData
                    select new MadeSoMeSocialItem
                    {
                        Id = media.Id.ToString(CultureInfo.InvariantCulture),
                        Link = media.Url,
                        Picture = media.ImageUrl,
                        Title = media.Name,
                        LikeCount = 0,
                        SocialType = "news",
                        Category = media.NewsType,
                        CreatedDate = media.NewsDate
                    });
            }

            #endregion

            #region Events

            if (includeEvents)
            {
                var eventData =
                    MadeContext.Current.Repositories.Events.SearchEvents(limit: 20, offset: 0)
                        .Items.Select(x => x as MadeEventItem);

                resEvents.AddRange(
                    from media in eventData
                    select new MadeSoMeSocialItem
                    {
                        Id = media.Id.ToString(CultureInfo.InvariantCulture),
                        Link = media.Url,
                        Picture = media.ImageUrl,
                        Title = media.Name,
                        Teaser = media.Teaser,
                        LikeCount = 0,
                        SocialType = "events",
                        Category = null,
                        CreatedDate = media.Startdate
                    });
            }

            #endregion



            #region Output

            int[] totals = { resInstagram.Count / 2, resTwitter.Count / 3, resLinkedIn.Count / 2, resNews.Count / 3, resEvents.Count / 3 };
            //int totalCount = resInstagram.Count + resTwitter.Count + resLinkedIn.Count + resNews.Count + resEvents.Count;

            var results = new List<object>();

            results.Add(new
            {
                instagram = resInstagram.Skip(offset * 2).Take(2),
                twitter = resTwitter.Skip(offset * 4).Take(4),
                linkedin = resLinkedIn.Skip(offset * 2).Take(2),
                news = resNews.Skip(offset * 3).Take(3),
                events = resEvents.Skip(offset * 3).Take(3)
            });

            // output
            return new MadeSoMeSearchResult
            {
                Items = results.ToArray(),
                Total = totals.Max(),
                Offset = 0,
                Limit = 999
            };

            #endregion
        }

        #endregion

    }
}
