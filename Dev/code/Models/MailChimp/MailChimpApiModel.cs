﻿using MailChimp.Helper;
using Newtonsoft.Json;

namespace made.Models.MailChimp
{
    public class MailChimpApiModel
    {
        [JsonProperty("status")]
        public string Staus { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("euid")]
        public string Euid { get; set; }

        [JsonProperty("leid")]
        public string Leid { get; set; }


        public static MailChimpApiModel GetFromContent(EmailParameter a)
        {
            return new MailChimpApiModel
            {
                Staus = "success",
                Email = a.Email,
                Euid = a.EUId,
                Leid = a.LEId
            };
        }
    }
}
