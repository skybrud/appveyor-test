﻿using System;
using System.Linq;
using made.Extensions;
using made.Interfaces;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Models.News
{
    public class MadeNewsItem : ISearchResultItem
    {
        [JsonProperty("teaser")]
        public string Teaser { get; private set; }

        [JsonProperty("displayDate")]
        public DateTime DisplayDate { get; private set; }

        [JsonProperty("newsdate")]
        public DateTime NewsDate { get; private set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; private set; }

        [JsonProperty("activityTypes")]
        public string[] ActivityTypes { get; private set; }

        [JsonProperty("newstype")]
        public string NewsType { get; private set; }

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }


        public static MadeNewsItem GetFromId(int id)
        {
            return GetFromContent(UmbracoContext.Current.ContentCache.GetById(id));
        }

        public static MadeNewsItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            IPublishedContent m = content.HasValue("overviewImage") ? content.TypedMedia("overviewImage") : null;

            return new MadeNewsItem
            {
                Id = content.Id,
                Name = content.HasValue("title") ? content.GetPropertyValue<string>("title") : content.Name,
                Teaser = content.GetPropertyValue<string>("teaser").Nl2P().ToString(),
                Url = content.Url,
                NewsDate = content.GetPropertyValue<DateTime>("contentDate"),
                DisplayDate = content.GetPropertyValue<DateTime>("contentDate"),
                ImageUrl =
                    m != null
                        ? m.GetCropUrl(Convert.ToInt32(m.GetPropertyValue<string>("umbracoWidth")),
                            Convert.ToInt32(m.GetPropertyValue<string>("umbracoheight")),
                            imageCropMode: ImageCropMode.Crop, preferFocalPoint: true)
                        : null,
                ActivityTypes = content.TypedCsvContent("activitytypes").Select(x => x.GetTitle()).ToArray(),
                NewsType = content.HasValue("newstype") ? content.TypedContent("newstype").Name : null
            };
        }
    }
}