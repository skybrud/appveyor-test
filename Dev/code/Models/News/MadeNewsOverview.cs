﻿using made.Models.Categories;
using made.Models.Website;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace made.Models.News
{
    public class MadeNewsOverview : Master
    {
        #region Properties

        [JsonProperty("filters")]
        public MadeCategory[] Filters { get; private set; }

        #endregion

        protected MadeNewsOverview(IPublishedContent content)
            : base(content)
        {
            Filters = MadeContext.Current.Repositories.News.GetEventCategories();
        }

        public static MadeNewsOverview GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeNewsOverview(content);
        }
    }
}