﻿using System.Collections.Generic;
using System.Linq;
using made.Models.Categories;
using made.Models.SearchResults;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.News
{
    public class MadeNewsRepository
    {
        #region Public methods

        public MadeSearchResult SearchNews(
            string theme = "",
            string activitytype = "",
            string type = "",
            int limit = 10,
            int offset = 0)
        {
            string[] arrThemes = theme.Split(',');
            string[] arrActType = activitytype.Split(',');
            string[] arrTypes = type.Split(',');


            return Search.SearchNewsDocuments(arrThemes, arrActType, arrTypes, limit, offset);
        }


        /// <summary>
        /// Get categories for filtering
        /// </summary>
        /// <returns></returns>
        public MadeCategory[] GetEventCategories()
        {
            List<MadeCategory> output = new List<MadeCategory>();

            //Themes
            IPublishedContent themes = UmbracoContext.Current.ContentCache.GetSingleByXPath("//MadeTemaer");

            if (themes != null)
            {
                MadeCategory category = new MadeCategory
                {
                    Name = "Tema",
                    Alias = "theme",
                    Items = themes.Children.Where(x => !x.Hidden()).Select(MadeCategoryItem.GetFromContent).ToArray()
                };

                output.Add(category);
            }

            //Activities
            MadeCategory activityCat = MadeContext.Current.Repositories.Categories.GetCategoryById(1118, "Aktiviteter");

            if (activityCat != null)
            {
                output.Add(activityCat);
            }


            //Types
            MadeCategory typeCat = MadeContext.Current.Repositories.Categories.GetCategoryById(1119, "Type");

            if (typeCat != null)
            {
                output.Add(typeCat);
            }

            return output.ToArray();
        }

        #endregion
    }
}