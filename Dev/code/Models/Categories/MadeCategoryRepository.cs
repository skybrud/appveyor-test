﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Categories
{
    public class MadeCategoryRepository
    {

        /// <summary>
        /// Return MadeCategory by parentId
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public MadeCategory GetCategoryById(int nodeId, string categoryName = "")
        {
            IPublishedContent parent = UmbracoContext.Current.ContentCache.GetById(nodeId);

            if (parent == null) return null;

         
            return new MadeCategory
                {
                    Name = !string.IsNullOrEmpty(categoryName) ? categoryName : parent.Name,
                    Alias = parent.GetPropertyValue<string>("alias"),
                    Items = parent.Children.Where(x => !x.Hidden()).Select(MadeCategoryItem.GetFromContent).ToArray()
                };
        }

    }
}
