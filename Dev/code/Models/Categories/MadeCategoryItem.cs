﻿using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace made.Models.Categories
{
    public class MadeCategoryItem
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }


        public static MadeCategoryItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            return new MadeCategoryItem
            {
                Id = content.Id,
                Name = content.Name
            };
        }
    }
}