﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Themes
{
    public class MadeThemeItem
    {
        #region Properties

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("number")]
        public int Number { get; private set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("url")]
        public string Url { get; private set; }

        #endregion


        protected MadeThemeItem(IPublishedContent content)
        {
            Name = content.HasValue("title") ? content.GetPropertyValue<string>("title") : content.Name;
            Icon = content.GetPropertyValue<string>("icon");
            Number = content.SortOrder + 1;
            Url = content.Url;
        }

        public static MadeThemeItem GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeThemeItem(content);
        }
    }
}
