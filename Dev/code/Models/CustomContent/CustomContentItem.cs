﻿using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Models.CustomContent
{
    public class CustomContentItem
    {

        #region Properties

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("imageurl")]
        public string ImageUrl { get; private set; }

        #endregion


        #region Static methods

        public static CustomContentItem GetFromContent(IPublishedContent content, string imagePropertyAlias = "")
        {
            if (content == null) return null;

            IPublishedContent media = content.HasProperty(imagePropertyAlias) && content.HasValue(imagePropertyAlias)
                ? content.TypedMedia(imagePropertyAlias)
                : null;

            return new CustomContentItem
            {
                Id = content.Id,
                Name = content.Name,
                ImageUrl = media != null ? media.GetCropUrl(50, 50, imageCropMode: ImageCropMode.Crop, preferFocalPoint: true) : null
            };
        }

        #endregion
    }
}