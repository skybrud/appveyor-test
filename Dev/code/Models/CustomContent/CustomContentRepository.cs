﻿using System.Linq;
using Umbraco.Web;

namespace made.Models.CustomContent
{
    public class CustomContentRepository
    {
        internal static object GetItemsByIds(int[] ids)
        {
            return (
                from id in ids
                let item = UmbracoContext.Current.ContentCache.GetById(id)
                where item != null
                select CustomContentItem.GetFromContent(item, "image")
                ).ToArray();
        }
    }
}