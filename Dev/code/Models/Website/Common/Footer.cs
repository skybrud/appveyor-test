﻿using System.Web.Mvc;
using made.Extensions;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Website.Common
{
    public class Footer
    {
        #region Properties

        [JsonProperty("companyname")]
        public string Companyname { get; private set; }

        [JsonProperty("address")]
        public string Address { get; private set; }

        [JsonProperty("email")]
        public string Email { get; private set; }

        [JsonProperty("cvr")]
        public string Cvr { get; private set; }

        [JsonProperty("introheadline")]
        public string IntroHeadline { get; private set; }

        [JsonProperty("introtext")]
        public string IntroText { get; private set; }

        [JsonProperty("mailChimpListId")]
        public string MailChimpListId { get; private set; }

        #endregion

        #region Static methods

        public static Footer GetFromContent(IPublishedContent content)
        {
            return new Footer
            {
                Companyname = content.GetPropertyValue<string>("firmanavn"),
                Address = content.GetPropertyValue<string>("adresse").Nl2P().ToString(),
                Email = content.GetPropertyValue<string>("eMail"),
                Cvr = content.GetPropertyValue<string>("cvr"),
                IntroHeadline = content.GetPropertyValue<string>("introHeadline"),
                IntroText = content.GetPropertyValue<string>("introtext").Nl2P().ToString(),
                MailChimpListId = content.GetPropertyValue<string>("mailChimpListId")
            };
        }

        #endregion
    }
}