﻿using System;
using System.Collections.Generic;

namespace made.Models.Website.Common {
    public static class HeroRepository {
        public static string GetHeroUrl() {
            var urls = new List<string>();

            // img/6153ma/hero/hero_{01-18}.jpg

            var rnd = new Random();

            int i = rnd.Next(1, 19); //generere tal mellem 1 og 18
            string number;

            if (i < 10) {
                number = "0" + i;
            }
            else {
                number = i + "";
            }

            return string.Format("/img/6153ma/hero/hero_{0}.jpg", number);
        }
    }
}