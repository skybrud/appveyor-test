﻿using System.Collections.Generic;
using made.Models.Website.SPA;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Website.Common
{
    public class Settings
    {
        [JsonProperty("sitename")]
        public string Sitename { get; private set; }

        [JsonProperty("siteRootId")]
        public int SiteRootId { get; private set; }

        [JsonProperty("mainNavigation")]
        public IEnumerable<NavItem> MainNavigation { get; set; }

        [JsonProperty("secNavigation")]
        public IEnumerable<NavItem> SecNavigation { get; set; }

        [JsonProperty("footer")]
        public Footer Footer { get; private set; }
        

        public static Settings GetFromContent(IPublishedContent content, Settings model){

            return new Settings{
                Sitename = content.GetPropertyValue<string>("sitename"),
                SiteRootId = content.Id,
                MainNavigation = NavItem.GetItems(content.TypedCsvContent("mainNav")),
                SecNavigation = NavItem.GetItems(content.TypedCsvContent("secNav")),
                Footer = Footer.GetFromContent(content)
            };
        }
    }
}
