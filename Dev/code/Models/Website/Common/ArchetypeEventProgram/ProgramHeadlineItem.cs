﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using made.Interfaces;

namespace made.Models.Website.Common.ArchetypeEventProgram
{
    public class ProgramHeadlineItem : ProgramItem
    {
        public string Type { get; set; }
        public string Headline { get; set; }
    }
}
