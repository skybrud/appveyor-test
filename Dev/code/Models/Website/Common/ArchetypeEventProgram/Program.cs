﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Archetype.Models;
using made.Extensions;
using Umbraco.Core.Logging;

namespace made.Models.Website.Common.ArchetypeEventProgram
{
    public class Program
    {


        public static IEnumerable<ProgramItem> GetFromContent(ArchetypeModel content)
        {

            List<ProgramItem> items = new List<ProgramItem>();

            foreach (ArchetypeFieldsetModel fieldset in content.Fieldsets)
            {
                if (!fieldset.Disabled)
                {
                    if (fieldset.Alias == "overskrift")
                    {
                        items.Add(new ProgramHeadlineItem
                        {
                            Type = fieldset.Alias,
                            Headline = fieldset.GetValue<string>("headline")
                        });
                    }
                    else if (fieldset.Alias == "programPunkt")
                    {
                        items.Add(new ProgramTimeItem
                        {
                            Type = fieldset.Alias,
                            Headline = fieldset.GetValue<string>("headline"),
                            Time = fieldset.GetValue<string>("time"),
                            Description = fieldset.GetValue<string>("description")
                        });
                    }
                }
            }


            return items;
        }
    }
}
