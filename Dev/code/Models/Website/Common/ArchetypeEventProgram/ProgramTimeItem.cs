﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using made.Interfaces;

namespace made.Models.Website.Common.ArchetypeEventProgram
{
    public class ProgramTimeItem : ProgramItem
    {
        public string Headline { get; set; }
        public string Time { get; set; }
        public string Description { get; set; }
    }
}
