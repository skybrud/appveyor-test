﻿using made.Models.Website;
using Umbraco.Core.Models;

namespace made.Models.Website
{
    public class StartView : Master
    {
        #region properties

        #endregion

        #region constructers

        protected StartView(IPublishedContent content)
            : base(content)
        {
        }

        #endregion

        #region static methods

        public static StartView GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new StartView(content);
        }

        #endregion
    }
}