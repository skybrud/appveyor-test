﻿using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Website.SiteSearch
{
    public class SiteSearchResult
    {
        #region Properties

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("teaser")]
        public string Teaser { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }

        #endregion


        #region Static methods

        public static SiteSearchResult GetFromContent(IPublishedContent content){
            return new SiteSearchResult{
                Id = content.Id,
                Name = content.Name,
                Teaser = content.GetPropertyValue<string>("teaser"),
                Url = GetUrl(content)
            };
        }

        #endregion


        #region Private methods

        private static string GetUrl(IPublishedContent content)
        {
            string url = "";

            switch (content.DocumentTypeAlias.ToLower())
            {
                case "madeperson":
                    url = string.Format("{0}?keywords={1}", content.Parent.Url, content.Name);
                    break;

                    //TODO: måske også til faq

                default:
                    url = content.Url;
                    break;
            }

            return url;
        }



        #endregion
    }
}
