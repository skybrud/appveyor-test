﻿using System.Linq;
using made.Models.Website.Made.Items;
using Newtonsoft.Json;
using Skybrud.LinkPicker;
using Skybrud.LinkPicker.Extensions;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Website.Made
{
    public class MadeUnderforside : Master
    {
        #region Properties

        [JsonProperty("btnTitle")]
        public string BtnTitle { get; private set; }

        [JsonProperty("btnLinkItem")]
        public LinkPickerItem BtnLinkItem { get; private set; }

        [JsonProperty("items")]
        public OverviewItem[] Items { get; private set; }

        #endregion

        protected MadeUnderforside(IPublishedContent content)
            : base(content)
        {
            BtnTitle = content.GetPropertyValue<string>("btnTitle");
            BtnLinkItem = content.GetLinkPickerItem("btnLink");

            Items = content.Children(x => !x.Hidden()).Select(OverviewItem.GetFromContent).ToArray();
        }

        public static MadeUnderforside GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeUnderforside(content);
        }
    }
}