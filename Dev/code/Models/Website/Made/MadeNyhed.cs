﻿using System;
using System.Linq;
using made.Models.Themes;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Website.Made
{
    public class MadeNyhed : MadeUnderside
    {
        #region Properties

        [JsonProperty("newsdate")]
        public DateTime NewsDate { get; private set; }

        [JsonProperty("themes")]
        public MadeThemeItem[] Themes { get; private set; }

        [JsonProperty("activitytype")]
        public string ActivityType { get; private set; }

        [JsonProperty("newstype")]
        public string NewsType { get; private set; }

        [JsonProperty("year")]
        public string Year { get; private set; }

        #endregion

        #region Constructors

        protected MadeNyhed(IPublishedContent content)
            : base(content)
        {
            NewsDate = content.GetPropertyValue<DateTime>("contentDate");

            Themes = content.HasValue("themes") ? content.TypedCsvContent("themes").Select(MadeThemeItem.GetFromContent).ToArray() : null;
            //ActivityType = ;
            //NewsType =;

            Year = content.GetPropertyValue<DateTime>("contentDate").ToString("yyyy");
        }

        #endregion

        #region Static methods

        public new static MadeNyhed GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeNyhed(content);
        }

        #endregion
    }
}