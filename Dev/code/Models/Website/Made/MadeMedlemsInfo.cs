﻿using Umbraco.Core.Models;

namespace made.Models.Website.Made {
    public class MadeMedlemsInfo : Master {
        public MadeMedlemsInfo(IPublishedContent content) : base(content) {}


        public static MadeMedlemsInfo GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeMedlemsInfo(content);
        }
    }

    
}