﻿using System;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Models.Website.Made.Items
{
    /// <summary>
    /// Klassen bruges til at hente data til underforsidernes items-egenskab
    /// </summary>
    public class OverviewItem
    {
        #region Properties

        [JsonProperty("title")]
        public string Title { get; private set; }

        [JsonProperty("teaser")]
        public string Teaser { get; private set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }

        #endregion

        #region Static properties

        public static OverviewItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            IPublishedContent m = content.HasValue("overviewImage") ? content.TypedMedia("overviewImage") : null;

            return new OverviewItem
            {
                Title = content.HasValue("title") ? content.GetPropertyValue<string>("title") : content.Name,
                Teaser = content.GetPropertyValue<string>("teaser"),
                ImageUrl =
                    m != null
                        ? m.GetCropUrl(Convert.ToInt32(m.GetPropertyValue<string>("umbracoWidth")),
                            Convert.ToInt32(m.GetPropertyValue<string>("umbracoheight")),
                            imageCropMode: ImageCropMode.Crop, preferFocalPoint: true)
                        : null,
                Url = content.HasProperty("umbracoRedirect") && content.HasValue("umbracoRedirect") 
                ? content.TypedContent("umbracoRedirect").Url
                : content.Url
            };
        }

        #endregion
    }
}