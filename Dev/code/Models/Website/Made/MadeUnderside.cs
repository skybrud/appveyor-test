﻿using made.Grid.Spa;
using Newtonsoft.Json;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions;
using Umbraco.Core.Models;

namespace made.Models.Website.Made
{
    public class MadeUnderside : Master
    {
        #region Properties

        [JsonProperty("grid")]
        [JsonConverter(typeof (SpaGridJsonConverter))]
        public GridDataModel Grid { get; private set; }

        #endregion

        protected MadeUnderside(IPublishedContent content)
            : base(content)
        {
            Grid = content.GetGridModel("grid");
        }

        public static MadeUnderside GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeUnderside(content);
        }
    }
}