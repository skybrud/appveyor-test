﻿using System.Collections.Generic;
using System.Linq;
using made.Models.MadeMembers;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace made.Models.Website.Made
{
    public class MadeMedlemmer : Master
    {
        #region Properties

        [JsonProperty("items")]
        public List<MadeMedlemCategory> Items { get; private set; }

        #endregion

        protected MadeMedlemmer(IPublishedContent content)
            : base(content)
        {
            Items = content.Children.Any()
                ? content.Children.Select(MadeMedlemCategory.GetFromContent).ToList()
                : null;
        }

        public static MadeMedlemmer GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeMedlemmer(content);
        }
    }
}