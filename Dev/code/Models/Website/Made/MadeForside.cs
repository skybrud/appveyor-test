﻿using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using StackExchange.Profiling.Data;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Website.Made
{
    public class MadeForside : Master
    {
        #region Properties

        [JsonProperty("introtext")]
        public string IntroText { get; private set; }

        [JsonProperty("linkReadMore")]
        public string LinkReadMore { get; private set; }

        [JsonProperty("linkReadMoreText")]
        public string LinkReadMoreText { get; private set; }

        #endregion

        protected MadeForside(IPublishedContent content)
            : base(content)
        {

            IntroText = content.GetPropertyValue<string>("introtext");
            LinkReadMore = content.HasValue("linkReadMore") ? content.TypedContent("linkReadMore").Url : null;
            LinkReadMoreText = content.HasValue("linkReadMoreText") ? content.GetPropertyValue<string>("linkReadMoreText") : content.TypedContent("linkReadMore").Name;
        }

        public static MadeForside GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeForside(content);
        }
    }
}
