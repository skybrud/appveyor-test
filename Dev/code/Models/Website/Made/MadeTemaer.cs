﻿using Umbraco.Core.Models;

namespace made.Models.Website.Made
{
    public class MadeTemaer : Master
    {
        #region Properties

        #endregion

        protected MadeTemaer(IPublishedContent content)
            : base(content)
        {
        }

        public static MadeTemaer GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeTemaer(content);
        }
    }
}