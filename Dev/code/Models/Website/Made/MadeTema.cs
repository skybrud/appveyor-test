﻿using System.Collections.Generic;
using System.Linq;
using made.Models.Themes;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.Website.Made
{
    public class MadeTema : MadeUnderside
    {
        #region Properties

        [JsonProperty("icon")]
        public string Icon { get; private set; }

        [JsonProperty("number")]
        public int Number { get; private set; }

        [JsonProperty("focustext")]
        public string Focustext { get; set; }

        [JsonProperty("niveautext")]
        public string Niveautext { get; set; }

        [JsonProperty("themes")]
        public List<MadeThemeItem> Themes { get; private set; }

        #endregion


        protected MadeTema(IPublishedContent content)
            : base(content)
        {
            Icon = content.GetPropertyValue<string>("icon");
            Number = content.SortOrder + 1;
            Focustext = content.GetPropertyValue<string>("focus");
            Niveautext = content.GetPropertyValue<string>("niveau");
            Themes = content.Parent.Children.ToList().Where(x => !x.Hidden()).Select(MadeThemeItem.GetFromContent).ToList();

        }

        public static MadeTema GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeTema(content);
        }
    }
}