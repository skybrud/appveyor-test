﻿using made.Models.Faq;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace made.Models.Website.Made
{
    public class MadeFaqside : MadeUnderside
    {
        #region Properties

        [JsonProperty("faqCategories")]
        public MadeFaqCategory[] FaqCategories { get; private set; }

        #endregion

        #region Constructors

        public MadeFaqside(IPublishedContent content) : base(content)
        {
            FaqCategories = MadeContext.Current.Repositories.Faq.GetFaqCategories();
        }

        #endregion

        #region Static Methods

        public static new MadeFaqside GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadeFaqside(content);
        }

        #endregion
    }
}