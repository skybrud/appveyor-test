﻿using System.Linq;
using made.Models.Persons;
using Newtonsoft.Json;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;

namespace made.Models.Website.Made
{
    public class MadePersoner : Master
    {
        #region Properties

        [JsonProperty("items")]
        public MadePersonItem[] Items { get; private set; }

        #endregion

        protected MadePersoner(IPublishedContent content)
            : base(content)
        {
            Items = content.Children.Where(x => !x.Hidden()).Select(MadePersonItem.GetFromContent).ToArray();
        }

        public static MadePersoner GetFromContent(IPublishedContent content)
        {
            return content == null ? null : new MadePersoner(content);
        }
    }
}