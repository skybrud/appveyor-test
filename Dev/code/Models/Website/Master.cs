﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CookComputing.XmlRpc;
using made.Models.Website.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SEOChecker.MVC;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Models.Website
{
    public class Master
    {
        #region Properties
        
        [JsonIgnore]
        public IPublishedContent Content { get; private set; }

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }

        [JsonProperty("contentTitle")]
        public string ContentTitle { get; private set; }

        [JsonProperty("contentTeaser")]
        public string ContentTeaser { get; protected set; }

        [JsonProperty("overviewImageUrl")]
        public string OverviewImageUrl { get; private set; }

        [JsonProperty("herourl")]
        public string HeroUrl { get; private set; }

        [JsonProperty("noCache")]
        public bool NoCache { get; set; }

        [JsonProperty("contentChangesGuid")]
        public Guid ContentChangesGuid { get; set; }

        [JsonProperty("jsChangesGuid")]
        public Guid JsChangesGuid { get; set; }

        [JsonIgnore]
        public bool HideFromSearch { get; private set; }

        [JsonProperty("bodyClass")]
        public string BodyClass { get; protected set; }

        [JsonProperty("path")]
        public int[] Path { get; private set; }

        [JsonProperty("templatename")]
        public string Templatename { get; private set; }

        [JsonProperty("created")]
        public DateTime Created { get; private set; }

        [JsonProperty("updated")]
        public DateTime Updated { get; private set; }

        [JsonProperty("jsonDebug")]
        public JObject JsonDebug { get; private set; }

        [JsonProperty("labels")]
        public Dictionary<string, object> Labels { get; private set; }

        #endregion


        #region Constructors

        public Master(IPublishedContent content) {


            IPublishedContent m = content.HasValue("overviewImage") ? content.TypedMedia("overviewImage") : null;

            Content = content;
            Id = content.Id;
            Name = content.Name;
            Url = content.Url;

            ContentTitle = content.HasValue("title") ? content.GetPropertyValue<string>("title") : content.Name;
            ContentTeaser = content.GetPropertyValue<string>("teaser");
            OverviewImageUrl = m != null
                ? m.GetCropUrl(Convert.ToInt32(m.GetPropertyValue<string>("umbracoWidth")),
                    Convert.ToInt32(m.GetPropertyValue<string>("umbracoheight")),
                    imageCropMode: ImageCropMode.Crop, preferFocalPoint: true)
                : null;
            HeroUrl = HeroRepository.GetHeroUrl();
            
            HideFromSearch = content.HasProperty("hideFromSearch") && content.GetPropertyValue<bool>("hideFromSearch");

            BodyClass = "";
            Path = content.Path.Split(',').Select(x => Convert.ToInt32(x)).Skip(1).ToArray();
            Templatename = content.GetTemplateAlias() + ".html";
            Created = content.CreateDate;
            Updated = content.UpdateDate;
            NoCache = content.GetPropertyValue<bool>("noCache");
            ContentChangesGuid = Startup.ContentChangesGuid;
            JsChangesGuid = Startup.JsChangesGuid;
            JsonDebug = content.HasProperty("jsonData") && content.HasValue("jsonData") ? JObject.Parse(content.GetPropertyValue<string>("jsonData").Remove(0, 1)) : new JObject();
            Labels = ConvertLabels(ApplicationContext.Current.Services.LocalizationService.GetRootDictionaryItems().ToList());
        }

        #endregion 


        public bool IsOfType<T>() where T : Master
        {
            T casted = this as T;
            return casted != null;
        }

        public bool IsOfType<T>(out T value) where T : Master
        {
            value = this as T;
            return value != null;
        }


        private Dictionary<string, object> ConvertLabels(IEnumerable<IDictionaryItem> labels)
        {
            Dictionary<string, object> labelsList = new Dictionary<string, object>();

            if (labels.Any())
            {
                foreach (IDictionaryItem label in labels)
                {
                    labelsList.Add(label.ItemKey, label.Translations.First(t => t.Language.CultureInfo.ThreeLetterISOLanguageName == CultureInfo.CurrentCulture.ThreeLetterISOLanguageName).Value);
                }
            }

            return labelsList;
        }
    }
}
