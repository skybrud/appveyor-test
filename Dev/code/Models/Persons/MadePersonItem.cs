﻿using System;
using System.Linq;
using made.Extensions;
using made.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Models.Persons
{
    public class MadePersonItem : ISearchResultItem
    {
        [JsonProperty("id")]
        public int Id { get; private set; }
        
        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("title")]
        public string Title { get; private set; }

        [JsonProperty("description")]
        public string Description { get; private set; }

        [JsonProperty("company")]
        public string Companyname { get; private set; }

        [JsonProperty("address")]
        public string Address { get; private set; }

        [JsonProperty("addressMore")]
        public string MoreAddress { get; private set; }

        [JsonProperty("zip")]
        public string Zip { get; private set; }

        [JsonProperty("city")]
        public string City { get; private set; }

        [JsonProperty("phone")]
        public string Phone { get; private set; }

        [JsonProperty("email")]
        public string Email { get; private set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; private set; }

        [JsonProperty("url")]
        public string Url { get; private set; }


        public static MadePersonItem GetFromId(int id)
        {
            return GetFromContent(UmbracoContext.Current.ContentCache.GetById(id));
        }

        public static MadePersonItem GetFromContent(IPublishedContent content)
        {
            if (content == null) return null;

            IPublishedContent m = content.HasValue("image") ? content.TypedMedia("image") : null;
                
            return new MadePersonItem
            {
                Id = content.Id,
                Name = content.HasValue("name") ? content.GetPropertyValue<string>("name") : content.Name,
                Title = content.GetPropertyValue<string>("title"),
                Description = Umbraco.Web.Templates.TemplateUtilities.ParseInternalLinks(content.GetPropertyValue<string>("description")),
                Companyname = content.GetPropertyValue<string>("companyname"),
                Address = content.GetPropertyValue<string>("address"),
                MoreAddress = content.GetPropertyValue<string>("moreAddress"),
                Zip = content.GetPropertyValue<string>("zip"),
                City = content.GetPropertyValue<string>("city"),
                Phone = content.GetPropertyValue<string>("phone"),
                Email = content.GetPropertyValue<string>("email"),
                ImageUrl =
                    m != null
                        ? m.GetCropUrl(Convert.ToInt32(m.GetPropertyValue<string>("umbracoWidth")),
                            Convert.ToInt32(m.GetPropertyValue<string>("umbracoheight")),
                            imageCropMode: ImageCropMode.Crop, preferFocalPoint: true)
                        : null,
                Url = content.Url
            };
        }
    }
}