﻿using Newtonsoft.Json;

namespace made.Models.SearchResults
{
    public class MadeSearchResultGroup
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }
    }
}
