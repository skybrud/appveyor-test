﻿using made.Interfaces;
using Newtonsoft.Json;

namespace made.Models.SearchResults
{
    public class MadeSearchResult
    {
        [JsonProperty("total")]
        public int Total { get; internal set; }

        [JsonProperty("offset")]
        public int Offset { get; internal set; }

        [JsonProperty("limit")]
        public int Limit { get; internal set; }

        [JsonProperty("groups")]
        public MadeSearchResultGroup[] Groups { get; internal set; }

        [JsonProperty("items")]
        public ISearchResultItem[] Items { get; internal set; }

        [JsonIgnore]
        public string ExamineQuery { get; internal set; }

    }
}