﻿using made.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made.Models.SearchResults
{
    public class MadeSearchResultItem : ISearchResultItem
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Url { get; private set; }


        public static MadeSearchResultItem GetFromId(int id)
        {
            return GetFromContent(UmbracoContext.Current.ContentCache.GetById(id));
        }

        public static MadeSearchResultItem GetFromContent(IPublishedContent content)
        {
            return new MadeSearchResultItem
            {
                Id = content.Id,
                Name = content.HasValue("title") ? content.GetPropertyValue<string>("title") : content.Name,
                Url = content.Url
            };
        }
    }
}
