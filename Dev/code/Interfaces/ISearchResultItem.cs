﻿using System;

namespace made.Interfaces
{
    public interface ISearchResultItem
    {
        Int32 Id { get; }
        String Name { get; }
        String Url { get; }
    }
}
