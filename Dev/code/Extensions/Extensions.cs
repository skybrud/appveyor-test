﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace made.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Konvertere en tekst med linieskift til html (<br /> eller <p>)
        /// </summary>
        /// <param name="text">Strengen der skal konverteres</param>
        /// <param name="cssClass">Hvis udfyldt, sættes dette klassenavn rundt om teksten i et p-tag</param>
        /// <returns></returns>
        public static MvcHtmlString Nl2P(this string text, string cssClass = "")
        {
            text = text ?? "";

            text = text.Trim().Replace("\r", "");

            var start = cssClass == "" ? "<p>" : string.Format("<p class=\"{0}\">", cssClass);

            string result = start + text
                 .Replace("\n\n", "</p>" + start)
                 .Replace("\n", "<br />")
                 .Replace("</p>" + start, "</p>" + Environment.NewLine + start) + "</p>";

            return new MvcHtmlString(result);
        }

        /// <summary>
        /// Returnere "contentDate" (nyhed) som datetime. Hvis dato ikke er sat eller egenskaben "contentDate" ikke findes, returneres dokumentets oprettelsesdato.
        /// </summary>
        /// <param name="content">IPublishedContent</param>
        /// <returns></returns>
        public static DateTime Date(this IPublishedContent content)
        {
            if (content.HasProperty("contentDate"))
            {
                var date = content.GetPropertyValue<DateTime>("contentDate");

                if (date != null && date != DateTime.MinValue)
                    return date;

                return content.CreateDate;
            }

            return content.CreateDate;
        }

        /// <summary>
        /// Returnere dokumentets sidedato i formateret tilstand
        /// </summary>
        /// <param name="content">IPublishedContent</param>
        /// <returns></returns>
        public static string SiteDate(this IPublishedContent content)
        {
            return content.Date().SiteDate();
        }

        /// <summary>
        /// Returnere en cropped url. Hvis egenskaben indeholder flere billeder, returneres det første billede
        /// </summary>
        /// <param name="content">IPublishedContent</param>
        /// <param name="property">Egenskabens alias</param>
        /// <param name="alias">CropUp alias</param>
        /// <returns></returns>
        public static string Image(this IPublishedContent content, string property, string alias)
        {
            var image = content.TypedCsvMedia(property).FirstOrDefault();

            if (image == null)
                return "";

            return image.GetCropUrl(propertyAlias: alias, preferFocalPoint: true);
        }

        /// <summary>
        /// Returnere en cropped url. Hvis egenskaben indeholder flere billeder, returneres det første billede
        /// </summary>
        /// <param name="content">IPublishedContent</param>
        /// <param name="property">Egenskabens alias</param>
        /// <param name="width">Bredde på CropUp</param>
        /// <param name="height">Højde på CropUp</param>
        /// <returns></returns>
        public static string Image(this IPublishedContent content, string property, int width, int height)
        {
            var image = content.TypedCsvMedia(property).FirstOrDefault();

            if (image == null)
                return "";

            return image.GetCropUrl(width, height, preferFocalPoint: true);
        }

        /// <summary>
        /// Returnere et IPublishedContent objekt af indstillingssiden
        /// </summary>
        /// <param name="helper">UmbracoHelper</param>
        /// <returns></returns>
        public static IPublishedContent SettingsPage(this UmbracoHelper helper)
        {
            return helper.ContentSingleAtXPath("//SOSU-SiteKontainer");
        }

        /// <summary>
        /// Returnere li'er til at bygge simpel navigation (main- / second navigation). Bruges til at rendere topmenuer ud fra en MNTP egenskab.
        /// </summary>
        /// <param name="content">IPublishedContent</param>
        /// <param name="model">RenderModel</param>
        /// <param name="property">Egenskabens navn</param>
        /// <param name="activeClassName">Klassenavn på aktivt menupunkt</param>
        /// <returns></returns>
        public static string MakeNavItems(this IPublishedContent content, RenderModel model, string property, string activeClassName)
        {
            var r = new StringBuilder();

            var data = content.TypedCsvContent(property);

            foreach (var item in data)
            {
                var act = model.Content.IsDescendantOrSelf(item);

                r.AppendLine(string.Format("<li><a href=\"{0}\" class=\"{1}\">{2}</a></li>", item.Url, "test", item.Name));
            }

            return r.ToString();
        }

        /// <summary>
        /// Returnere en liste af IPublishedContent objekter ud fra strukturen. Forsiden tilføjes ud fra indstillingssidens umbracoInternalRedirectId egenskab.
        /// </summary>
        /// <param name="content">IPublishedContent</param>
        /// <returns></returns>
        public static IEnumerable<IPublishedContent> BreadCrumb(this IPublishedContent content)
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);

            var breadcrumb = new List<IPublishedContent>();

            var frontpage = helper.TypedContent(helper.SettingsPage().GetPropertyValue<int>("umbracoInternalRedirectId"));

            breadcrumb.Add(frontpage);

            breadcrumb.AddRange(from pathId in content.Path.Split(',') where pathId != "-1" select helper.TypedContent(pathId) into item where item.Level > 1 select item);

            return breadcrumb;
        }

        public static String FormatToKilobytes(this string filesizeinbytes){

            int number = 0;

            int.TryParse(filesizeinbytes, out number);

            return (number/1024).ToString();

        }


        #region DateTime extensions

        public static string SiteDate(this DateTime date)
        {
            return date.ToString("d.MM.yyyy");
        }

        #endregion

        
    }
}
