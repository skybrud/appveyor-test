﻿//using sfi.Models.Sfi.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;


namespace made.Extensions
{
    public static class PublishedContentExtensions
    {

        public static string[] GetStringArray(this IPublishedContent content, string propertyAlias)
        {
            return GetStringArray(content, propertyAlias, new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] GetStringArray(this IPublishedContent content, string propertyAlias, StringSplitOptions options)
        {
            return GetStringArray(content, propertyAlias, new[] { ',' }, options);
        }

        public static string[] GetStringArray(this IPublishedContent content, string propertyAlias, char separator, StringSplitOptions options)
        {
            return GetStringArray(content, propertyAlias, new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] GetStringArray(this IPublishedContent content, string propertyAlias, char[] separator, StringSplitOptions options)
        {
            return content == null ? new string[0] : (content.GetPropertyValue<string>(propertyAlias) ?? "").Split(separator, options);
        }

        public static string GetString(this IPublishedContent content, string propertyAlias) {
            return content == null ? null : content.GetPropertyValue<string>(propertyAlias);
        }

        public static T GetString<T>(this IPublishedContent content, string propertyAlias, Func<string, T> function) {
            return content == null ? default(T) : function(content.GetPropertyValue<string>(propertyAlias));
        }

        public static int GetInt32(this IPublishedContent content, string propertyAlias) {
            return content == null ? 0 : content.GetPropertyValue<int>(propertyAlias);
        }

        public static T GetInt32<T>(this IPublishedContent content, string propertyAlias, Func<int, T> function) {
            return content == null ? default(T) : function(content.GetPropertyValue<int>(propertyAlias));
        }

        public static JObject GetJObject(this IPublishedContent content, string propertyAlias) {
            return content == null ? null : content.GetPropertyValue<JObject>(propertyAlias);
        }

        public static T GetJObject<T>(this IPublishedContent content, string propertyAlias, Func<JObject, T> function) {
            return content == null ? default(T) : function(content.GetPropertyValue<JObject>(propertyAlias));
        }

        public static HtmlString GetHtmlString(this IPublishedContent content, string propertyAlias)
        {
            return content == null ? null : content.GetPropertyValue<HtmlString>(propertyAlias);
        }

        public static DateTime GetDateTime(this IPublishedContent content, string propertyAlias, DateTime fallback)
        {
            return content == null || !content.HasValue(propertyAlias) ? fallback : content.GetPropertyValue<DateTime>(propertyAlias);
        }

        public static string GetTitle(this IPublishedContent content)
        {
            return content.HasValue("title") ? content.GetPropertyValue<string>("title") : content.Name;
        }

        //public static NavigationItem[] ToNavigationItems(this IEnumerable<IPublishedContent> items, string context, IPublishedContent current)
        //{
        //    if (items == null)
        //        return new NavigationItem[0];

        //    List<NavigationItem> hai = new List<NavigationItem>();

        //    foreach (IPublishedContent item in items)
        //    {

        //        if (context == "mobile") continue;

        //        hai.Add(new NavigationItem
        //        {
        //            Id = item.Id,
        //            Name = item.Name,
        //            Url = item.HasValue("redirectumbracoRedirect") ? item.TypedContent("redirectumbracoRedirect").Url : item.Url,
        //            Content = item,
        //            IsCurrent = current.Path.Contains("," + item.Id),
        //            Children = ToNavigationItems(item.Children, context, current)
        //        });

        //    }

        //    return hai.ToArray();
        //}

       
    }

}
