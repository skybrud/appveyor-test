﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using made.Models.Website;
using SEOChecker.MVC;
using Umbraco.Web;

namespace made
{
    public class Utils
    {
        public static class Helpers
        {

            public static class Tags
            {

                public static class Meta
                {
                    /// <summary>
                    /// Sætter noindex, nofollow på stage og localhost domæner
                    /// </summary>
                    /// <returns></returns>
                    public static string NoIndex()
                    {
                        var host = HttpContext.Current.Request.Url.Host;

                        if (host.Contains(".stage.") || host.Contains("localhost"))
                        {
                            return "<meta name=\"robots\" content=\"noindex, nofollow\">";
                        }

                        return "";
                    }
                }


                public static HtmlString RenderSeoCheckerTags(Master content)
                {
                    MetaData seoData = content.Content.GetPropertyValue<MetaData>("seoChecker");

                    if (seoData != null)
                    {
                        var host = HttpContext.Current.Request.Url.Host;
                        var output = new StringBuilder();


                        if (!host.Contains("6153ma."))
                        {
                            output.AppendLine(string.Format("<title ng-bind=\"pageTitle\">{0}</title>", seoData.Title));
                            output.AppendLine(string.Format("<meta name=\"description\" content=\"{0}\" />",
                                seoData.Description));
                            output.AppendLine(string.Format("<link rel=\"canonical\" href=\"{0}\" />",
                                seoData.CanonicalUrl));

                            output.AppendLine(content.HideFromSearch
                                ? "<meta name=\"robots\" content=\"noindex, nofollow\">"
                                : "<meta name=\"robots\" content=\"" + seoData.Robots + "\">");
                        }
                        else
                        {
                            output.AppendLine(string.Format("<title ng-bind=\"pageTitle\">{0}</title>", seoData.Title));
                            output.AppendLine(string.Format("<meta name=\"description\" content=\"{0}\" />",
                                seoData.Description));
                            output.AppendLine(string.Format("<link rel=\"canonical\" href=\"{0}\" />",
                                seoData.CanonicalUrl));
                            output.AppendLine("<meta name=\"robots\" content=\"noindex, nofollow\">");
                        }

                        return new HtmlString(output.ToString());
                    }

                    return
                        new HtmlString(string.Format("<title ng-bind=\"pageTitle\">{0} | {1}</title>", content.ContentTitle, "MADE"));
                }
            }
        }
    }
}
