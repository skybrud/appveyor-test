﻿using made.Models.Grid;
using Newtonsoft.Json.Linq;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Extensions.Json;
using Skybrud.Umbraco.GridData.Interfaces;
using Skybrud.Umbraco.GridData.Rendering;

namespace made.Grid {

    public class MadeGridConverter : IGridConverter {

        public bool ConvertControlValue(GridControl control, JToken token, out IGridControlValue value) {
            
            value = null;
            
            switch (control.Editor.Alias) {
                case "skybrud.linkpicker":
                case "skybrud.linkpicker.spot":
                    value = GridControlLinkPickerValue.Parse(token as JObject);
                    break;

                case "boxFacts":
                    value = GridControlBoxFactValue.Parse(token as JObject);
                    break;

                case "citat":
                    value = GridControlQuoteValue.Parse(token as JObject);
                    break;

                case "contactPerson":
                    value = GridControlContactPersonValue.Parse(token as JObject);
                    break;

                case "tools":
                case "cases":
                    value = GridControlBoxItemsValue.Parse(token as JObject);
                    break;

                case "faqs":
                    value = GridControlFaqValue.Parse(token as JObject);
                    break;

                case "macro":
                    value = GridControlMacroValue.Parse(control, token as JObject);
                    break;

                case "mailchimp":
                    value = GridControlMailChimpValue.Parse(control, token as JObject);
                    break;

                case "video":
                    value = GridControlVideoValue.Parse(control, token as JObject);
                    break;

                case "image":
                    value = GridControlImageValue.Parse(control, token as JObject);
                    break;

                case "gallery":
                    value = GridControlGalleryValue.Parse(control, token as JObject);
                    break;



                    //case "members":
                    //    value = GridControlContactPersonValue.Parse(token as JObject);
                    //    break;

                    //case "headline":
                    //    value = GridControlContactPersonValue.Parse(token as JObject);
                    //    break;



            }

            return value != null;
        
        }

        public bool ConvertEditorConfig(GridEditor editor, JToken token, out IGridEditorConfig config) {
            config = null;
            return false;
        }

        public bool GetControlWrapper(GridControl control, out GridControlWrapper wrapper) {
            
            wrapper = null;
            
            switch (control.Editor.Alias) {
                case "skybrud.linkpicker":
                    wrapper = control.GetControlWrapper<GridControlLinkPickerValue>();
                    break;

                case "boxFacts":
                    wrapper = control.GetControlWrapper<GridControlBoxFactValue>();
                    break;
            }
            
            return false;
        
        }

    }

}