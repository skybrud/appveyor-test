﻿using System;
using System.Collections.Generic;
using System.Linq;
using Examine;
using Examine.Providers;
using Examine.SearchCriteria;
using Lucene.Net.QueryParsers;
using made.Interfaces;
using made.Models.Events;
using made.Models.News;
using made.Models.SearchResults;
using Umbraco.Web;
using Umbraco.Web.PublishedCache;

namespace made
{
    internal class Search
    {
        public static MadeSearchResult SearchNewsDocuments(
            string[] themes = null,
            string[] activitytypes = null,
            string[] types = null,
            int limit = 10,
            int offset = 0
            )
        {
            string orderByFieldName = "contentDate_range";
            string[] documentTypes = {"MadeNyhed"};

            var query = new List<string>(); //collecting query object

            #region Build Query

            // DocumentTypes
            if (documentTypes.Length != 0)
                query.Add(string.Format("nodeTypeAlias:({0})",
                    string.Join(" ",
                        documentTypes.Select(a => string.Format("\"{0}\"", QueryParser.Escape(a))).ToArray())));

            // Themes
            if (themes != null && themes.Length != 0)
                query.Add(ConvertStringArrToQuery("themes_search", themes));

            // Regions
            if (activitytypes != null && activitytypes.Length != 0)
                query.Add(ConvertStringArrToQuery("activitytypes_search", activitytypes));

            // Types
            if (types != null && types.Length != 0)
                query.Add(ConvertStringArrToQuery("newstype_search", types));

            #endregion

            #region Build Search

            BaseSearchProvider externalSearcher = ExamineManager.Instance.SearchProviderCollection["ExternalSearcher"];
            ISearchCriteria criteria = externalSearcher.CreateSearchCriteria();
            criteria = criteria.RawQuery(string.Join(" AND ", query.ToArray()));

            #endregion

            #region Do Search

            SearchResult[] results = (
                from result in externalSearcher.Search(criteria)
                //orderby result.Fields[orderByFieldName] descending
                orderby
                    result.Fields.ContainsKey(orderByFieldName)
                        ? result.Fields[orderByFieldName]
                        : result.Fields["createDate"] descending
                select result
                ).ToArray();

            ContextualPublishedContentCache contentCache = UmbracoContext.Current.ContentCache;


            return new MadeSearchResult
            {
                Items = (
                    from result in results.Skip(offset).Take(limit)
                    select (ISearchResultItem) MadeNewsItem.GetFromContent(contentCache.GetById(result.Id))
                    ).ToArray(),
                Total = results.Count(),
                Offset = offset,
                Limit = limit,
                ExamineQuery = String.Join(" AND ", query.ToArray())
            };

            #endregion
        }


        public static MadeSearchResult SearchEventDocuments(
            string[] themes = null,
            string[] regions = null,
            string[] types = null,
            bool hasBeen = false,
            int limit = 10,
            int offset = 0
            )
        {
            string orderByFieldName = "startdato_range";
            string[] documentTypes = {"MadeArrangement"};

            var query = new List<string>(); //collecting query object

            #region Build Query

            // DocumentTypes
            if (documentTypes.Length != 0)
                query.Add(string.Format("nodeTypeAlias:({0})",
                    string.Join(" ",
                        documentTypes.Select(a => string.Format("\"{0}\"", QueryParser.Escape(a))).ToArray())));

            // Themes
            if (themes != null && themes.Length != 0)
                query.Add(ConvertStringArrToQuery("themes_search", themes));

            // Regions
            if (regions != null && regions.Length != 0)
                query.Add(ConvertStringArrToQuery("region_search", regions));

            // Types
            if (types != null && types.Length != 0)
                query.Add(ConvertStringArrToQuery("eventtype_search", types));

            // hasBeen
            if (hasBeen)
            {
                query.Add(string.Format(" +({0}:[{1} TO {2}])", "startdato_range",
                    DateTime.Now.AddYears(-5).ToString("yyyyMMddHHmm00000"), DateTime.Now.ToString("yyyyMMddHHmm00000")));
            }
            else
            {
                query.Add(string.Format(" +({0}:[{1} TO {2}])", "startdato_range",
                    DateTime.Now.ToString("yyyyMMddHHmm00000"), DateTime.Now.AddYears(5).ToString("yyyyMMddHHmm00000")));
            }

            #endregion

            #region Build Search

            BaseSearchProvider externalSearcher = ExamineManager.Instance.SearchProviderCollection["ExternalSearcher"];
            ISearchCriteria criteria = externalSearcher.CreateSearchCriteria();
            criteria = criteria.RawQuery(string.Join(" AND ", query.ToArray()));

            //LogHelper.Info<Search>(string.Join(" AND ", query.ToArray()));

            #endregion

            #region Do Search

            //SearchResult[] results = (
            //    from result in externalSearcher.Search(criteria)
            //    orderby
            //        result.Fields.ContainsKey(orderByFieldName)
            //            ? result.Fields[orderByFieldName]
            //            : result.Fields["createDate"] ascending
            //    select result
            //    ).ToArray();

            SearchResult[] results = externalSearcher.Search(criteria)
                .OrderBy(c => c.Fields["isMemberEvent"])
                .ThenBy(n => n.Fields[orderByFieldName])
                .ToArray();

            ContextualPublishedContentCache contentCache = UmbracoContext.Current.ContentCache;


            return new MadeSearchResult
            {
                Items = (
                    from result in results.Skip(offset).Take(limit)
                    select (ISearchResultItem) MadeEventItem.GetFromContent(contentCache.GetById(result.Id))
                    ).ToArray(),
                Total = results.Count(),
                Offset = offset,
                Limit = limit,
                ExamineQuery = String.Join(" AND ", query.ToArray())
            };

            #endregion
        }


        private static string ConvertStringArrToQuery(string fieldname, string[] arrData)
        {
            return string.Format("{0}:({1})", fieldname,
                string.Join(" ",
                    arrData.Select(a => string.Format("\"{0}\"", QueryParser.Escape(a))).ToArray()));
        }
    }
}