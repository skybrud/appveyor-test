﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using made.Models.Website.Common;
using StackExchange.Profiling;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace made
{
    public class MadeContext
    {
        private MadeContext()
        {
            Repositories = new MadeRepositories();
        }



        public MadeRepositories Repositories { get; private set; }
        
        public static MadeContext Current
        {
            get
            {
                var context = HttpContext.Current.Items["madeContext:Current"] as MadeContext;

                if (context == null)
                    HttpContext.Current.Items["MadeContext:Current"] = context = new MadeContext();
                return context;
            }
        }

        //public IPublishedContent VirtualParent
        //{
        //    get { return HttpContext.Current.Items["VirtualParent"] as IPublishedContent; }
        //    set { HttpContext.Current.Items["VirtualParent"] = value; }
        //}
        
    }
}
