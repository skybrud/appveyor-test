﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;

namespace made.EventHandlers
{
    internal class ContentDefaultValues
    {
        public ContentDefaultValues()
        {
            ContentService.Created += ContentService_Creating;
            ContentService.Saved += ContentService_Saved;

            ContentService.Published += ContentService_Published;
            ContentService.UnPublished += ContentService_Unpublished;
            ContentService.Trashing += ContentService_Trashing;

        }

        private void ContentService_Trashing(IContentService sender, MoveEventArgs<IContent> e)
        {
            HandleCaching(e.MoveInfoCollection.ToList().Select(x => x.Entity));
        }

        private void ContentService_Unpublished(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            HandleCaching(e.PublishedEntities);
        }

        private void ContentService_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            HandleCaching(e.PublishedEntities);
        }

        private void ContentService_Creating(IContentService sender, NewEventArgs<IContent> e)
        {
            //Bruges til at udføre custom-handlinger når et dokument oprettes
            if (e.Alias == "MadeNyhed")
            {
                e.Entity.SetValue("contentDate", DateTime.Now);
            }
        }


        private void ContentService_Saved(IContentService sender, SaveEventArgs<IContent> e)
        {
            //Bruges til at udføre custom-handlinger når et dokument er gemt
            //Eksempel: Opretter en side under siden, hvis der ikke findes en af samme dokumentType i forvejen
            //foreach (var content in e.SavedEntities)
            //{
            //    if (content.ContentType.Alias.Equals("SOSU-Uddannelse"))
            //    {
            //        //Tjek om siden har en node af typen "SOSU-Kontainer-Uddannelse-Mediekarussel" i forvejen
            //        if (!content.Children().Any(x => x.ContentType.Alias.Equals("SOSU-Kontainer-Uddannelse-Mediekarussel")))
            //        {
            //            var mediaContainer = sender.CreateContent("Mediekarussel", content, "SOSU-Kontainer-Uddannelse-Mediekarussel");
            //            sender.SaveAndPublish(mediaContainer);
            //        }
            //    }
            //}
        }


        private void HandleCaching(IEnumerable<IContent> hai)
        {

            foreach (IContent content in hai)
            {
                //ApplicationContext.Current.ApplicationCache.RuntimeCache.ClearCacheByKeySearch(string.Format("{0}-{1}", content.ContentType.Alias, content.Id));
                //LogHelper.Info<ContentDefaultValues>("Partial pagecache cleared due to publish. NodeId: " + content.Id + " (" + content.ContentType.Alias + ")");

                if (content.ContentType.Alias == "MadeNyhed" || content.ContentType.Alias == "MadeArrangement")
                {
                    //Clear SoMe Cache
                    ApplicationContext.Current.ApplicationCache.RuntimeCache.ClearCacheByKeySearch("MadeSoMeCache");
                }

            }

        }
    }
}