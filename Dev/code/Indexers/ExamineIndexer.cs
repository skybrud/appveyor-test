﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Examine;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Values;
using Umbraco.Core.Logging;

namespace made.Indexers
{
    class ExamineIndexer
    {
        public ExamineIndexer()
        {
            var externalIndexer = ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"];
            var memberIndexer = ExamineManager.Instance.IndexProviderCollection["InternalMemberIndexer"];

            externalIndexer.GatheringNodeData += externalIndexer_GatheringNodeData;
            memberIndexer.GatheringNodeData += memberIndexer_GatheringNodeData;

        }


        void externalIndexer_GatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            if (e.Fields.ContainsKey("grid"))
            {
                IndexGridDataModel(e, e.Fields["grid"]);
            }

            if (!e.Fields.ContainsKey("hideFromSearch"))
            {
                e.Fields["hideFromSearch"] = "0";
            }

            if (!e.Fields.ContainsKey("isMemberEvent"))
            {
                e.Fields["isMemberEvent"] = "0";
            }


            // make csv-fields searchable
            if (e.Fields.ContainsKey("themes"))
            {
                MakeSearchable(e, "themes");
            }

            if (e.Fields.ContainsKey("region"))
            {
                MakeSearchable(e, "region");
            }

            if (e.Fields.ContainsKey("eventtype"))
            {
                MakeSearchable(e, "eventtype");
            }

            if (e.Fields.ContainsKey("activitytypes"))
            {
                MakeSearchable(e, "activitytypes");
            }

            if (e.Fields.ContainsKey("newstype"))
            {
                MakeSearchable(e, "newstype");
            }
        }

        void memberIndexer_GatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            //Bruges til at lave custom handlinger til et Examine index. I dette tilfælde "InternalMemberIndexer"
        }

        private void IndexGridDataModel(IndexingNodeDataEventArgs e, string propertyValue)
        {

            try
            {

                // Just return/exit now if the value doesn't look like JSON
                if (!propertyValue.StartsWith("{")) return;

                // Parse/deserialize the grid value
                GridDataModel grid = GridDataModel.Deserialize(propertyValue);

                // StringBuilder used for building the new grid value optimized for searching
                StringBuilder combined = new StringBuilder();

                foreach (GridControl ctrl in grid.GetAllControls())
                {

                    switch (ctrl.Editor.Alias)
                    {

                        case "rte":
                            {

                                // Get the HTML value
                                string html = ctrl.GetValue<GridControlRichTextValue>().Value;

                                // Strip any HTML tags so we only have text
                                string text = Regex.Replace(html, "<.*?>", "");

                                // Extra decoding may be necessary
                                text = HttpUtility.HtmlDecode(text);

                                // Now append the text
                                combined.AppendLine(text);

                                break;

                            }

                        case "quote":
                        case"headline" :{

                            string text = ctrl.GetValue<GridControlTextValue>().Value;

                            combined.AppendLine(text);

                            break;
                        }

                        //case "products":{
                        //    GridControlProductsValue value = ctrl.GetValue<GridControlProductsValue>();
                        //    if (value != null){

                        //        foreach (ProductRow row in value.Items) {
                        //            combined.AppendLine(row.Title);
                        //            combined.AppendLine(row.Description);    
                        //        }
                                
                        //    }
                        //    break;
                        //}

                    }

                }

                e.Fields["content"] = combined.ToString();
            }
            catch (Exception ex)
            {

                LogHelper.Error<ExamineIndexer>("MAYDAY! MAYDAY! MAYDAY!", ex);

            }

        }

        /// <summary>
        /// Make csv-field searchable
        /// </summary>
        /// <param name="e"></param>
        /// <param name="fieldname"></param>
        private void MakeSearchable(IndexingNodeDataEventArgs e, string fieldname)
        {
            var data = e.Fields[fieldname];
            data = data.Replace(',', ' ');
            e.Fields.Add(string.Format("{0}_search", fieldname), data);
        }
    }
}
