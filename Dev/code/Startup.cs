﻿using System;
using made.EventHandlers;
using made.Grid;
using made.Indexers;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.Module.Indexers;
using Umbraco.Core;
using ContentDefaultValues = Skybrud.Umbraco.Module.EventHandlers.ContentDefaultValues;

namespace made
{
    public class Startup : IApplicationEventHandler
    {
        private static readonly object Lock = new object();
        private static bool _started;

        private static ExamineLciIndexer _examineLciIndexer;
        private static ExamineDateRangeIndexer _examineDateRangeIndexer;

        private static ContentDefaultValues _defaultNewsValues;

        private static ExamineIndexer _examineIndexer;
        private static EventHandlers.ContentDefaultValues _defaultValues;

        private static AssetsWatcher _assetsWatcher;

        public static Guid ContentChangesGuid;
        public static Guid JsChangesGuid;


        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
            if (_started)
                return;

            lock (Lock)
            {
                if (!_started)
                {
                    _started = true;

                    _defaultNewsValues = new ContentDefaultValues(); //Default newsdate defaultvalue
                    _defaultValues = new EventHandlers.ContentDefaultValues();

                    //Adding Custom GridConverter
                    GridContext.Current.Converters.AddAt(0, new MadeGridConverter());

                    _examineIndexer = new ExamineIndexer(); //Register events for examine
                    _examineLciIndexer = new ExamineLciIndexer(); //LowerCase Indexer
                    _examineDateRangeIndexer = new ExamineDateRangeIndexer(new[] { "startdato", "slutdato", "contentDate" });


                    //SPA Stuff
                    _assetsWatcher = new AssetsWatcher(); //Watches pt. scripts-folder, to handle any js updates
                    ContentChangesGuid = Guid.NewGuid();
                        //Sets guid to handle changes in Umbracontent, to update frontend NG-cache
                    JsChangesGuid = Guid.NewGuid(); //Sets guid to handle changes in scripts-folder
                }
            }
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
        }

        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
        }
    }
}