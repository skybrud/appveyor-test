﻿using System.Web.Mvc;
using made.Models.Website;
using Umbraco.Web.Models;

namespace made.Controllers.Mvc
{
    public class MadeMedlemmerController : SkyController
    {
        public override ActionResult Index(RenderModel model)
        {
            return View(StartView.GetFromContent(CurrentPage));
        }
    }
}
