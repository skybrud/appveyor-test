﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using made.Models.Faq;
using Skybrud.Umbraco.Module.Extensions.Strings;
using Skybrud.WebApi.Json;
using Umbraco.Web.WebApi;

namespace made.Controllers.BackOffice
{
    [JsonOnlyConfiguration]
    public class FaqController : UmbracoAuthorizedApiController
    {
        [HttpGet]
        public object Search(string query = "", string categories = null)
        {

            // Make sure we have an array of category IDs
            int[] array = (categories ?? "").CsvToInt();

            // Make the search
            MadeFaqItem[] items = MadeFaqRepository.Search(query, array);

            return new
            {
                meta = new
                {
                    code = 200
                },
                data = items
            };

        }

        [HttpGet]
        public object GetItemsByIds(string ids)
        {

            // Make sure we have an array of item IDs
            int[] array = (ids ?? "").CsvToInt();

            return new
            {
                meta = new
                {
                    code = 200
                },
                data = MadeFaqRepository.GetItemsByIds(array)
            };

        }
    }
}
