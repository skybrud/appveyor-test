﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using made.Models.CustomContent;
using Skybrud.Umbraco.Module.Extensions.PublishedContent;
using Skybrud.Umbraco.Module.Extensions.Strings;
using Skybrud.WebApi.Json;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;

namespace made.Controllers.BackOffice
{
    [JsonOnlyConfiguration]
    [PluginController("CustomContent")]
    public class PickerController : UmbracoAuthorizedApiController
    {
        [HttpGet]
        public object GetContentByDocumentAlias(string documentTypeAlias, int startNodeId)
        {
            IPublishedContent startNode = UmbracoContext.ContentCache.GetById(startNodeId);

            if (startNode == null) return null;


            IEnumerable<CustomContentItem> nodes = startNode.Descendants(documentTypeAlias).Where(x => !x.Hidden()).Select(y => CustomContentItem.GetFromContent(y, "image"));

            return new
            {
                meta = new
                {
                    code = 200
                },
                data = nodes
            };
        }

        [HttpGet]
        public object GetItemsByIds(string ids)
        {
            // Make sure we have an array of item IDs
            int[] array = (ids ?? "").CsvToInt();

            return new
            {
                meta = new
                {
                    code = 200
                },
                data = CustomContentRepository.GetItemsByIds(array)
            };

        }
    }
}