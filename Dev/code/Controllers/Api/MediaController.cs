﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using made.Models.Media;
using Skybrud.WebApi.Json;
using Umbraco.Web.WebApi;

namespace made.Controllers.Api
{
    [JsonOnlyConfiguration]
    public class MediaController : UmbracoAuthorizedApiController
    {
        [HttpGet]
        public object GetByFolderId(int mediaFolderId, bool goDeep = false)
        {
            return new
            {
                meta = new
                {
                    code = 200
                },
                data = MediaRepository.GetByMediaFolderId(mediaFolderId, goDeep)
            };
        }

        [HttpGet]
        public object GetMediaFolderName(int mediaFolderId)
        {
            return new
            {
                meta = new
                {
                    code = 200
                },
                data = MediaRepository.GetMediaFolderName(mediaFolderId)
            };
        }
    }
}
