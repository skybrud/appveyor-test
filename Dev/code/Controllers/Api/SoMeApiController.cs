﻿using System;
using System.Net;
using System.Web;
using System.Web.Http;
using made.Models.SoMe;
using Skybrud.Umbraco.Module.ErrorHandling;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using Umbraco.Core.Logging;
using Umbraco.Web.WebApi;

namespace made.Controllers.Api
{
    [JsonOnlyConfiguration]
    public class SoMeApiController : UmbracoApiController
    {
        [HttpGet]
        public object GetSoMe(int limit = 1, int offset = 0)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            MadeSoMeSearchResult result;
            try
            {
                //TODO: Midlertidig fjernet, pga. paginering
                //MadeSoMeSearchResult soMeCached =
                //    ApplicationContext.ApplicationCache.RuntimeCache.GetCacheItem("MadeSoMeCache",
                //        () => MadeContext.Current.Repositories.SoMe.GetSoMe(true, true, true, true, true), new TimeSpan(0, 0, 30, 0)) as
                //        MadeSoMeSearchResult;

                //result = soMeCached;

                result = MadeContext.Current.Repositories.SoMe.GetSoMe(true, true, true, true, true, limit, offset);
            }
            catch (Exception ex)
            {
                var error = new Error("Der skete en fejl på serveren");
                LogHelper.Error<SoMeApiController>(error.ToString(), ex);

                return
                    Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError, error.Message,
                        error));
            }


            JsonMetaResponse response = JsonMetaResponse.GetSuccess(result.Items);
            response.SetPagination(new JsonPagination
            {
                Total = result.Total,
                Limit = result.Limit,
                Offset = result.Offset
            });

            return Request.CreateResponse(response); 
        }
    }
}
