﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using made.Models.Website.SiteSearch;
using Skybrud.Umbraco.Module;
using Skybrud.Umbraco.Module.ErrorHandling;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;


namespace made.Controllers.Api{
    [JsonOnlyConfiguration]
    public class SiteSearchApiController : UmbracoApiController{
        [HttpGet]
        public object Search(string keywords = "", int offset = 0, int limit = 10, string doctypes = "",
            string fields = ""){
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            //defaults
            doctypes = string.IsNullOrWhiteSpace(doctypes)
                ? "MadeUnderside,MadeNyhed,MadeArrangement,MadePerson,MadeTema"
                : doctypes;
            fields = string.IsNullOrWhiteSpace(fields)
                ? "nodeName_lci,title_lci,teaser_lci,content_lci,location_lci,price_lci,info_lci,moreInfo_lci,question_lci,answer_lci,name_lci,title_lci,phone_lci,email_lci,description_lci,companyname_lci,address_lci,moreAddress_lci,zip_lci,city_lci"
                : fields;


            try{
                if (string.IsNullOrEmpty(doctypes)){
                    return
                        Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError,
                            "Der er ikke angivet dokumenttype der skal søges i."));
                }

                Int32 total = 0;
                string[] dts = doctypes.Split(',');
                string[] fieldsArr = String.IsNullOrEmpty(fields) ? null : fields.Split(',');

                IEnumerable<IPublishedContent> results = SkybrudSearch.SearchDocuments(keywords, out total, dts, fieldsArr, limit: limit, offset: offset);
                

                IEnumerable<SiteSearchResult> convertedResults = results.Select(SiteSearchResult.GetFromContent);

                JsonMetaResponse response = JsonMetaResponse.GetSuccess(convertedResults);

                response.SetPagination(new JsonPagination
                {
                    Total = total,
                    Limit = limit,
                    Offset = offset
                });

                return Request.CreateResponse(response);
            }
            catch (Exception ex){
                var error = new Error("Der skete en fejl på serveren");
                LogHelper.Error<SiteSearchApiController>(error.ToString(), ex);
                return
                    Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError, error.Message,
                        error));
            }
        }
    }
}