﻿using System.Web;
using made.Models.Website.Common;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

namespace made.Controllers.Api.Spa
{
    [JsonOnlyConfiguration]
    public class SettingsApiController : UmbracoApiController
    {
        /// <summary>
        /// Return settings based on current domain
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public object GetSettings(){
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            string currentDomain = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            int rootNodeID = umbraco.cms.businesslogic.web.Domain.GetRootFromDomain(currentDomain);

            if (rootNodeID == 0) return "";

            IPublishedContent _settings = UmbracoContext.ContentCache.GetById(rootNodeID);

            return Request.CreateResponse(JsonMetaResponse.GetSuccess(Settings.GetFromContent(_settings, new Settings())));
        }
    }
}
