﻿using System;
using System.Linq;
using System.Web;
using made.Models.Website.SPA;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using Umbraco.Web.WebApi;

namespace made.Controllers.Api.Spa
{
    [JsonOnlyConfiguration]
    public class NavigationApiController : UmbracoApiController
    {
        [System.Web.Http.HttpGet]
        public object GetPageTree(int nodeId, int levels = 1){
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            
            return Request.CreateResponse(JsonMetaResponse.GetSuccess(NavItem.GetItem(UmbracoContext.ContentCache.GetById(nodeId), levels)));
        }

        [System.Web.Http.HttpGet]
        public object GetByIds(string ids, int levels = 1){
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            int[] idArr = ids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToArray();

            return Request.CreateResponse(JsonMetaResponse.GetSuccess(NavItem.GetItems(idArr, levels)));
        }
    }
}
