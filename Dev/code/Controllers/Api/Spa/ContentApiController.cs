﻿using System;
using System.Net;
using System.Web;
using made.Models.Events;
using made.Models.Faq;
using made.Models.News;
using made.Models.SoMe;
using made.Models.Website.Made;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using umbraco;
using Umbraco.Core.Logging;
using Umbraco.Web.WebApi;

namespace made.Controllers.Api.Spa
{
    [JsonOnlyConfiguration]
    public class ContentApiController : UmbracoApiController
    {
        [System.Web.Http.HttpGet]
        public object GetContent(string url){
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            

            url = HttpUtility.UrlDecode(url);

            var urlName = url == "/"
                ? "/forside"
                : url;

            // find content by url. If non found return 404
            var content = !string.IsNullOrEmpty(urlName)
                ? UmbracoContext.ContentCache.GetByRoute(urlName)
                : null;

            if (content == null) return Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.NotFound, "Siden fandtes ikke"));

            
            // Find Culture
            var domains = library.GetCurrentDomains(content.Id);
            string culture = domains[0].Language.CultureAlias;

            // Set Culture for api-call
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(culture);
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;



            switch (content.DocumentTypeAlias){

                case "MadeForside":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeForside.GetFromContent));

                case "MadeTemaer":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeTemaer.GetFromContent));

                case "MadeTema":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeTema.GetFromContent));

                case "MadePersoner":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadePersoner.GetFromContent));

                //case "MadePerson":
                //    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeUnderforside.GetFromContent));

                case "MadeUnderside":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeUnderside.GetFromContent));

                case "MadeUnderforside":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeUnderforside.GetFromContent));

                case "MadeNyhed":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeNyhed.GetFromContent));

                case "MadeNyheder":
                    return Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeNewsOverview.GetFromContent));

                case "MadeFaqVisning":
                    return
                        Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeFaqside.GetFromContent));

                case "MadeArrangement":
                    return
                        Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeEventPage.GetFromContent));

                case "MadeArrangementer":
                    return
                        Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeEventOverview.GetFromContent));

                case "MadeSoMe":
                    return
                        Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeSoMePage.GetFromContent));

                case "MadeMedlemmer":
                    return
                        Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeMedlemmer.GetFromContent));

                case "MadeMedlemsinfo":
                    return
                        Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(content, MadeMedlemsInfo.GetFromContent));

            }

            //throw error
            return Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError, "Der opstod en fejl på serveren."));
        }
    }

}
