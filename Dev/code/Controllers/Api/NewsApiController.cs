﻿using System;
using System.Net;
using System.Web;
using System.Web.Http;
using made.Models.SearchResults;
using Skybrud.Umbraco.Module.ErrorHandling;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using Umbraco.Core.Logging;
using Umbraco.Web.WebApi;

namespace made.Controllers.Api
{
    [JsonOnlyConfiguration]
    public class NewsApiController : UmbracoApiController
    {
        [HttpGet]
        public object Search(string theme = "", string activitytype = "", string type = "", int limit = 10,
            int offset = 0)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            MadeSearchResult result;
            try
            {
                result = MadeContext.Current.Repositories.News.SearchNews(
                    theme,
                    activitytype,
                    type,
                    limit,
                    offset
                    );
            }
            catch (Exception ex)
            {
                var error = new Error("Der skete en fejl på serveren");
                LogHelper.Error<EventApiController>(error.ToString(), ex);

                return
                    Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError, error.Message,
                        error));
            }

            JsonMetaResponse response = JsonMetaResponse.GetSuccess(result.Items);

            response.SetPagination(new JsonPagination
            {
                Total = result.Total,
                Limit = result.Limit,
                Offset = result.Offset
            });

            return Request.CreateResponse(response);
        }
    }
}