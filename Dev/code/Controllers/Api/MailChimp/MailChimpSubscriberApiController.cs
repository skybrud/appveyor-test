﻿using System;
using System.Configuration;
using System.Net;
using System.Web.Http;
using made.Models.MailChimp;
using MailChimp;
using MailChimp.Helper;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using Umbraco.Web.WebApi;

namespace made.Controllers.Api.MailChimp
{
    [JsonOnlyConfiguration]
    public class MailChimpSubscriberApiController : UmbracoApiController
    {
        [HttpPost]
        public object PostSubscriber([FromBody] SubscriberParameters data)
        {
            try
            {
                var MC = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);
                var EP = new EmailParameter
                {
                    Email = data.Email
                };

                var MV = new CustomMergeVar
                {
                    MMERGE3 = data.Name,
                    MMERGE4 = data.Organisation
                };

                EmailParameter res = MC.Subscribe(data.ListId, EP, MV, doubleOptIn: true);

                return
                    Request.CreateResponse(JsonMetaResponse.GetSuccessFromObject(res, MailChimpApiModel.GetFromContent));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError, ex.Message));
            }
        }
    }

    public class SubscriberParameters
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string ListId { get; set; }
        public string Organisation { get; set; }
    }
}