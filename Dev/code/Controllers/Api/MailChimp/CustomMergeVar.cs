﻿using MailChimp.Lists;

namespace made.Controllers.Api.MailChimp
{
    /// <summary>
    /// Adds custom property to MailChimp.Net´s MergeVar class
    /// </summary>
    public class CustomMergeVar : MergeVar
    {
        public string MMERGE3 { get; set; } //Navn
        public string MMERGE4 { get; set; } //Organisation / virksomhed
    }
}
